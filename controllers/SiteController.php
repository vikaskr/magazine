<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;

class SiteController extends Controller
{
    public $discount = 0;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (
                $action->id == "payment-process" || $action->id == 'payment-cancel'
        ) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $this->view->params['page'] = 'home';
       $magazine = new Magazine();
       $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        
        if(!empty($origin))
        {
            $nationalMag = $magazine::find()->where(['del_status' => '0'])->count();
            $nationalMagazines = $magazine::find()->where(['del_status' => '0'])->limit(24)->all();
            return $this->render('index',['nationalMagazines' => $nationalMagazines, 'origin' => $origin]);
            
        }
        else
        {
            p("No Country found by name ".$userCountry);
        }
    }

    public function sendMail($contact)
    {
        $cart = new ShoppingCart();
        $cartItems = ArrayHelper::toArray($cart->getPositions());
        $price = $cart->getCost();
        $success = \Yii::$app->mailer->compose('booking',['cartItems' => $cartItems, 'price' => $price])
          ->setFrom('contact@nursingMagazine.online')
          ->setTo('contact@nursingMagazine.online.test-google-a.com')
          ->setSubject('Your Journal Details')
          ->setTextBody('Plain text content')
          ->setCc($contact->email)
          ->setBcc('praveen@makeubig.com')
          ->send();
        return $success;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        return $this->renderAjax('signup');
    }

   public function actionClientLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('signin', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }


   public function actionCheckoutLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('checkout-login', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('checkout-login', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }

    public function actionClientRegister()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
             
            if ($model->load(Yii::$app->request->post())) 
            {      
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setCc('info@osmstays.com')
                        ->setSubject('Your Profile Created')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
                else
                {
                   $model->addError('username','This Username Already Exists in Database');
                   return $this->renderAjax('signup',['model' => $model]);
                }
            }
            return $model->getErrors();
        }
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionProfile()
    {   
        $this->view->params['page']='profile'; 
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/site/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    public function actionLogout()
    {
        $this->layout = false;
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/');
            }
    }

    public function actionGetSortedResult($sortBy,$categoryId)
    {
     $this->layout = false;
        $magazine = new Magazine();
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();

        $magCount = $magazine::find()->where(['subject_id' => $categoryId])->count();
        $pages = new Pagination(['totalCount' => $magCount,'pageSize' => 12]);
        
        if(!empty($origin))
        {
        $command = "Select 
            m.id,
            m.name,
            m.slug,
            m.cover_image,
            m.subject_id,
            p.origin_id,
            m.del_status,
            p.price from magazine as m LEFT JOIN magazine_price as p on m.id = p.magazine_id where p.origin_id = '$origin->id' AND m.subject_id = '".$categoryId."'";
        switch($sortBy)
        {
            case 'phtl':
            {
                $command .= " ORDER BY `price` DESC";
                break;
            }
            case 'plth':
            {
                $command .= " ORDER BY `price` ASC";
                break;
            }
            case 'nasc':
            {
                $command .= " ORDER BY `name` DESC";
                break;
            }
            case 'ndsc':
            {
                $command .= " ORDER BY `name` ASC";
                break;
            }

            case 'newest_tab':
            {
                $command .= " ORDER BY `id` ASC";
                break;
            }
            default:
            {

            }

        }
            $command .= " LIMIT $pages->pageSize OFFSET $pages->offset";

        $result = \Yii::$app->db->createCommand($command)->queryAll();

        return $this->renderAjax('sortdata',['result' => $result, 'origin' => $origin, 'pages' => $pages]);
        }
    }

    /**
     * Displays about page.
     *
     * @return string
     */

    public function actionSuccesspdf($id)
    {
        $this->layout = false;
        $pdf = \Yii::$app->pdf;
        $pdf->content = $this->renderPartial('successpdf');
        return $pdf->render();
    }

    public function actionGetItems($menuId)
    {
        $this->layout = false;
        $getItem = new Magazine();
        $menuDetail = $getItem::find()->where(['menu_id' => $menuId, 'del_status' => '0'])->all();
        $response = $this->render('items',['items' => $menuDetail]);
       return $response;
    }

    public function actionAddtocart($id)
    {   
        $cart = new ShoppingCart();
        $this->layout = false;
        $model = Magazine::findOne($id);
        if ($model){
            $cart->put($model, 1);
             p($cart->getCost());
            return $this->render('addtocart', [
                'cart' => $cart
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionClearCart($id=NULL)
    {
         $cart = new ShoppingCart();
         $this->layout = false;
         if($id == NULL)
         {
            $cart->removeAll();
         }
         else
         {
            $cart->removeById($id);  
         }
         return true;
         // return $this->render('showcart', [
         //    'cart' => $cart]);
    }
    
    

    public function actionCategory($name)
    {
    $this->view->params['page']='category';
    $magazine = new Magazine();
       $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        
        if(!empty($origin))
        {
            $nationalMagazines = $magazine::find()->where(['del_status' => '0'])->all();
            return $this->render('category',['nationalMagazines' => $nationalMagazines, 'origin' => $origin]);
            
        }
        else
        {
            p("No Country found by name ".$userCountry);
        }
    }

    public function actionAbout()
    {
        $this->view->params['page']='about';
        return $this->render('about');
    }

    public function actionSubcriber()
    {
        $subcriber = new \app\models\SignupMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $subcriber->load($params))
        {
            $this->layout = false;
            if($subcriber->save(false))
            {   
                return true;
            }
            else
            {
                p($subcriber->getErrors());
            }
                    
        }
        return $this->render('subcriber',['subcriber' => $subcriber]);
    }

    public function actionContact()
    {
        $this->view->params['page']='contact';
        $contact = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contact->load($params))
        {
            $this->layout = false;
            if($contact->save(false))
            {   
                return true;
            }
            else
            {
                p($contact->getErrors());
            }
                    
        }
        return $this->render('contact',['contact' => $contact]);
    }

    public function actionTerm()
    {
        $this->view->params['page']='term';
        return $this->render('term');
    }

    public function actionPolicy()
    {
        $this->view->params['page']='policy';
        return $this->render('policy');
    }

    public function actionRefund()
    {
        $this->view->params['page']='refund';
        return $this->render('refund');
    }

    public function actionPayment()
    {
        $this->view->params['page']='payment';
        return $this->render('payment');
    }

    public function actionShipping()
    {
        $this->view->params['page']='shipping';
        return $this->render('shipping');
    }

    public function actionLocation()
    {
        $this->view->params['page']='location';
        return $this->render('location');
    }

    public function actionEstimatedDelivery()
    {
        $this->view->params['page']='estimated-delivery';
        return $this->render('estimated-delivery');
    }

    public function actionLibrariescorporate()
    {
         $this->view->params['page']='librariescorporate';
        return $this->render('librariescorporate');
    }

    public function actionMagazinebysector()
    {
        $this->view->params['page']='magazinebysector';
        return $this->render('magazinebysector');
    }

    public function actionFaq()
    {
        $this->view->params['page']='faq';
        return $this->render('faq');
    }

     public function actionCartcount()
    {
        $this->layout = false;
        $this->view->params['page']='cartcount';
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        return $this->render('cartcount', ['count' => $count]);
    }

    public function actionCheckout()
    {
      $this->view->params['page']='checkout';   
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        $originId = $origin->id;
         if(!empty($cartItems)){ 
          foreach($cartItems as $item){
           $magazineId = $item->id;
           $gbpPrice = MagazinePrice::find()->where(['magazine_id' => $magazineId,'origin_id' => $originId])->one();
           $disc = $gbpPrice->discount_percentage;
           $gbp = $gbpPrice->price;
            $discVal = PriceHelper::getCountryPrice($gbp);
            $this->discount += (($disc/100) * ($discVal) * ($item->getQuantity()));
             }
        }
            $discValue = $this->discount;
        if(!empty($origin))
        {
            return $this->render('checkout',['origin' => $origin, 'discValue' => $discValue, 'cartItems' => $cartItems, 'count' => $count, 'cart' => $cart]);
        }
    }

    public function actionPayments()
    {
        $this->view->params['page']='payments';
        $booking = new \app\models\Booking();  
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        
         $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        $originId = $origin->id;
         if(!empty($cartItems)){ 
          foreach($cartItems as $item){
           $magazineId = $item->id;
           $gbpPrice = MagazinePrice::find()->where(['magazine_id' => $magazineId,'origin_id' => $originId])->one();
           $disc = $gbpPrice->discount_percentage;
           $gbp = $gbpPrice->price;
            $discVal = PriceHelper::getCountryPrice($gbp);
            $this->discount += (($disc/100) * ($discVal) * ($item->getQuantity()));
             }
        }
            $discValue = $this->discount;
        if(!empty($origin))
        {
            return $this->render('payments',['booking' => $booking,'origin' => $origin, 'discValue' => $discValue, 'cartItems' => $cartItems, 'count' => $count, 'cart' => $cart]);
        }
    }

    public function actionLoginCheckout()
    {
        $this->view->params['page']='login-checkout';
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->render('login-checkout', [
                'model' => $model,
            ]);
        
    }

    public function actionQuickview($name)
    {
        $this->layout = false;
        $userDetails = ip_info();
        $magazine = new Magazine();
        $magazineDetails = $magazine::find()->where(['slug' => $name])->one();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        return $this->renderAjax('quickview',['magazineDetails' => $magazineDetails,'origin' => $origin]);
    }

    public function actionPublisher()
    {
        $this->view->params['page']='publisher'; 
        return $this->render('publisher');
    }

    public function actionShowcart()
    {
        $this->layout = false;
        $cart = new ShoppingCart();
        $items = $cart->getPositions();
        return $this->render('showcart', [
            'items' => $items]);  
    }

    public function actionUpdateCartItems($id,$quantity)
    {  
        if($quantity >= 0)
        {
            $cart = new ShoppingCart();
            $this->layout = false;
            $model = Magazine::findOne($id);
            if($model)
            {
                $cart->update($model, $quantity);
            }
            $total = $cart->getCost(); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['total'] = $total;
            return $data;
        } 
    }
    
    public function actionForgetpass()
    {
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('admin@makeubig.com')
                    ->setTo($mubUserContact->email)
                    ->setCc('praveen@makeubig.com')
                    ->setSubject('Your Magazine World Online Password')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->renderAjax('forgetpass');
    }

    public function actionProduct($name)
    {
        $this->view->params['page']='product';
        $magazine = new Magazine();
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }
        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        
        if(!empty($origin))
        {
        $magazineDetails = $magazine::find()->where(['slug' => $name])->one();
        $nationalMagazines = $magazine::find()->where(['del_status' => '0'])->all();
        return $this->render('product',['magazineDetails' => $magazineDetails, 'nationalMagazines' => $nationalMagazines, 'origin' => $origin]);
        }   
    }

    public function actionSubsciber()
    {
        $params = \yii::$app->request->getBodyParams();
        $signupMail = new \app\models\SignupMail();
        if($signupMail->load($params))
         {
          if ($signupMail->save())
          {
             \Yii::$app->mailer->compose('subscribe')
                ->setTo($signupMail->email)
                ->setBcc('contact@mazagineworld.online')
                ->setCc('praveen@makeubig.com')
                ->setFrom(['admin@makeubig.com' => 'Magazine'])
                ->setSubject('Customer Signed Up')
                ->setHtmlBody("<b>Subscibe with Mail Id ".$signupMail->email." Signed up to your Newslette Magazines World")
                ->send();    
            return true;
            }
            else
            {
            return false;
            }
        }
    }

    public function actionCountryPrice()
    {
        $this->layout = false;
        $country = \yii::$app->request->getBodyParams();
        $magazinePrice = new MagazinePrice();
        $origin = new Origin();
        $magazine = new Magazine();

        $userDetails = ip_info();
        $countryDetail = $userDetails['country'];
        if(($countryDetail == '')||
            ($countryDetail == 'US')||
            ($countryDetail == 'USA'))
        {
            $countryDetail = 'UNITED STATES OF AMERICA';
        }

        $countryDetails = \app\helpers\StringHelper::generateSlug($countryDetail);
        $originCur = Origin::find()->where(['origin_slug' => $countryDetails])->one();
        $currentId = $originCur->id;

        $countrySlug = \app\helpers\StringHelper::generateSlug($country['country']);
        $magazineSlug = $country['magazine'];

        $magazineId = $magazine::find()->where(['slug' => $magazineSlug])->one();

        $magId = $magazineId->id;


        $countryId = $origin::find()->where(['origin_slug' => $countrySlug])->one();
        $orgId = $countryId->id;

        $gbpPrice = $magazinePrice::find()->where(['origin_id' => $orgId, 'magazine_id' => $magId])->one();
        $discount = $gbpPrice->discount_percentage;
        $userCountry = $countrySlug;
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }

        if(!empty($countryId))
        {
            switch($currentId)
            {
                case '158': 
                {
                   $gbpPrice->price = $gbpPrice->price;                    
                    break;
                }
                case '100': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.83782);                    
                    break;
                }
                case '56': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 0.529505);                    
                    break;
                }
                case '126': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.14227);
                    break;
                }
                case '164': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.81913);
                    break;
                }

                case '61': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 8.86216);
                    break;
                }

                case '15': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 24.8449);
                    break;
                }
                case '62': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 91.7839);
                    break;
                }
                case '67': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 150.001);
                    break;
                }
                case '69': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 0.42348);
                    break;
                }
                case '73': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 5.44895);
                    break;
                }
                case '79': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 0.541517);
                    break;
                }
                case '83': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 5.12645);
                    break;
                }
                case '84': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 81.215);
                    break;
                }
                case '96': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 5.17233);
                    break;
                }
                case '182': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.40833);
                    break;
                }
                case '68': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 0.998505);
                    break;
                }
                case '72': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 2123.06);
                    break;
                }
                case '85': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 5.28147);
                    break;
                }

                case '86': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.8471);
                    break;
                }

                case '45': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 16.5631);
                    break;
                }

                case '88': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 219.249);
                    break;
                }

                case '156': 
                {
                    $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.34625);
                    break;
                }

                default:  
                $gbpPrice->price = ceil(floatval($gbpPrice->price) * 1.40833);
                    break;
            }

        return $this->render('country-price', ['price' => $gbpPrice, 'originCur' => $originCur, 'magazineId' => $magazineId, 'discount' => $discount]);
    }
    }

}


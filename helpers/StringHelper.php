<?php

namespace app\helpers;
use app\modules\MubAdmin\modules\csvreader\models\Product;

class StringHelper
{
	public static function generateSlug($string)
	{
		$string = str_replace(' ', '-', $string);
		$data = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		return strtolower($data);
	}

	public function getFistLastName($name)
	{

		$clientName	= explode(' ',$name);
		$response = [];
		if(count($clientName) > 1)
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[1];
		}
		else 
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[0];
		}
		return $response;
	}

	public static function getAuthorSlug($allAuthors)
	{	
		$authorLastName = [];

		foreach($allAuthors as $lastName)
		{
			if($lastName != "")
			{
				$lastName = strtolower($lastName);

				if(preg_match('(phd|md|m.d.|group|ed.|ed|dr|club|jr.|jr|pharm.d|m d|phd.|ph d)', $lastName) === 1)
				{	

					$authorLastName[] = self::generateSlug($lastName);
				}
				else
				{
					$authorsLastName = explode(' ', $lastName);
					$authorLastName[] = end($authorsLastName);
				}
			}
			else
			{	
				$authorLastName[] = "na";
			}
		}
		return $authorLastName;
	}

	public static function getIdentifierType(String $identifierType)
	{
		$identifier['01'] = 'Proprietary';
		$identifier['02'] = 'isbn_10';
		$identifier['03'] = 'ean_13';
		$identifier['04'] = 'upc';
		$identifier['05'] = 'ISMN-10';
		$identifier['06'] = 'DOI';
		$identifier['13'] = 'LCCN';
		$identifier['14'] = 'gtin_14';
		$identifier['15'] = 'isbn_13';
		$identifier['17'] = 'Legal deposit number';
		$identifier['22'] = 'URN';
		$identifier['23'] = 'OCLC number';
		$identifier['24'] = 'Co-publisher’s ISBN-13';
		$identifier['25'] = 'ISMN-13';
		$identifier['26'] = 'ISBN-A';
		$identifier['27'] = 'JP e-code';
		$identifier['28'] = 'OLCC number';
		$identifier['29'] = 'JP Magazine ID';
		$identifier['30'] = 'UPC12+5';
		$identifier['31'] = 'BNF Control number';
		$identifier['35'] = 'ARK';

		return $identifier[$identifierType];
	}

	public static function getProductIdentifier($productIdentifier)
	{
		$product = new Product();
		$record = null;
		foreach($productIdentifier as $value){

			$column = self::getIdentifierType((String) $value->b221);
			$productValue = (String) $value->b244;
			$record = $product::find()->where([$column => $productValue,'del_status' => '0'])->one();
			//p($record->createCommand()->rawSql);
			if(!empty($record))
			{
				//print_r($record->createCommand()->rawSql);
				$product = $record;
				break;
			}
			$product->{$column} = (String)$value->b244;
		}
		return $product;
	}

	public static function getAuthorRole($roleCode)
	{	
		
	}

	public static function getAuthorField($keyName)
	{	p($keyName);
		$formatAuthor['NA'] = 'undefined';
		$formatAuthor['b039'] = 'first_name';
		$formatAuthor['b040'] = 'last_name';
		$formatAuthor['b249'] = 'undefined';
		$formatAuthor['A01'] = 'author';
		$formatAuthor['B01'] = 'edited_author';
		$formatAuthor['A09'] = 'created_author';
		$formatAuthor['D02'] = 'director';
		$formatAuthor['E01'] = 'actor';
		$formatAuthor['Z99'] = 'other';

		if(isset($formatAuthor[$keyName]))
		{

		return $formatAuthor[$keyName];
		}
		return $formatAuthor['NA'];

	}

	public static function getFormatValue($format)
	{

		$formatArray = [];

		$formatArray['00']="Undefined";
		$formatArray['AA']="Audio";
		$formatArray['AB']="Audio cassette";
		$formatArray['AC']="CD-Audio";
		$formatArray['AD']="DAT";
		$formatArray['AE']="Audio disc";
		$formatArray['AF']="Audio tape";
		$formatArray['AG']="MiniDisc";
		$formatArray['AH']="CD-Extra";
		$formatArray['AI']="DVD Audio";
		$formatArray['AJ']="Downloadable audio";
		$formatArray['AK']="Pre-recorded digital audio player";
		$formatArray['AL']="Pre-recorded SD card";
		$formatArray['AZ']="Other audio format";
		$formatArray['BA']="Book";
		$formatArray['BB']="Hardback";
		$formatArray['BC']="Paperback / softback";
		$formatArray['BD']="Loose-leaf";
		$formatArray['BE']="Spiral bound";
		$formatArray['BF']="Pamphlet";
		$formatArray['BG']="Leather / fine binding";
		$formatArray['BH']="Board book";
		$formatArray['BI']="Rag book";
		$formatArray['BJ']="Bath book";
		$formatArray['BK']="Novelty book";
		$formatArray['BL']="Slide bound";
		$formatArray['BM']="Big book";
		$formatArray['BN']="Part-work (fascículo)";
		$formatArray['BO']="Fold-out book or chart";
		$formatArray['BP']="Foam book";
		$formatArray['BZ']="Other book format";
		$formatArray['CA']="Sheet map";
		$formatArray['CB']="Sheet map, folded";
		$formatArray['CC']="Sheet map, flat";
		$formatArray['CD']="Sheet map, rolled";
		$formatArray['CE']="Globe";
		$formatArray['CZ']="Other cartographic";
		$formatArray['DA']="Digital";
		$formatArray['DB']="CD-ROM";
		$formatArray['DC']="CD-I";
		$formatArray['DD']="DVD";
		$formatArray['DE']="Game cartridge";
		$formatArray['DF']="Diskette";
		$formatArray['DG']="Electronic book text";
		$formatArray['DH']="Online resource";
		$formatArray['DI']="DVD-ROM";
		$formatArray['DJ']="Secure Digital (SD) Memory Card";
		$formatArray['DK']="Compact Flash Memory Card";
		$formatArray['DL']="Memory Stick Memory Card";
		$formatArray['DM']="USB Flash Drive";
		$formatArray['DN']="Double-sided CD/DVD";
		$formatArray['DZ']="Other digital";
		$formatArray['FA']="Film or transparency";
		$formatArray['FB']="Film";
		$formatArray['FC']="Slides";
		$formatArray['FD']="OHP transparencies";
		$formatArray['FE']="Filmstrip";
		$formatArray['FF']="Film";
		$formatArray['FZ']="Other film or transparency format";
		$formatArray['MA']="Microform";
		$formatArray['MB']="Microfiche";
		$formatArray['MC']="Microfilm";
		$formatArray['MZ']="Other microform";
		$formatArray['PA']="Miscellaneous print";
		$formatArray['PB']="Address book";
		$formatArray['PC']="Calendar";
		$formatArray['PD']="Cards";
		$formatArray['PE']="Copymasters";
		$formatArray['PF']="Diary";
		$formatArray['PG']="Frieze";
		$formatArray['PH']="Kit";
		$formatArray['PI']="Sheet music";
		$formatArray['PJ']="Postcard book or pack";
		$formatArray['PK']="Poster";
		$formatArray['PL']="Record book";
		$formatArray['PM']="Wallet or folde";
		$formatArray['PN']="Pictures or photographs";
		$formatArray['PO']="Wallchart";
		$formatArray['PP']="Stickers";
		$formatArray['PQ']="Plate (lámina)";
		$formatArray['PR']="Notebook / blank book";
		$formatArray['PS']="Organizer";
		$formatArray['PT']="Bookmark";
		$formatArray['PZ']="Other printed item";
		$formatArray['VA']="Video";
		$formatArray['VB']="Video, VHS, PAL";
		$formatArray['VC']="Video, VHS, NTSC";
		$formatArray['VD']="Video, Betamax, PAL";
		$formatArray['VE']="Video, Betamax, NTSC";
		$formatArray['VF']="Videodisc";
		$formatArray['VG']="Video, VHS, SECAM";
		$formatArray['VH']="Video, Betamax, SECAM";
		$formatArray['VI']="DVD video";
		$formatArray['VJ']="VHS video";
		$formatArray['VK']="Betamax video";
		$formatArray['VL']="VCD";
		$formatArray['VM']="SVCD";
		$formatArray['VN']="HD DVD";
		$formatArray['VO']="Blu-ray";
		$formatArray['VP']="UMD Video";
		$formatArray['VZ']="Other video format";
		$formatArray['WW']="Mixed media product";
		$formatArray['WX']="Multiple copy pack";
		$formatArray['XA']="Trade-only material";
		$formatArray['XB']="Dumpbin – empty";
		$formatArray['XC']="Dumpbin – filled";
		$formatArray['XD']="Counterpack – empty";
		$formatArray['XE']="Counterpack – filled";
		$formatArray['XF']="Poster, promotional";
		$formatArray['XG']="Shelf strip";
		$formatArray['XH']="Window piece";
		$formatArray['XI']="Streamer";
		$formatArray['XJ']="Spinner";
		$formatArray['XK']="Large book display";
		$formatArray['XL']="Shrink-wrapped pack";
		$formatArray['XZ']="Other point of sale";
		$formatArray['ZA']="General merchandise";
		$formatArray['ZB']="Doll";
		$formatArray['ZC']="Soft toy";
		$formatArray['ZD']="Toy";
		$formatArray['ZE']="Game";
		$formatArray['ZF']="T-shirt";
		$formatArray['ZZ']="Other merchandise";
		$formatArray['NA']="NA";

		if(isset($formatArray[$format]))
		{

		return $formatArray[$format];
		}
		return $formatArray['NA'];
	}

}
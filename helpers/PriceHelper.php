<?php

namespace app\helpers;
use app\modules\MubAdmin\modules\csvreader\models\Origin;

class PriceHelper {
	
public static function getPriceRange()
{
    return [
        '0-25000000'        => 'Below 25 million',
        '25000000-50000000'   => '25 - 50 million',
        '50000000-100000000'   => '50 - 100 million',
        '100000000-200000000'   => '100 - 200 million',
        '200000000-250000000'   => '200 - 250 million',
        '250000000-350000000'   => '250 - 350 million',
        '350000000-500000000'  => '350 - 500 million',
        '500000000-'        => '500 million And Above',
    ];
}

public static function getPriceRangeApi()
{
    return [
        ['key' => '0-25000000','value' => 'Below 25 million'],
        ['key' => '25000000-50000000','value' => '25 - 50 million'],
        ['key' => '50000000-100000000','value' => '50 - 100 million'],
        ['key' => '100000000-200000000','value' => '100 - 200 million'],
        ['key' => '200000000-250000000','value' => '200 - 250 million'],
        ['key' => '250000000-350000000','value' => '250 - 350 million'],
        ['key' => '350000000-500000000','value' => '350 - 500 million'],
        ['key' => '500000000-1100000000','value' => '500 million And Above'],
    ];
}
public static function getBudgetListApi()
{
    return [
        ['key' => '0','value' => '0'],
        ['key' => '25000000','value' => '25 million'],
        ['key' => '50000000','value' => '50 million'],
        ['key' => '100000000','value' => '100 million'],
        ['key' => '200000000','value' => '200 million'],
        ['key' => '250000000','value' => '250 million'],
        ['key' => '350000000','value' => '350 million'],
        ['key' => '500000000','value' => '500 million'],
        ['key' => '1000000000','value' => '1000 million'],
    ];
}
public static function getPrice()
{
    return [
    	''		   =>	'Select',
        '25000000'   => 'Below 25 million',
        '50000000'   => '50 million',
        '100000000'   => '100 million',
        '200000000'   => '200 million',
        '250000000'   => '250 million',
        '350000000'   => '350 million',
        '500000000'  => '500 million',
        '600000000'  => '500 million And Above',
    ];
}

public static function getMinPriceRange()
{
    return [
    	''		   =>	'Min',
    	'25000000'   => 'Below 25 million',
        '50000000'   => '50 million',
        '100000000'   => '100 million',
        '200000000'   => '200 million',
        '250000000'   => '250 million',
        '350000000'   => '350 million',
        '500000000'  => '500 million',
        '600000000'  => '500 million And Above',
    ];
}

public static function getMaxPriceRange($minPrice)
{
    $range = [
    	'25000000'   => 'Below 25 million',
        '50000000'   => '50 million',
        '100000000'   => '100 million',
        '200000000'   => '200 million',
        '250000000'   => '250 million',
        '350000000'   => '350 million',
        '500000000'  => '500 million',
        '600000000'  => '500 million And Above',
    ];

    $result =  [];
    $flag = 0;
    foreach ($range as $key => $value) {
        if($key == $minPrice)
        {
            $flag = 1;
        }
        if($flag == 1)
        {
          $result[$key] = $value;
        }
    }
    return $result;
}

public static function getCountryPrice($price)
{
    $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }

        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        if(!empty($origin))
        {
            $originId = $origin->id;

            switch($originId)
            {
                case '158': 
                {
                    return $price;                    
                    break;
                }
                case '100': 
                {
                    return ($price * 1.83782);                    
                    break;
                }
                case '56': 
                {
                    return ($price * 0.529505);                    
                    break;
                }
                case '126': 
                {
                    return ($price * 1.14227);
                    break;
                }
                case '164': 
                {
                    return ($price * 1.81913);
                    break;
                }

                case '61': 
                {
                    return ($price * 8.86216);
                    break;
                }

                case '15': 
                {
                    return ($price * 24.8449);
                    break;
                }
                case '62': 
                {
                    return ($price * 91.7839);
                    break;
                }
                case '67': 
                {
                    return ($price * 150.001);
                    break;
                }
                case '69': 
                {
                    return ($price * 0.42348);
                    break;
                }
                case '73': 
                {
                    return ($price * 5.44895);
                    break;
                }
                case '79': 
                {
                    return ($price * 0.541517);
                    break;
                }
                case '83': 
                {
                    return ($price * 5.12645);
                    break;
                }
                case '84': 
                {
                    return ($price * 81.215);
                    break;
                }
                case '96': 
                {
                    return ($price * 5.17233);
                    break;
                }
                case '182': 
                {
                    return ($price * 1.40833);
                    break;
                }
                case '68': 
                {
                    return ($price * 0.998505);
                    break;
                }
                case '72': 
                {
                    return ($price * 2123.06);
                    break;
                }
                case '85': 
                {
                    return ($price * 5.28147);
                    break;
                }

                case '86': 
                {
                    return ($price * 1.8471);
                    break;
                }

                case '45': 
                {
                    return ($price * 16.5631);
                    break;
                }

                case '88': 
                {
                    return ($price * 219.249);
                    break;
                }

                case '156': 
                {
                    return ($price * 1.34625);
                    break;
                }

               default:  
               return ($price * 1.40833);
                    break;
            }
        }
        else
        {
            p($userCountry);
        }        

}

public static function getCurrencyCode($originId)
{
    if($originId !== '')
    { 

        switch($originId)
        {
            case '158': 
            {
                return 'GBP';                    
                break;
            }
            case '100': 
            {
                return 'AUD';                    
                break;
            }
            case '56': 
            {
                return 'BHD';                    
                break;
            }
            case '126': 
            {
                return 'EUR';
                break;
            }
            case '164': 
            {
                return 'CAD';
                break;
            }

            case '61': 
            {
                return 'CNY';
                break;
            }

            case '15': 
            {
                return 'EGP';
                break;
            }
            case '62': 
            {
                return 'INR';
                break;
            }
            case '67': 
            {
                return 'JPY';
                break;
            }
            case '69': 
            {
                return 'KWD';
                break;
            }
            case '73': 
            {
                return 'MYR';
                break;
            }
            case '79': 
            {
                return 'OMR';
                break;
            }
            case '83': 
            {
                return 'QAR';
                break;
            }
            case '84': 
            {
                return 'RUB';
                break;
            }
            case '96': 
            {
                return 'AED';
                break;
            }
            case '182': 
            {
                return 'USD';
                break;
            }
            case '68': 
            {
                return 'JOD';
                break;
            }
            case '72': 
            {
                return 'LBP';
                break;
            }
            case '85': 
            {
                return 'SAR';
                break;
            }

            case '86': 
            {
                return 'SGD';
                break;
            }

            case '45': 
            {
                return 'ZAR';
                break;
            }

            case '88': 
            {
                return 'LKR';
                break;
            }

            case '156': 
            {
                return 'CHF';
                break;
            }

           default:  
           return 'USD';
                break;
        }
    }
}



}
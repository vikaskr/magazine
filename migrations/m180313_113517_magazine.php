<?php

namespace app\migrations;
use app\commands\Migration;

class m180313_113517_magazine extends Migration

{
    public function getTableName()
    {
        return 'magazine';
    }

    public function getForeignKeyFields()
    {
        return [
          'origin_id' => ['origin','id'],
          'publisher_id' => ['publisher','id'],
          'language_id' => ['language','id'],
          'subject_id' => ['subject','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'issn'  =>  'issn',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'product_code' => $this->string(100),
            'subject_id' => $this->integer(100),
            'cover_image' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'issues_per_year' => $this->string(100),
            'issn' => $this->string(255)->notNull(),
            'periodicity' => $this->string(100)->notNull(),
            'publisher_id' => $this->integer(),
            'language_id' => $this->integer(100),
            'origin_id' => $this->integer()->notNull(),
            'estimated_delivery' => $this->string(255),
            'format' => $this->string(255),
            'promoted_by' => $this->string(255),
            'distributed_by' => $this->string(255),
            'supplied_by' => $this->string(255),
            'marketed_by' => $this->string(255),
            'description' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['slug','status','del_status'];
        $this->db->createCommand()->createIndex('uni_mag_sta_del', $this->getTableName(), $columns, true)->execute();
    }
}

<?php

namespace app\migrations;
use app\commands\Migration;

class m180313_113522_magazine_subject extends Migration
{
    public function getTableName()
    {
        return 'magazine_subject';
    }

    public function getForeignKeyFields()
    {
        return [
          'subject_id' => ['subject','id'],
          'magazine_id' => ['magazine','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'subject_id' => 'subject_id',
            'status' => 'status',
            'del_status'=> 'del_status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'magazine_id' => $this->integer()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['subject_id','magazine_id','status','del_status'];
        $this->db->createCommand()->createIndex('unique_magazine_subject', $this->getTableName(), $columns, true)->execute();
    }
}

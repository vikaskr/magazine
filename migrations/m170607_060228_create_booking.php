<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060228_create_booking extends Migration
{
    public function getTableName()
    {
        return 'booking';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'magazine' => 'magazine',
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'first_name' => $this->string(50),
            'last_name' => $this->string(50),
            'email' => $this->string(50),
            'mobile' => $this->string(50),
            'landline' => $this->string(),
            'order_id' => $this->string(), 
            'address' => $this->string(),
            'city' => $this->string(),
            'state' => $this->string(),
            'country' => $this->string(), 
            'pin_code' => $this->string(), 
            'delivery_address' => $this->string(), 
            'magazine' =>  $this->string(), 
            'quantity' =>  $this->string(), 
            'amount' => $this->integer(),
            'currency' => $this->string(),
            'txn_id' => $this->string(),
            'status' => "enum('booking','done','cancelled') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT 'booking'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}

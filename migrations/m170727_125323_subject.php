<?php

namespace app\migrations;
use app\commands\Migration;

class m170727_125323_subject extends Migration
{

   public $subjects = [
"Animals & Pets",
"Agriculture",
"Architecture",
"Art & Design",
"Auto & Cycles",
"Aviation",
"Ayurveda & Herbal",
"Business & Finance",
"Celebrity & Films",
"Children",
"Comics",

"Computers & Electronics",
"Cooking, Food & Beverage",
"Cricket",
"Craft & Hobbies",
"Culture",
"Education",
"Enrichment",
"Entertainment & TV",
"Fashion",
"Fiction",
"Gaming",

"General Interest",
"Health & Fitness",
"History",
"Hobbies & Puzzles",
"Home & Garden",
"Hotels & Hospitality",
"Industry & Trade",
"Interior Designing",
"Jewellery Design",
"Journals",
"Language Learning Skills",

"Lifestyle",
"Medical",
"Men's Interest",
"Music & Dance",
"Nature",
"Newspapers",
"News & Politics",
"Parenting & Family",
"Photography",
"Psychology",
"Real Estate",

"Religious & Spiritual",
"Salon & Spa",
"Science & Technology",
"Self Help",
"Special Needs",
"Sports & Recreation",
"Teen",
"Travel",
"Weddings",
"Women's Interest",
"Yoga",
    ];
    
   public function getTableName()
   {
        return 'subject';
   }

    public function getKeyFields()
    {
        return [
            'status' => 'status',
            'subject_code' => 'subject_code',
            'subject_name' => 'subject_name',
            'del_status'=> 'del_status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'subject_code' => $this->string(3)->defaultValue(NULL),
            'subject_name' => $this->string()->notNull(),
            'subject_slug' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    } 

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['subject_code','status','del_status'];
        $this->db->createCommand()->createIndex('unique_sub_slug_status_del_status', $this->getTableName(), $columns, true)->execute();
        $resultSet = [];
        foreach ($this->subjects as $subject)
        {
            $slug = \app\helpers\StringHelper::generateSlug($subject);
            $resultSet[] = [$subject,$slug];
        }
        $count = \Yii::$app->db->createCommand()->batchInsert('subject',
            ['subject_name','subject_slug'], $resultSet)->execute();
        echo $count . " records entered";
    }
}
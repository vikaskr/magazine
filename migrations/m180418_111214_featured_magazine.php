<?php

namespace app\migrations;
use app\commands\Migration;

class m180418_111214_featured_magazine extends Migration
{
    public function getTableName()
    {
        return 'featured_magazine';
    }

    public function getForeignKeyFields()
    {
        return [
          'magazine_id' => ['magazine','id'],
          'origin_id' => ['origin','id'],
        ];
    }
    
    public function getKeyFields()
    {
        return [
            'magazine_id'  =>  'magazine_id',
            'origin_id'  =>  'origin_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'magazine_id' => $this->integer()->notNull(),
            'origin_id' => $this->integer()->notNull(),
            'featured' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['magazine_id','origin_id','status','del_status'];
        $this->db->createCommand()->createIndex('uni_mag_pr_st_d', $this->getTableName(), $columns, true)->execute();
    }
}

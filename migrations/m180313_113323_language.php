<?php

namespace app\migrations;
use app\commands\Migration;
use app\helpers\StringHelper;


class m180313_113323_language extends Migration
{
    public $allLanguage = [
        ["Mandarin"],
        ["Spanish"],
        ["English"],
        ["Hindi"],
        ["Arabic"],
        ["Portuguese"],
        ["Bengali"],
        ["Russian"],
        ["Japanese"],
        ["Punjabi"],
        ["German"],
        ["Javanese"],
        ["Wu"],
        ["Malay"],
        ["Telugu"],
        ["Vietnamese"],
        ["Korean"],
        ["French"],
        ["Marathi"],
        ["Tamil"],
        ["Urdu"],
        ["Turkish"],
        ["Italian"],
        ["Yue"],
        ["Thai"],
        ["Gujarati"],
        ["Jin"],
        ["Southern Min"],
        ["Persian"],
        ["Polish"],
        ["Pashto"],
        ["Kannada"],
        ["Xiang"],
        ["Malayalam"],
        ["Sundanese"],
        ["Hausa"],
        ["Odia"],
        ["Burmese"],
        ["Hakka"],
        ["Ukrainian"],
        ["Bhojpuri"],
        ["Tagalog"],
        ["Yoruba"],
        ["Maithili"],
        ["Uzbek"],
        ["Sindhi"],
        ["Amharic"],
        ["Fula"],
        ["Romanian"],
        ["Oromo"],
        ["Igbo"],
        ["Azerbaijani"],
        ["Awadhi"],
        ["Gan Chinese"],
        ["Cebuano"],
        ["Dutch"],
        ["Kurdish"],
        ["Serbo-Croatian"],
        ["Malagasy"],
        ["Saraiki"],
        ["Nepali"],
        ["Sinhalese"],
        ["Chittagonian"],
        ["Zhuang"],
        ["Khmer"],
        ["Turkmen"],
        ["Assamese"],
        ["Madurese"],
        ["Somali"],
        ["Marwari"],
        ["Magahi"],
        ["Haryanvi"],
        ["Hungarian"],
        ["Chhattisgarhi"],
        ["Greek"],
        ["Chewa"],
        ["Deccan"],
        ["Akan"],
        ["Kazakh"],
        ["Northern Min"],
        ["Sylheti"],
        ["Zulu"],
        ["Czech"],
        ["Kinyarwanda"],
        ["Dhundhari"],
        ["Haitian Creole"],
        ["Eastern Min"],
        ["Ilocano"],
        ["Quechua"],
        ["Kirundi"],
        ["Swedish"],
        ["Hmong"],
        ["Shona"],
        ["Uyghur"],
        ["Hiligaynon"],
        ["Mossi"],
        ["Xhosa"],
        ["Belarusian"],
        ["Balochi"],
        ["Konkani"],
     ];   

    public function getTableName()
    {
        return 'language';
    }

    public function getKeyFields()
    {
        return [
            'language'  => 'language',
            'lang_code' => 'lang_code',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'lang_code' => $this->string(100),
            'language' => $this->string(100),
            'slug' => $this->string(100),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();

        $columns = ['slug','status','del_status'];
        $this->db->createCommand()->createIndex('unique_language_slug_del_status', $this->getTableName(), $columns, true)->execute();
        
        $allSubs = $this->allLanguage;
        $recordSet = [];
        foreach ($allSubs as $key => $value) {
            $langSlug = StringHelper::generateSlug($value[0]);
            $recordSet[$key] =  [$langSlug,$value[0],$langSlug];
        }
        $count = \Yii::$app->db->createCommand()->batchInsert('language',
            ['lang_code','language', 'slug'], $recordSet)->execute();
        echo $count ." rows affected";
    }

}

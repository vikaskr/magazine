<?php
namespace app\migrations;
use app\commands\Migration;

class m170610_121205_signup_mail extends Migration
{
    public function getTableName()
    {
        return 'signupmail';
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email'
                ];
    }

    public function getFields()
    {
        return [
            
            
            'email' => $this->string(50)->notNull(),
            
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}


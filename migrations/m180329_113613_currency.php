<?php

namespace app\migrations;
use app\commands\Migration;

class m180329_113613_currency extends Migration
{
    public function getTableName()
    {
        return 'currency';
    }

    public function getForeignKeyFields()
    {
        return [
          'origin_id' => ['origin','id'],
        ];
    }
    
    public function getKeyFields()
    {
        return [
            'name'  =>  'name'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'origin_id' => $this->integer()->notNull(),
            'name' => $this->string(100),
            'slug' => $this->string(100),
            'code' => $this->string(),
            'symbol' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['slug','status','del_status'];
        $this->db->createCommand()->createIndex('uni_cur_sl_d_st', $this->getTableName(), $columns, true)->execute();
    }
}

<?php

namespace app\components;

use Yii;
use yz\shoppingcart\DiscountBehavior;

class MyDiscount extends DiscountBehavior
{
    /**
     * @param \yz\shoppingcart\CostCalculationEvent $event
     */
    public function onCostCalculation($event)
    {
        $discount = 5;
        $event->discountValue = $event->price * ($discount / 100);
    }
}
?>
$(document).ready(function(){
	$('#mub-state').on('change',function(){
    var selected = $("#mub-state option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/users/user/get-city",
         data:{stateId:selected},
        success: function(result){
            $('#mubusercontact-city').children("option").remove();
            $('#mubusercontact-city').prop("disabled", false);
            $('#mubusercontact-city').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
            $('#mubusercontact-city').append(new Option(item.city_name, item.id));
        });}
    }); 
 });

  $('.read_data').click(function(){
      var id = $(this).attr('id');
           $.ajax({
                url:'/mub-admin/csvreader/xml/full-table',
                dataType:'json',
                type:'get',
                cache:true,
                data: {
                  id: id
                },
                success:  function (response) {
                    console.log("ok");
                },              
        });
  });

});
var approveComment = function(commentId)
{
    $.ajax({
         type:'POST',
         url:"/mub-admin/blog/comments/approve-comment",
         data:{commentId:commentId},
        success: function(result)
        {
            if(result)
            {
                alert('Selected Comment has been approved');
            }
            else
            {
               alert('Comment could not be approced by you'); 
            }
            location.reload();
        }
    }); 
}
var setModelAttribute = function(model,attribute,value,id)
{
    var dataArray = {model:model,attribute:attribute,value:value,id:id};
    $.ajax({
     type:'POST',
     url:"/mub-admin/dashboard/set-attribute",
     data:dataArray,
        success: function(result){
            if(result)
            {
                location.reload();
            }
        }
    }); 

};

var changeUserStatus = function(field,val,id)
{
    var models = [{name:'\\app\\models\\User',status:val},{name:'\\app\\models\\MubUser',status:val}];
    for (var i = models.length - 1; i >= 0; i--) {
      setModelAttribute(models[i].name,'status',val,id);  
    }
};
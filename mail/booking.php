<?php ?>
<!DOCTYPE html>
<html>
   <head>
      <title> User Login Email</title>
      <!--
         An email present from your friends at makeubig (@makeubigapp)
         
         Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
         It's highly recommended that you test using a service like makeubig (http://nursingjournals.online) and your own devices.
         
         Enjoy!
         
         -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <style type="text/css">
         /* CLIENT-SPECIFIC STYLES */
         body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
         table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
         img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
         /* RESET STYLES */
         img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
         table{border-collapse: collapse !important;}
         body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}
         /* iOS BLUE LINKS */
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: none !important;
         font-size: inherit !important;
         font-family: inherit !important;
         font-weight: inherit !important;
         line-height: inherit !important;
         }
         /* MOBILE STYLES */
         @media screen and (max-width: 525px) {
         /* ALLOWS FOR FLUID TABLES */
         .wrapper {
         width: 100% !important;
         max-width: 100% !important;
         }
         /* ADJUSTS LAYOUT OF LOGO IMAGE */
         .logo img {
         margin: 0 auto !important;
         }
         /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
         .mobile-hide {
         display: none !important;
         }
         .img-max {
         max-width: 100% !important;
         width: 100% !important;
         height: auto !important;
         }
         /* FULL-WIDTH TABLES */
         .responsive-table {
         width: 100% !important;
         }
         /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
         .padding {
         padding: 10px 5% 15px 5% !important;
         }
         .padding-meta {
         padding: 30px 5% 0px 5% !important;
         text-align: center;
         }
         .no-padding {
         padding: 0 !important;
         }
         .section-padding {
         padding: 50px 15px 50px 15px !important;
         }
         /* ADJUST BUTTONS ON MOBILE */
         .mobile-button-container {
         margin: 0 auto;
         width: 100% !important;
         }
         .mobile-button {
         padding: 15px !important;
         border: 0 !important;
         font-size: 16px !important;
         display: block !important;
         }
         }
         /* ANDROID CENTER FIX */
         div[style*="margin: 16px 0;"] { margin: 0 !important; }
      </style>
   </head>
   <body style="margin: 0 !important; padding: 0 !important;">
      <!-- HIDDEN PREHEADER TEXT -->
      <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
         Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...
      </div>
      <!-- HEADER -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
         <tr>
            <td bgcolor="#fff" align="center">
               <!--[if (gte mso 9)|(IE)]>
               <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                  <tr>
                     <td align="center" valign="top" width="500">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                           <tr>
                              <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                                 <a href="http://nursingjournals.online" target="_blank">
                                 <h1>Nursing Journals Online</h1>
                                 </a>
                              </td>
                           </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                     </td>
                  </tr>
               </table>
               <![endif]-->
            </td>
         </tr>
         <tr>
            <td bgcolor="#ffffff" align="center"  class="section-padding">
               <!--[if (gte mso 9)|(IE)]>
               <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                  <tr>
                     <td align="center" valign="top" width="500">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
                           <tr>
                              <td>
                                 <!-- HERO IMAGE -->
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td class="padding" align="center">
                                          <a href="http://nursingjournals.online" target="_blank">
                                          <h3>Thanks for Your Query At NursingJournals.online</h3>
                                          </a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <!-- COPY -->
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                             <center>
                                               <p style="font-size:30px"> Greetings from Nursing Journals</p>
                                                </center>
                                             </tr>
                                             <tr>
                                             <center>
                                                <p style="font-size:30px">Following are your Journal Details<p>
                                            </center>
                                             </tr>
                                             <?php 
                                                foreach ($cartItems as $key => $item){ ?>
                                             <tr>
                                                <td align="center" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #09295e; padding-top: 30px;" class="padding"><?= $item['title'];?></td>
                                                <td align="center" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #09295e; padding-top: 30px;" class="padding"><?= $item['inr_price']?></td>
                                             </tr>
                                             <?php }?>
                                             <tr><h2>Total Amount : <?=$price;?></h2></tr>
                                             </table>
                                             </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center">
                                          <!-- BULLETPROOF BUTTON -->
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                                <td align="center" style="padding-top: 25px;" class="padding">
                                                   <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                                <td bgcolor="#09295e" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding" >
                                   <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                                      <tr>
                                         <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#EEEEEE;">
                                            <a href="#" style="color:#FFFFFF ; text-decoration: none; ">
                                               <h3>Contact Us</h3>
                                            </a>
                                            <a href="#" style="color:#FFFFFF ; text-decoration: none; ">
                                            Knowledge Tree | Salarpuria Sattva Knowledge City | Unit 2 . Sy.No. 83/1 . Plot No-2 |<br /> Inorbit Mall Road | Raidurg Village | HITEC City | Hyderabad 500081 | Telangana | India
                                            </a>
                                            <br>
                                            <a href="#" style="color:#EEEEEE ; text-decoration: none;"> contact@nursingjournals.online</a>
                                            <span style="font-family: Arial, sans-serif; font-size: 12px; color:#EEEEEE;">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                                            <a href="#" style="color:#EEEEEE ; text-decoration: none;">9773723591 / 9773723592</a><br>
                                            <a href="#" style="color:#FFFFFF ; text-decoration: none;">
                                               <h2>&copy;NURSING JOURNALS ONLINE.</h2>
                                            </a>
                                         </td>
                                      </tr>
                                   </table>
                          </table>
         
   </body>
</html>
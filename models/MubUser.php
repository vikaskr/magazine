<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_user".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $domain
 * @property string $first_name
* @property string $last_name
* @property string $gender
* @property string $dob
 * @property string $username
 * @property string $password
 * @property string $organization
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property User $mubUser
 * @property MubUserAlbum[] $mubUserAlbums
 * @property MubUserContact[] $mubUserContacts
 * @property MubUserImages[] $mubUserImages
 * @property MubUserPage[] $mubUserPages
 * @property MubUserSocial[] $mubUserSocials
 * @property Post[] $posts
 * @property PostComment[] $postComments
 * @property PostComment[] $postComments0
 */
class MubUser extends \app\components\Model
{
    public $role;
    public $password_confirm;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['username', 'password','password_confirm','first_name','last_name'], 'required'],
            ['password_confirm', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"],
            [['gender', 'status', 'del_status'], 'string'],
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['domain', 'first_name', 'last_name'], 'string', 'max' => 50],
            [['username', 'password', 'organization'], 'string', 'max' => 100],
            ['username','checkActive'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_mub_user'] = ['domain', 'first_name', 'last_name', 'username', 'password', 'password_confirm','organization','status'];
        $scenarios['update_mub_user'] = ['domain', 'first_name', 'last_name', 'username', 'password','organization','status'];
        $scenarios['status_update'] = ['status'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'domain' => Yii::t('app', 'Domain'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'gender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Dob'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'organization' => Yii::t('app', 'Organization'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->where(['del_status' => '0']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserAlbums()
    {
        return $this->hasMany(MubUserAlbum::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserContacts()
    {
        return $this->hasOne(MubUserContact::className(), ['mub_user_id' => 'id'])->where(['mub_user_contact.del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserImages()
    {
        return $this->hasMany(MubUserImages::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserPages()
    {
        return $this->hasMany(MubUserPage::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserSocials()
    {
        return $this->hasMany(MubUserSocial::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUserState()
    {
        return $this->hasMany(UserStates::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['mub_user_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostComments()
    {
        return $this->hasMany(PostComment::className(), ['approved_by' => 'id'])->where(['del_status' => '0']);
    }

    
}

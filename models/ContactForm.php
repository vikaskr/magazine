<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_form".
 *
 * @property integer $id
 * @property string $name
 * @property string $sur_name
 * @property string $designation
 * @property string $department
 * @property string $organisation
 * @property string $address
 * @property string $city
 * @property string $location
 * @property string $state
 * @property string $pincode
 * @property string $country
 * @property string $email
 * @property string $telephone
 * @property string $mobile
 * @property string $website
 * @property string $contact_person
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 */
class ContactForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sur_name', 'address', 'city', 'state', 'country', 'email', 'mobile'], 'required'],
            [['email'],'email'],
            [['message', 'status', 'del_status'], 'string'],
            ['email', 'email'],
            [['mobile','telephone'],'number'],
            [['mobile','telephone'],'string', 'max' => 20 , 'min' => 6],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'sur_name', 'designation', 'department', 'organisation', 'address', 'city', 'location', 'state', 'pincode', 'country', 'website', 'contact_person'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sur_name' => 'Surname',
            'designation' => 'Designation',
            'department' => 'Department',
            'organisation' => 'Organisation',
            'address' => 'Address',
            'city' => 'City',
            'location' => 'Location',
            'state' => 'State',
            'pincode' => 'Pincode',
            'country' => 'Country',
            'email' => 'Email',
            'telephone' => 'Telephone',
            'mobile' => 'Mobile',
            'website' => 'Website',
            'contact_person' => 'Contact Person',
            'message' => 'Message',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }
}

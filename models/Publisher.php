<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publisher".
 *
 * @property integer $id
 * @property string $publisher
 * @property string $slug
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Magazine[] $magazines
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher', 'slug'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['publisher', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher' => 'Publisher',
            'slug' => 'Slug',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazines()
    {
        return $this->hasMany(Magazine::className(), ['publisher_id' => 'id']);
    }
}

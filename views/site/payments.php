<?php

    use yii\helpers\Url;        
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use app\helpers\PriceHelper;
    use yz\shoppingcart\ShoppingCart;
    use app\modules\MubAdmin\modules\csvreader\models\Origin;
    use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;

    $user = new \app\models\MubUser();
    $userContact = new \app\models\MubUserContact();
    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();
    $count = count($cartItems);
    if(!Yii::$app->user->isGuest){
    $mubId = Yii::$app->user->getId();
    $users = $user::find()->where(['user_id' => $mubId])->one();
    $id = $users->id;
    $userDetails = $userContact::find()->where(['mub_user_id' => $id])->one();
    $countryId = $userDetails->country;
    $country = $origin::find()->where(['id' => $countryId])->one();
    }

?>

<section class="category-banner-panel">

</section>

<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
    <ul>
    <li><a>Wide Range</a></li>
    <li><a>Great prices</a></li>
    <li><a>Best Discounts</a></li>
    <li><a>Quick Delivery</a></li>
    <li><a>24x7 Customer care</a></li>
    <li><a>Free Shipping</a></li>
    <li><a>Easy Renewals</a></li>
    </ul>
    </div>

</div>

<section class="product-main-outer-panel">

    <div class="row">
        
        <div class="col-xs-12 nopadding shopping-cart-main-outer">
        
        
         <div class="col-xs-12 nopadding shopping-cart-bottom-main">
          <div class="col-xs-12 nopadding shopping-cart-left-main nopadding checkout-guest-panel">
          
         
        <div class="col-xs-12 checkout-guest-outer nopadding">
 
        <div class="checkout-shipping-form-main-outer">
        <div class="col-xs-12 checkout-guest-inner-form">
        
        
        <!--BILLING START-->

        <div class="col-sm-4 col-md-4 col-xs-12 nopadding shipping-grid-outer shipping-grid-outer01">

        <h2>1. SHIPPING ADDRESS</h2> <?php if(!Yii::$app->user->isGuest){?>   
         <?php $form = ActiveForm::begin(['options' => ['method' => 'POST'],'action' => ['/site/payments']]); ?>

            <div class="contact-subject-field contact-name-field">
              <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'first_name')->textInput(['readOnly'=> true, 'value' => $users->first_name." ".$users->last_name, 'placeholder' => "Name", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'mobile')->textInput(['readOnly'=> true, 'value' => $userDetails->mobile, 'placeholder' => "Mobile", 'class' => 'login-popup-panel-main input'])->label(false); ?>
              </div>
              </div>
            </div>
            
            <div class="contact-subject-field contact-name-field">
                <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'email')->textInput(['readOnly'=> true, 'value' => $userDetails->email, 'placeholder' => "Email ID", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'address')->textInput(['readOnly'=> true, 'value' => $userDetails->address, 'placeholder' => "Address", 'class' => 'login-popup-panel-main input'])->label(false); ?>
              </div>
              </div>
            </div>
   		
            <div class="contact-subject-field contact-name-field shipping-country-select">
               <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'country')->textInput(['readOnly'=> true, 'value' => $country->origin_name, 'placeholder' => "Country", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'pin_code')->textInput(['readOnly'=> true, 'value' => $userDetails->pin_code, 'placeholder' => "Zip Code", 'class' => 'login-popup-panel-main input'])->label(false); ?>
              </div>
              </div>
            </div>
            <div class="contact-subject-field contact-name-field">
               <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'city')->textInput(['readOnly'=> true, 'value' => $userDetails->city, 'placeholder' => "City", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'state')->textInput(['readOnly'=> true, 'value' => $userDetails->state, 'placeholder' => "State", 'class' => 'login-popup-panel-main input'])->label(false); ?>
              </div>
              </div>
            </div>
            
           <div class="col-xs-12 nopadding shipping-address-same-con">
          	 	<input type="checkbox" id="ship" name="ship" />
            	<label id="label-ship" for="ship">Ship to the same address</label>
            </div>
            <?php ActiveForm::end();?>
             <?php $form = ActiveForm::begin(['options' => ['method' => 'POST', 'id' => 'ship-address'],'action' => ['/site/payments']]); ?>

           <div class="col-xs-12 nopadding shipping-address-form" id="show-form-bottom">

            <div class="contact-subject-field contact-name-field">
              <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'first_name')->textInput([ 'placeholder' => "First Name", 'class' => 'login-popup-panel-main input','id' => 'shipFirstName'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'last_name')->textInput([ 'placeholder' => "Last Name", 'class' => 'login-popup-panel-main input','id' => 'shipLastName'])->label(false); ?>
              </div>
              </div>
            </div>
                
                <div class="contact-subject-field contact-name-field">
                    <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'email')->textInput([ 'placeholder' => "Email", 'class' => 'login-popup-panel-main input','id' => 'shipEmail'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'mobile')->textInput([ 'placeholder' => "Mobile", 'class' => 'login-popup-panel-main input','id' => 'shipMobile'])->label(false); ?>
              </div>
              </div>
                </div>
            
                <div class="contact-subject-field contact-name-field shipping-country-select ">
                   <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'country')->textInput([ 'placeholder' => "Country", 'class' => 'login-popup-panel-main input','id' => 'shipCountry'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'pin_code')->textInput([ 'placeholder' => "Pincode/ZipCode", 'class' => 'login-popup-panel-main input','id' => 'shipPincode'])->label(false); ?>
              </div>
              </div>
                </div>

                <div class="contact-subject-field contact-name-field">
                   <div class="row">
                <div class="col-md-6">
                <?= $form->field($booking, 'city')->textInput([ 'placeholder' => "City", 'class' => 'login-popup-panel-main input','id' => 'shipCity'])->label(false); ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($booking, 'state')->textInput([ 'placeholder' => "State", 'class' => 'login-popup-panel-main input','id' => 'shipState'])->label(false); ?>
              </div>
              </div>
                </div>
                <div class="contact-subject-field contact-name-field">
                   <div class="row">
                <div class="col-md-12">
                <?= $form->field($booking, 'address')->textInput([ 'placeholder' => "Address", 'class' => 'login-popup-panel-main input','id' => 'shipAddress'])->label(false); ?>
                </div>
              </div>
                </div>
           </div>
           <div class="contact-subject-field"><input name="" type="submit" value="Continue" id="continue-button01"></div>

       <?php ActiveForm::end();} else
       {?>
        
        <?php }?>
        </div>
       
        <!--BILLING END-->
            <div class="col-sm-4 col-md-4 col-xs-12 nopadding shipping-grid-outer shipping-grid-outer02 shipping-opacity-div">
             <div class="col-xs-12 nopadding shipping-payment-tabs-panel">
            <h2>2. Review Your Order</h2>
             
             <div class="col-xs-12 col-sm-12 col-md-12 nopadding shopping-cart-right-main shipping-review-rightbar">
             <div class="shopping-summary-outer">
          <h3>Summary</h3>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Subtotal: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?= floor($cart->getCost());?></span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Estimated Shipping: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  0</span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Estimated tax: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  0</span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Discount Value: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?php if(!empty(substr($discValue, 0, -3))){
            echo substr($discValue, 0, -3);
          }else{?> 0
         <?php };?></span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Amount Payable: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>   <?= floor($cart->getCost());?></span></div>
          <!-- <div class="col-xs-12 nopadding shopping-review-promo-outer"><input name="" type="text" placeholder="Do You have a Promo Code?"><button value="Submit">Apply</button></div> -->
          </div>
          
       
          <div class="col-xs-12 nopadding review-product-in-bag-outer">
            <h6>In Your Bag </h6>
              <!-- <div class="review-edit-con"><a href="#"><i class="fa fa-edit"></i> Edit</a></div> -->
              <?php 
             foreach($cartItems as $item){
               $itemCartId = $item['id'];
               $itemCartName = $item['name'];
            ?>
              <div class="col-xs-12 nopadding review-bag-inner" style="padding-bottom: 10px;">
                <div class="review-bag-pic"><?php
                $appPath = \Yii::getAlias('@app'); 
                if(file_exists($appPath."/images/".$item['cover_image'].".jpg"))
                {
                  $extension = '.jpg';
                }
                else
                {
                  $extension = '.png';
                }
                $quant = $item->getQuantity();
                ?>
                  <img src="/images/<?= $item['cover_image'].$extension;?>" alt="pic"></div>
                  <div class="review-bag-content">
                    <samp><?= $itemCartName;?></samp><br/><br/>
                    <span class="review-quantity-con">QTY  : <?= $item->getQuantity();?></span>
                    <span class="review-quantity-con">Annual Subscription</span>
                  </div>
              </div>
              <div class="contact-subject-field">

            </div>
          <?php }?>
          </div>
          <div class="contact-subject-field"><input name="" type="submit" value="Continue" id="continue-button02"></div>
          
          </div>
                                     
          </div>
        </div>   
         <!--SHIPPING PAYMENT START-->
        <div class="col-sm-4 col-md-4 col-xs-12 nopadding shipping-grid-outer shipping-grid-outer03 shipping-opacity-div">
     
          <div class="col-xs-12 nopadding shipping-payment-tabs-panel">
             
             <h2>3. Payment Method</h2>                                
          </div>
           <?php if(!empty($cartItems)){?>

            <div class="col-xs-12 nopadding ship-payment-con">
              <span class="col-xs-6 nopadding ship-payment-left paypal-shipping-input"> 
                <input type="radio" id="shipp-payment03" name="shipp-payment" value="payment" checked="" /> 
                <label for="shipp-payment03">CCAvenue</label>
              </span>
              <span class="col-xs-4 nopadding ship-payment-right">
                <img src="/images/img_paypal_icon.png" alt="paypal pic" class="paypal-pic-shipping">
              </span>
            </div>

            <div class="col-xs-12 nopadding ship-payment-con">
            <span class="col-xs-12 nopadding ship-payment-left">
             <input type="radio" id="shipp-payment04" name="shipp-payment" value="check" />
              <label for="shipp-payment04">Check Payment</label>
            </span>
            </div>
            
           <?php            
          $form = ActiveForm::begin(['options' => ['id' => 'confirm-payment', 'name' => 'ccavenue','method' => 'POST'],'action' => '/Non_Seamless_kit/ccavRequestHandler.php']);
          $mechentId = "145130";
        ?>
       
        <input type="hidden" name="txn_id" id="txn_id" value="<?= isset($users->first_name)? $users->first_name: ''.'_'.time();?>" />
        
       
        <input type="hidden" id="merchant_id" name="merchant_id" value="<?= $mechentId;?>"/>
        
       
        <input type="hidden" id="order_id" name="order_id" value="<?= isset($users->first_name)? $users->first_name: ''.'_'.time();?>"/>
        
       
        <input type="hidden" id="amount" name="amount" value="<?= floor($cart->getCost());?>"/>
        
       
         <input type="hidden" id="currency" name="currency" value="<?php echo PriceHelper::getCurrencyCode($origin->id);?>"/>
        
       
         <input type="hidden" id="redirect_url" name="redirect_url" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/success.php' ;?>"/>
        
       
         <input type="hidden" id="cancel_url" name="cancel_url" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/failure.php';?>"/>
        
       
         <input type="hidden" id="language" name="language" value="EN"/>
          
           
             <input type="hidden" id="first_name" name="billing_name" value="<?= isset($users->first_name)? $users->first_name: '';?>"/>

             <input type="hidden" id="last_name" name="last_name" value="<?= isset($users->last_name)? $users->last_name: '';?>"/>

             <input type="hidden" id="magazine" name="magazine" value="<?= $itemCartName;?>"/>

             <input type="hidden" id="quantity" name="quantity" value="<?= $item->getQuantity();?>"/>
           
             <input type="hidden" id="address" name="billing_address" value="<?= isset($userDetails->address)? $userDetails->address: '';?>"/>
            
             <input type="hidden" id="city" name="billing_city" value="<?= isset($userDetails->city)? $userDetails->city: '';?>"/>
                       
             <input type="hidden" id="state" name="billing_state" value="<?= isset($userDetails->state)? $userDetails->state: '';?>"/>
            
             <input type="hidden" id="pin_code" name="billing_zip" value="<?= isset($userDetails->pin_code)? $userDetails->pin_code: '';?>"/>
            
             <input type="hidden" id="country" name="billing_country" value="<?= isset($country->origin_name)? $country->origin_name: '';?>"/>
            
             <input type="hidden" id="mobile" name="billing_tel" value="<?= isset($userDetails->mobile)? $userDetails->mobile: '';?>"/>

             <input type="hidden" id="landline" name="landline" value="<?= isset($userDetails->mobile)? $userDetails->mobile: '';?>"/>
           
             <input type="hidden" id="email" name="billing_email" value="<?= isset($userDetails->email)? $userDetails->email: '';?>"/>           
           
              <input type="hidden" id="delivery_name" name="delivery_name" value="<?= isset($users->first_name)? $users->first_name: '';?>"/>
            
           
              <input type="hidden" id="delivery_address" name="delivery_address" value="<?= isset($userDetails->address)? $userDetails->address: '';?>"/>
           
              <input type="hidden" id="delivery_city" name="delivery_city" value="<?= isset($userDetails->city)? $userDetails->city: '';?>"/>
            
           
              <input type="hidden" id="delivery_state" name="delivery_state" value="<?= isset($userDetails->state)? $userDetails->state: '';?>"/>
            
          
              <input type="hidden" id="delivery_zip" name="delivery_zip" value="<?= isset($userDetails->pin_code)? $userDetails->pin_code: '';?>"/>
            
           
              <input type="hidden" id="delivery_country" name="delivery_country" value="<?= isset($country->origin_name)? $country->origin_name: '';?>"/>
            
              <input type="hidden" id="delivery_tel" name="delivery_tel" value="<?= isset($userDetails->mobile)? $userDetails->mobile: '';?>"/>
            
           
             <input type="hidden" id="merchant_param1" name="merchant_param1" value="additional Info."/>
            
           
             <input type="hidden" id="merchant_param2" name="merchant_param2" value="additional Info."/>
            
       
             <input type="hidden" id="merchant_param3" name="merchant_param3" value="additional Info."/>
            
           
             <input type="hidden" id="merchant_param4" name="merchant_param4" value="additional Info."/>
            
           
             <input type="hidden" id="" name="merchant_param5" value="additional Info."/>
        
       
             <input type="hidden" id="promo_code" name="promo_code" value=""/>
        
        
             <input type="hidden" id="customer_identifier" name="customer_identifier" value=""/>
        
            <div class="contact-subject-field">
              <input type="submit" id="send" value="Place Order">
            </div>
         
     <?php ActiveForm::end(); }?>
        </div>
        <!--SHIPPING PAYMENT END-->
        
        <!--REVIEW START-->
    
                               
       </div>
         
                           
        </div>

       </div>
    </div> 
</div>
   
</div>
</section>



<?php 
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\modules\MubAdmin\modules\csvreader\models\Language;
use app\modules\MubAdmin\modules\csvreader\models\Subject;
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\Publisher;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
$cart = new ShoppingCart();
$cartItems = $cart->getPositions();
$userOrigin = ip_info(); 
$magazines = new Magazine();
$subject = new Subject();
$category = $magazineDetails->subject_id;
$magId = $magazineDetails->id;
$nationalMagazines = $magazines::find()->where(['del_status' => '0','subject_id' => $category])->all();
$sub = $subject::find()->where(['del_status' => '0','id' => $category])->one();
$originId = $origin->id;
    $gbpPrice = MagazinePrice::find()->where(['magazine_id' => $magId,'origin_id' => $originId])->one();
    $disc = $gbpPrice->discount_percentage;
    $gbp = $gbpPrice->price;
    $price = ceil(floatval(PriceHelper::getCountryPrice($gbp)));

?>
<style type="text/css">
#mgQuant{
    padding-left: 5px;
    padding-right: 5px;
}
.imgborder {
   display:inline-block;
   position:relative;
   border:1px solid #ccc;
   padding:5px;
   background:#f2f2f2;
   margin-bottom:30px;
}
.s1 {
   -webkit-box-shadow: 0 28px 16px -26px rgba(0, 0, 0);
   -moz-box-shadow: 0 28px 16px -26px rgba(0, 0, 0);
   box-shadow: 0 28px 16px -26px #545252;
}</style>
<script language="JavaScript" type="text/javascript">
function displayAnswer(divID)
{
if(document.getElementById(divID).style.display == 'block')
{
var closeAnswer = true;
}
document.getElementById('answer1').style.display = 'none';
document.getElementById('answer2').style.display = 'none';
document.getElementById('answer3').style.display = 'none';
document.getElementById('answer4').style.display = 'none';
document.getElementById('answer5').style.display = 'none';
if(!closeAnswer)
{
document.getElementById(divID).style.display = '';
}
return false;
}
</script>

<section class="product-main-outer-panel product-main-content">
<div class="row">
<div class="col-xs-12 nopadding product-main-inner-panel">
<div class="col-sm-4 col-md-4 col-xs-4 col-lg-5 nopadding product-main-top-left-con text-center">
<div class="col-xs-12 nopadding product-left-main-pic">
<div class="bzoom_wrap">
        <ul id="bzoom">
            <li>
                <?php
                $appPath = \Yii::getAlias('@app'); 
                if(file_exists($appPath."/images/".$magazineDetails->cover_image.".jpg"))
                {
                    $extension = '.jpg';
                }
                else
                {
                    $extension = '.png';
                }
                ?>
                <img class="bzoom_thumb_image" src="/images/<?= $magazineDetails->cover_image.$extension;?>" title="first img" />
                <?php
                $appPath = \Yii::getAlias('@app'); 
                if(file_exists($appPath."/images/".$magazineDetails->cover_image.".jpg"))
                {
                    $extension = '.jpg';
                }
                else
                {
                    $extension = '.png';
                }
                ?>
                <img class="bzoom_big_image" src="/images/<?= $magazineDetails->cover_image.$extension;?>"/>
            </li>
            <li>
                <img class="bzoom_thumb_image" src="/images/<?= $magazineDetails->cover_image;?>.jpg" title="first img" />
                <img class="bzoom_big_image" src="/images/<?= $magazineDetails->cover_image;?>.jpg"/>
            </li>
        </ul>
    </div><!--<img src="/images/img_quick_popup_pic.jpg"  alt="pic">-->
    </div>
<div class="col-xs-12 nopadding free-delivery-con">
<img src="/images/img_free_delivery.jpg" alt="pic">
<p>Your subscription will start with the next available issue. Kindly expect the delivery in 4-6 weeks</p>
</div>
</div>
<div class="col-sm-8 col-md-8 col-xs-8 col-lg-7 nopadding product-main-top-right-con">
<?php
    if(!isset($userOrigin['country'])||($userOrigin['country'] == ''))
    {
        $userOrigin['country'] = 'UNITED STATES OF AMERICA';
    } 
    $originName = Origin::find()->where(['del_status' => '0','id' => $magazineDetails->origin_id])->one();
    $language = new Language();
    $languageName = $language::find()->where(['del_status' => '0', 'id' => $magazineDetails->language_id])->one(); 
    $publisher = new Publisher();
    $publisherName = $publisher::find()->where(['del_status' => '0', 'id' => $magazineDetails->publisher_id])->one(); 
 ?>
    <h2><?= $magazineDetails->name; ?></h2>
    <div class="col-xs-12 nopadding product-origin-main">
    <span>Frequency: <?= $magazineDetails->issues_per_year; ?> issues per year </span>
    <span>Periodicity: <?= $magazineDetails->periodicity; ?></span>
    <span>Origin: <?= $originName->origin_name; ?></span>
    </div>
    <div class="col-xs-12 nopadding product-bio-con">
    <?php if($magazineDetails->description != 'NA'){?><p><?= $magazineDetails->description; ?></p><?php }?>
    </div>


<div class="col-xs-12 nopadding product-nation-main-tabs-panel">
<ul>
<li><a href="javascript:void(0);" class="national-click-tab product-nation-main-tabs-active ">National</a></li>
<li><a href="javascript:void(0);" class="international-click-tab">International</a></li>
</ul>

<div class="col-xs-12 nopadding product-national-tab-content" id="national-tab">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th>Subscription Period</th>
    <th>Format</th>
    <th>Price</th>
    <th>Discount</th>
    <th>You Pay</th>
    <th>QTY</th>
    <th>&nbsp;</th>
  </tr>
  <tr>
    <td>ONE YEAR</td>
    <td><?= $magazineDetails->format;?></td>
    <td><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= $price;?></td>
    <td><?= $disc?>%</td>
    <td><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= number_format($magazineDetails->getPrice());?></td>
    <td>
    <span style="padding-left: 5px;"><i class="fa fa-minus decrementcart" id="dec_<?= $magazineDetails->id;?>"></i></span>
  <span id="mgQuant_<?= $magazineDetails->id;?>" style="padding-left: 5px; padding-right: 5px;">1</span>
  <span style="padding-right: 10px;"><i class="fa fa-plus incrementcart" id="inc_<?= $magazineDetails->id;?>"></i></span>
    </td>
    <?php
        $alreadyInCart = $cart->getPositionById($magazineDetails->id);
        ?>
    <td>
        <a class="btn-updateaddcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $magazineDetails->id;?>" style="cursor: pointer;">Add To Cart</a>
        <a href="/site/checkout" target="_blank" class="btn-addcart <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $magazineDetails->id;?>">View Cart</a>
    </td>
  </tr>  
</table>
<table class="national-bottom-text" width="100%">
    <tr>
        <td>Wide Range | Great Prices | Best Discounts | Quick Delivery | 24 X 7 Customer Care | Free Shipping | Easy Renewals</td>
    </tr>
</table>
</div>


<div class="col-xs-12 nopadding product-national-tab-content product-international-tab-content" id="international-tab" style="display:none;">

<div class="col-sm-12 col-md-12 col-xs-12 nopadding product-internetinal-right country-select-con">
<label>Select Country <span style="font-size:12px!important;color:red">(* Only Deliverable to address in Selected Country.)</span></label>
<select id="mySelect">
<option value="0">Select Country</option>
<option value="Afghanistan">Afghanistan</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Antigua and Barbuda">Antigua and Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas, The">Bahamas, The</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Brazil">Brazil</option>
<option value="Brunei">Brunei</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burma">Burma</option>
<option value="Burundi">Burundi</option>
<option value="Cabo Verde">Cabo Verde</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo, Democratic Republic of the">Congo, Democratic Republic of the</option>
<option value="Congo, Republic of the">Congo, Republic of the</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Cote d'Ivoire">Cote d'Ivoire</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Curacao">Curacao</option>
<option value="Cyprus">Cyprus</option>
<option value="Czechia">Czechia</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="East Timor (see Timor-Leste)">East Timor (see Timor-Leste)</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="Gabon">Gabon</option>
<option value="Gambia, The">Gambia, The</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Greece">Greece</option>
<option value="Grenada">Grenada</option>
<option value="Guatemala">Guatemala</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-Bissau">Guinea-Bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Holy See">Holy See</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran">Iran</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea, North">Korea, North</option>
<option value="Korea, South">Korea, South</option>
<option value="Kosovo">Kosovo</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Laos">Laos</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libya">Libya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macau">Macau</option>
<option value="Macedonia">Macedonia</option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall">Marshall Islands</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mexico">Mexico</option>
<option value="Micronesia">Micronesia</option>
<option value="Moldova">Moldova</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Namibia">Namibia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="North Korea">North Korea</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau">Palau</option>
<option value="Palestinian">Palestinian Territories</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Qatar">Qatar</option>
<option value="Romania">Romania</option>
<option value="Russia">Russia</option>
<option value="Rwanda">Rwanda</option>
<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option value="Saint Lucia">Saint Lucia</option>
<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="Sao Tome and Principe">Sao Tome and Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Sint Maarten">Sint Maarten</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="South Korea">South Korea</option>
<option value="South Sudan">South Sudan</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syria">Syria</option>
<option value="Taiwan">Taiwan</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania">Tanzania</option>
<option value="Thailand">Thailand</option>
<option value="Timor-Leste">Timor-Leste</option>
<option value="Togo">Togo</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States of America">United States of America</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnam">Vietnam</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
</select>

<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="country-tab">
  
</table>
<table class="national-bottom-text" width="100%">
    <tr>
        <td>Wide Range | Great Prices | Best Discounts | Quick Delivery | 24 X 7 Customer Care | Free Shipping | Easy Renewals</td>
    </tr>
</table>
</div>
</div>

</div>

<div class="col-xs-12 nopadding product-share-social-outer">
<div class="col-xs-6 col-md-6 col-sm-6 nopadding product-social-media-con">Share in Social media <a href="#"><i class="fa fa-facebook-f"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-pinterest"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a></div>
<!-- <div class="col-xs-6 col-md-6 col-sm-6 nopadding product-wishlist-con"><span><a href="#"><i class="fa fa-heart"></i> Wishlist</a></span>| <span class="product-email-address-button"><i class="fa fa-envelope"></i> <input name="#" type="text" placeholder="Email Your Friend"></span> </div> -->
</div>

<!--<div class="col-xs-12 nopadding product-addtocart-outer">
<ul>
<li><a href="#"><i class="fa fa-minus"></i></a></li>
<li><a href="#">1</a></li>
<li><a href="#"><i class="fa fa-plus"></i></a></li>
</ul>-->

<!--<span class="product-addtocart-button"><a href="#"><i class="fa fa-shopping-cart"></i> Add To Cart</a></span>-->


</div>


</div>


<div class="col-xs-12 nopadding product-bottom-tabs-main-outer">
<ul>
<li><a  href="javascript:;" class="anchor product-bottom-tabs-active" onClick="displayAnswer('answer1');">Description</a></li>
<li><a  href="javascript:;" class="anchor" onClick="displayAnswer('answer2');">Details</a></li>
<li><a  href="javascript:;" class="anchor" onClick="displayAnswer('answer3');">Payment Option</a></li>
<li><a  href="javascript:;" class="anchor" onClick="displayAnswer('answer4');">Advertise</a></li>
<li><a  href="javascript:;" class="anchor" onClick="displayAnswer('answer5');">Review(0)</a></li>
</ul>

<div class="col-xs-12 nopadding product-bottom-description-outer-con">
<div class="col-xs-12 nopadding product-description-con" id="answer1" style="display:block">
<?php if($magazineDetails->description != 'NA'){?><p><?= str_replace(".",".<br/>",$magazineDetails->description); ?></p><?php }?>
</div>
<div class="col-xs-12 nopadding product-description-con" id="answer2" style="display:none">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th>Category:</th>
    <th><?= $sub->subject_name; ?></th>
  </tr>
  <?php if($magazineDetails->name != 'NA'){?>
  <tr>
    <td>Title: </td>
    <td><?= $magazineDetails->name; ?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->issn != 'NA'){?>
  <tr>
  <td>ISSN:</td>
  <td><?= $magazineDetails->issn; ?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->issues_per_year != 'NA'){?>
   <tr>
  <td>Issues per year: </td>
  <td><?= $magazineDetails->issues_per_year; ?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->periodicity != 'NA'){?>
   <tr>
  <td>Periodicity: </td>
  <td><?= $magazineDetails->periodicity; ?></td>
  </tr>
  <?php }?>
  <?php if($originName->origin_name != 'NA'){?>
    <tr>
  <td>Origin: </td>
  <td><?= $originName->origin_name; ?></td>
  </tr>
   <?php }?>
  <?php if($languageName->language != 'NA'){?>
    <tr>
  <td>Language: </td>
  <td><?= $languageName->language;?></td>
  </tr>
   <?php }?>
  <?php if($publisherName->publisher != 'NA'){?>
    <tr>
  <td>Publisher: </td>
  <td><?= $publisherName->publisher;?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->promoted_by != 'NA'){?>
    <tr>
  <td>Promoted by: </td>
  <td><?= $magazineDetails->promoted_by;?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->distributed_by != 'NA'){?>
    <tr>
  <td>Distributed by: </td>
  <td><?= $magazineDetails->distributed_by;?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->marketed_by != 'NA'){?>
    <tr>
  <td>Marketed by: </td>
  <td><?= $magazineDetails->marketed_by;?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->supplied_by != 'NA'){?>
    <tr>
  <td>Supplied by: </td>
  <td><?= $magazineDetails->supplied_by;?></td>
  </tr>
  <?php }?>
  <?php if($magazineDetails->estimated_delivery != 'NA'){?>
    <tr>
  <td>Estimated Delivery: </td>
  <td><?= $magazineDetails->estimated_delivery;?></td>
  </tr>
  <?php }?>
</table>

</div>
<div class="col-xs-12 nopadding product-description-con product-payment-option" id="answer3" style="display:none">
    <div class="col-xs-8">
        <p><strong>You can make payments by:</strong></p>
        <p>All MasterCard &amp; Visa Credit Cards (Domestic)</p>
        
        <p><strong>8 Major bank's EMI options</strong>
        SBI / ICICI / HDFC / Axis / Kotak / IndusInd / HSBC / Central Bank of India</p>
        
        <p><strong>100+ Debit Cards</strong>
        MasterCard / Visa / Maestro / RuPay</p>
        
        <p><strong>53+ Netbanking</strong></p>
        
        <p><strong>4 Cash Cards</strong>
        ITZCash / ICash / Oxicash / PayCash</p>
        
        <p><strong>8 Wallets</strong>
        Paytm / MobiKwik / JioMoney / PayZapp / Jana Cash / Freecharge / SBI Buddy / The Mobile Wallet</p>
        
        <p><strong>46 Bank's IMPS</strong>
        MasterCard &amp; Visa Credit Cards (International)
        American Express / Amex EMI, JCB &amp; Diners Club</p>
        
        <p><strong>Library &amp; Corporates can make payment by </strong>Bank Transfer / Demand Draft / Cheque</p>
    </div>
    <div class="col-xs-4">
      <img src="/images/payment-options.jpg" alt=""/> 
    </div>
</div>

<div class="col-xs-12 nopadding product-description-con" id="answer4" style="display:none">
<div class="col-xs-12 product-advertise-pic"><img src="/images/advertise1.jpg" alt=""/> </div>
<div class="product-advertise-con">
<h3>If You Wish To Advertise In Any Publication, Please Fill This Form And Send It To Us.<strong> We Will Connect You With The Concerned Team.</strong></h3>

<form action="#" method="get">
<div class="col-xs-12 nopadding product-advertise-form">
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Name"><span class="required-con">*</span></div> 
<div class="col-sm-6 col-md-6 col-xs-6 product-advertise-form-field nopadding"><input name="" type="text" placeholder="Designation"></div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Department"></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Organisation"> <span class="required-con">*</span></div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner address-textarea"><input name="" type="text" placeholder="Address"><span class="required-con">*</span></div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="City"> <span class="required-con">*</span></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Pincode/ Zipcode"><span class="required-con">*</span></div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="State"> <span class="required-con">*</span></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"> <input name="" type="text" placeholder="Country"><span class="required-con">*</span></div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Mobile"><span class="required-con">*</span></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"> <input name="" type="text" placeholder="Telephone"><span class="required-con">*</span></div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"> <input name="" type="text" placeholder="Email"><span class="required-con">*</span></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Website"> </div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner address-textarea02"><textarea rows="" cols="" placeholder="Write Your Message"></textarea></div>
<div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button"><input type="submit" name="submit"></div>
</div>
</form>
</div>

</div>

<div class="col-xs-12 nopadding product-description-con product-review-con" id="answer5" style="display:none">
<div class="product-advertise-con">
<h3>Be the first to review<strong> "<?= $magazineDetails->name; ?>"</strong></h3>

<h4>Your Rating<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></h4>
<form action="#" method="get">
<div class="col-xs-12 nopadding product-advertise-form">
<div class="col-xs-12 nopadding product-advertise-form-inner address-textarea02"><textarea rows="" cols="" placeholder="Your Review"></textarea></div>
<div class="col-xs-12 nopadding product-advertise-form-inner">
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"><input name="" type="text" placeholder="Name"></div>
<div class="col-sm-6 col-md-6 col-xs-6  product-advertise-form-field nopadding"> <input name="" type="text" placeholder="Email"> </div>
</div>
<div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button"><input type="submit" name="submit"></textarea></div>
</div>
</form>
</h3>
</div>


</div>
</div>

</div>


</div>
</div>
</div>
</section>


<div class="experience-world-main-outer-panel">
<div class="col-xs-12 nopadding experience-world-main-inner">
<h2 class="main-heading-text">you may find these <span>magazines of interest!</span></h2>
<div class="related-products-section">
 <div id="demo">
    <div  class="owl-carousel owl-demo">
        <?php 
          foreach ($nationalMagazines as $value) {  
           $proId = $value['id']; 
                            if( $proId !== $magId){                     
          $appPath = \Yii::getAlias('@app'); 
            if(file_exists($appPath."/images/".$value['cover_image'].".jpg"))
            {
              $extension = '.jpg';
            }
            else
            {
              $extension = '.png';
            } ?>
      <div class="item"><a href="/site/product?name=<?= $value['slug']?>"><img src="/images/<?= $value['cover_image'].$extension;?>" alt="logo" height="330" width="252" class="imgborder s1"></a></div>
      <?php } } ?>
      
    </div>
  </div>      
</div>
</div>
</div>


<div class="vfx-counter-block">
  <div class="vfx-item-container-slope vfx-item-bottom-slope vfx-item-left-slope"></div>
    <div class="vfx-item-counter-up">
        <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-file"></i></div>
            <div class="text_number">Wide </div>
            <div class="counter_text">Range of Magazines</div>
          </div>
        </div>
         <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-users"></i></div>
             <div class="text_number">View </div>
            <div class="counter_text">Collection</div>
          </div>
        </div>
         <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th"></i></div>
             <div class="text_number">Subscribe </div>
            <div class="counter_text">Magazines</div>
          </div>
        </div>
        <div class="vfx-counter-main">
          <div class="vfx-item-countup last-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th-list"></i></div>
             <div class="text_number">Renew</div>
            <div class="counter_text">Subscription</div>
          </div>
        </div>
        <div class="vfx-counter-main">
          <div class="vfx-item-countup last-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th-list"></i></div>
            <div class="text_number ">Easy</div>
            <div class="counter_text">Ordering</div>
          </div>
        </div>

    </div>
  </div>

<!--POPUP END-->


<!-- Scripts --> 

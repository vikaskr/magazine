<section class="category-banner-panel">

</section>

<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           

<!--PRODUCT BANNER START-->
<!--<section class="product-main-banner-panel">
<h1>Working Mom</h1>
</section>-->
<!--PRODUCT BANNER  END--> 


<!--PRODUCT PAGE START-->
<section class="product-main-outer-panel">

    <div class="row">
    	<!--<div class="by-sector-top-con col-xs-12">
        	<ul>
        		<li>WIDE RANGE </li> 
                <li>GREAT PRICES</li>
                <li>BEST DISCOUNTS</li>
                <li>QUICK DELIVERY</li>
                <li>24X7 CUSTOMER CARE</li>
                <li>FREE SHIPPING</li>
                <li>EASY RENEWALS</li>
            </ul>
        </div>-->
        <!--<h2 class="magazines-by-sectors-heading col-xs-12">Magazines by sectors</h2>-->
       
        <div class="col-xs-12 nopadding product-section-con">
         <h2 class="main-heading-text">Magazines by <br><span class="categories-button"><img src="/images/img_sectors_button.png" alt="categories pic"></span></h2>
       	  <div class="col-md-3">
            <div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-1.jpg" alt=""/> 
                <h3>Doctor’s Office</h3>
                <p>Tired of the patients twiddling their thumbs at the doctor’s office?? Magazines World has taken care of that. Along with getting a concession on our magazines, you get better services as well.</p>
                <a href="#con1">Find out more</a>
            </div>
          </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-2.jpg" alt=""/>
                <h3>Dentist Offices</h3>
                <p>The scared look of the patients at a dentist’s reception is something you really do not like seeing. We, at Magazines World have come to your rescue. </p>
                <a href="#con2">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-3.jpg" alt=""/> 
                <h3>Health Clubs & Gyms</h3>
                <p>The “Stay Healthy” factor is growing by leaps and bounds. As a result, you need to stay abreast of all the kind of equipment that comes in the market. </p>
                <a href="#con3">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-4.jpg" alt=""/> 
                <h3>Hospital & Medical Clinics</h3>
                <p>The OPD is a place where you see people from all walks of life. You need to make sure that your hospital is equipped with magazines that cater to most of them.</p>
                <a href="#con4">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-6.jpg" alt=""/> 
                <h3>Beauty Salons &amp; Spa</h3>
                <p>Looking beautiful is an amazing experience. However, the wait at the salon gets exasperating sometimes. Magazines World helps your customers
kill time while waiting their turn at the chair.</p>
                <a href="#con5">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-7.jpg" alt=""/> 
                <h3>Architecture Firms &amp; Design Agencies</h3>
                <p>Yes, your office should contain samples and recommendations of the work you have done for your clients. However, if your client wants to see something more, then Magazine World comes to the rescue.</p>
                <a href="#con6">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-8.jpg" alt=""/> 
                <h3>Hotel Lobbies &amp; Reception Areas</h3>
                <p>The lobby of the hotel says a lot about its guests and employees. Magazines World adds to its grandeur by giving you the magazines apt for your reception areas.</p>
                <a href="#con7">Find out more</a>
            </div>
            </div>
            <div class="col-md-3">
            	<div class="col-xs-12 magazines-by-sectors-product">
                <img src="/images/demo-img-9.jpg" alt=""/> 
                <h3>Small Business, Agencies, &amp; Consulting Firms</h3>
                <p>If you think your business is small, think again. You might not have the clientele that your competitors might have, but the recurring clients and
they getting more clients prove that you and your business mean a lot to them.</p>
                <a href="#con8">Find out more</a>
            </div>
            </div>
        </div>
        
       <!-- <h2 class="magazines-by-sectors-heading_new col-xs-12">Why choose Magazines World?</h2>-->
           <h2 class="main-heading-text">Why choose <br><span class="categories-button"><img src="/images/img_magazines_world_button.png" alt="categories pic"></span></h2>
        <div class="col-xs-12 magazines-content-section">
     
        	<div class="col-md-5 sector-marketing-img">
            	<img src="/images/demo-img-5.jpg" alt=""/>
            </div>
            <div class="col-md-7 magazines-world-content">
            <ul>
            	<li>We take care of each customer!</li>
                <li>We make it EASY – One point of contact and one invoice.</li>
                <li>Ask for subject & product catalogs for selection.</li>
                <li>Get quick quotations for Magazines & Journals.</li>
                <li>Save money – Receive assured discounts on all the publications.</li>
                <li>Save time – one invoice for all your requirements.</li>
                <li>No extra charges – no admin or service fees.</li>
                <li>Flexible payment options for Corporate & Libraries.</li>
                <li>Free Delivery.</li>
                <li>24 x 7 Online Customer Support.</li>
                <li>Hassle-free – a dedicated account manager to answer any queries and resolve any issues.</li>
                <li>Quick and Urgent Shipments are handled with expertise and best courier services.</li>
            </ul>
            <span id="con1"></span>
            </div>
          </div>  
            
           
          <!-- <h2 class="magazines-by-sectors-heading col-xs-12">Shop by categories</h2>-->
          <h2 class="main-heading-text shop-category-sector" id="shop-category-section">SHOP BY  <br><span class="categories-button"><img src="/images/img_shop_by_sectors_button.png" alt="categories pic"></span></h2>
        <div class="col-xs-12 magazines-world-content-main-panel">
     
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-left-panel">
             <h3 class="magazines-by-sectors-heading_new">Doctor's Office</h3>
            <p>
            <strong>Tired of the patients twiddling their thumbs at the doctor’s office??</strong> </p>
            <p>Magazines World has taken care of that. Along with getting a concession on our magazines, you get better services as well. </p>
            <p>We have centralized your current subscription into one bill. </p>
            <p>We have also organized the subscriptions in a way that give you an overview of all the magazines in the doctor's office. </p>
            <p>The problems you have always had while ordering your copies are sorted with the click of a finger. </p>
            <p>Needless to say, you can always order multiple copies of the same issue. Thanks to Magazines World, patients will be happier browsing thru the magazines rather than giving you bored looks.</p>
            </p>
            
            </div>
			<div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-1.jpg" alt=""/>
            </div>  
              <div class="col-xs-12"><span id="con2"></span></div>      
        </div>
          
        
        
         <div class="col-xs-12 magazines-world-content-main-panel">
        	<div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-2.jpg" alt=""/>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-right-panel">
            <h3 class="magazines-by-sectors-heading_new ">Dentist Offices</h3>
            <p>
            	<strong>The scared look of the patients at a dentist's reception is something you really do not like seeing. </strong> </p>
                <p>We, at Magazines World have come to your rescue. All the magazines that your patients wish to read will be there at the click of a finger. </p>
                <p>With the allowance we give you, rejecting our offers will definitely be a task. We have also created one invoice per account so you do not need to look into past bills for assistance. </p>
                <p>Though you get a copy of the magazine you order per subscription, you can always order an additional copy as per your requirement. </p>
                <p>Heads buried in the magazines is better than scared faces, right??</p>
            </p>
            </div>
           <div class="col-xs-12"><span id="con3"></span></div>      

         </div>
            
          
        
         <div class="col-xs-12 magazines-world-content-main-panel">
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-left-panel">
              <h3 class="magazines-by-sectors-heading_new ">Health Clubs &amp; Gyms</h3>
            <p><strong>The "Stay Healthy" factor is growing by leaps and bounds. </strong></p>
            <p>As a result, you need to stay abreast of all the kind of equipment that comes in the market. </p>
            <p>The best way that you and your client would know the latest equipment in the market is thru magazines. </p>
            <p>Magazine World has all the magazines that are related to the fitness world. We organize all the magazines you want thru one click. Thanks to the login id you have; you do not need to remember all the magazines you want once you place the order the first-time round. </p>
            <p>The discounts we offer, when you order in bulk, is going to save you much more money than you bargained for. You can order multiple copies of the magazines that have gone thru a number of hands so the people in the gym do not need to wait their turn to have a look. </p>
            <p>Place an order once and you will keep returning for more.</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-3.jpg" alt=""/>
            </div>
            <div class="col-xs-12"><span id="con4"></span></div>      
         </div>   
           
        
         <div class="col-xs-12 magazines-world-content-main-panel">
        	 <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-4.jpg" alt=""/>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-right-panel">
             <h3 class="magazines-by-sectors-heading_new ">Hospital &amp; Medical Clinics</h3>
            <p>
            	<strong>The OPD is a place where you see people from all walks of life. </strong>  </p>
                <p>You need to make sure that your hospital is equipped with magazines that cater to most of them.</p>
                <p>We, at Magazine World, are here to help you. We have just about every magazine your patients would like to read. </p>
                <p>Along with giving you most of the magazines at a discounted rate, we have incorporated all your bills into just one invoice. </p>
                <p>Like most hospitals around, you can always order more than 1 copy of the magazine that everyone likes to read and place it on another table. </p>
                <p>This way you ensure that a lot of people are not coveting for the same magazine at the same time. 
Our one invoice per bill procedure makes it easier to order from us every time.</p>
            </div>
        	 <div class="col-xs-12"><span id="con5"></span></div> 
        </div>
        
      
         <div class="col-xs-12 magazines-world-content-main-panel">
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-left-panel">
              <h3 class="magazines-by-sectors-heading_new">Beauty Salons &amp; Spa</h3>
                <p>
                    <strong>Looking beautiful is an amazing experience.</strong></p>
<p>However, the wait at the salon gets exasperating sometimes. Magazines World helps your customers
kill time while waiting their turn at the chair.</p>
<p>We have most of the magazines from the beauty world which include the upcoming trends and new
salons opening all around the country.</p>
<p>All you need to do is have a look at the ones you want and order them. We have combined your past
and existing subscriptions in one bill. From now on, all your customers will enter and exit your salon
with a pleasing look on their faces.
                </p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-6.jpg" alt=""/>
            </div>        	
             <div class="col-xs-12"><span id="con6"></span></div> 
        </div>
    
         <div class="col-xs-12 magazines-world-content-main-panel">
            <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-7.jpg" alt=""/>
            </div> 
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-right-panel">
                <h3 class="magazines-by-sectors-heading_new">Architecture Firms &amp; Design Agencies</h3>
                <p><strong>Yes, your office should contain samples and recommendations of the work you have done for your
clients.</strong> </p>
<p>However, if your client wants to see something more, then Magazine World comes to the rescue.</p>
<p>We have magazines many architectural and designer magazines from around the country. Looking
through these magazines will help you and your clients get ideas on how to shape their houses
better.</p>
<p>We have created slots for every niche. So, ordering your favourite magazine will not take more than
a few seconds. We have the option of summarizing the bills of all the magazines you want. This way,
you do not have to click on all the magazines every time you place an order.</p>
            </div>
             <div class="col-xs-12"><span id="con7"></span></div> 
        </div>
        
       <div class="col-xs-12 magazines-world-content-main-panel">
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-left-panel">
            <h3 class="magazines-by-sectors-heading_new ">Hotel Lobbies &amp; Reception Areas</h3>
                <p>
<strong>The lobby of the hotel says a lot about its guests and employees.</strong></p>
<p>Magazines World adds to its grandeur by giving you the magazines apt for your reception areas.
Be it information about different hotels around the country or the kind of cuisine that a hotel
provides, we have it all for you.</p>
<p>The purchase and the finance department of you hotel does not need to focus on the bills as we
have a consolidated bill for every invoice.</p>
<p>You can also order multiple copies to be placed in different areas of your hotel. Guests will now have
something to read the minute they sit down at any corner of your hotel.</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-8.jpg" alt=""/>
            </div> 
               <div class="col-xs-12"><span id="con8"></span></div>     	
        </div>
        

         <div class="col-xs-12 magazines-world-content-main-panel">
        <div class="col-md-6 col-sm-6 col-xs-6">
            	<img src="/images/demo-img-9.jpg" alt=""/>
            </div> 
            <div class="col-md-6 col-sm-6 col-xs-6 magazines-world-content magazines-right-panel">
                    <h3 class="magazines-by-sectors-heading_new">Small Business, Agencies, &amp; Consulting Firms</h3>
                <p><strong>If you think your business is small, think again.</strong></p>

<p>You might not have the clientele that your competitors might have, but the recurring clients and
they getting more clients prove that you and your business mean a lot to them.</p>
<p>We, at Magazine World, help you keep your clients. Our wide array of magazines in the field you are
in will give you ideas of helping your clients in a better have magazines where entrepreneurs who
have excelled in their business pen down articles that help you.</p>
<p>We also have magazines of various genre so no one in your office gets bored. You can always order
in bulk so you get better discounts. Be ensured Magazine World helps you help your clients.</p>
            </div>
        </div>
    </div>
</section>
<!--PRODUCT PAGE END-->






<div id="recent-product-item-listing">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="col-md-12 recent-item-listing-heading bt_heading_3">
          <h1>Recent <span>Listing</span></h1>
          <div class="blind line_1"></div>
          <div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
          <div class="blind line_2"></div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img1.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img2.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img3.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img4.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img5.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img6.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="pricing-item-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="col-md-12 pricing-heading-title bt_heading_3">
          <h1>Pricing <span>Plan</span></h1>
          <div class="blind line_1"></div>
          <div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
          <div class="blind line_2"></div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Basic</h1>
              <hr>
              <p>Free <span>$24</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Limited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block active">
              <h1>Premium</h1>
              <hr>
              <p>Free <span>$49</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Plus</h1>
              <hr>
              <p>Free <span>$99</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Personal Training</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="category-banner-panel">

</section>



<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           



<section class="refund-policy-content shiping-content-outer">
	<div class="col-xs-12 nopadding main-inner-heading"><h1>Shipping Policy</h1></div>
        <div class="col-xs-12 nopadding shiping-content-inner">
             <div class="col-md-4 col-sm-4 col-xs-12 nopadding shiping-content-left free-deliver-left-img"><img src="/images/Shipping-Policy.png" alt=""></div>
             <div class="col-md-8 col-sm-8 col-xs-12 nopadding shiping-content-right">
                  <p><strong>We offer free shipping on all the magazines!</strong></p>
                  <p>All prices listed, are inclusive of delivery costs. There will be no extra delivery charges. </p>
                  <p>The deliveries are handled by each of our publishing partners. The publishers have been handling the delivery of magazine subscriptions for many years. They have developed a great system that ensures the titles arrive in your letterbox on time every time. </p>
                  <p>If you are concerned about a delivery please contact us at Contact@MagazinesWorld.Online and we will be delighted to look after your request.</p>
             </div>
        </div>
</section>
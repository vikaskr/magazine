<?php 
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
$cart = new ShoppingCart();
$cartItems = $cart->getPositions();
$disPrice = (ceil(floatval($price->price))) - ($discount / 100) * (ceil(floatval($price->price)));
?>
  <tr>
    <th>Subscription Period</th>
    <th>Format</th>
    <th>Price</th>
    <th>Discount</th>
    <th>You Pay</th>
    <th>Contact</th>
  </tr>
  <tr>
    <td>ONE YEAR</td>
    <td>Print Only</td>
    <td><?php echo PriceHelper::getCurrencyCode($originCur->id);?> <?= number_format($price->price);?></td>
    <td><?= $discount;?>%</td>
    <td><?php echo PriceHelper::getCurrencyCode($originCur->id);?> <?= (ceil(floatval($disPrice)));?></td>
    <td><a href="/site/contact">Contact</a></td>
  </tr>
  <br/>
  <br/>



<section class="category-banner-panel">

</section>



<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           






<section class="refund-policy-content privacy-policy-content">
<div class="col-xs-12 nopadding term-and-condition-logo"> <img src="/images/privacy-policy-top-img.png" alt=""></div>
        <div class="col-xs-12 refund-content-inner">
           <div class="col-xs-12 nopadding privacy-policy-content-right term-condition-content-right">
               <p>This privacy policy covers the use of the website WWW.MAGAZINESWORLD.ONLINE which is operated by SUBSCRIPTIONS WORLD. </p>
               <p>Your use of the website confirms that you have read, understood and that you expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy. </p>
               <p>If you do not agree please do not use or access WWW.MAGAZINESWORLD.ONLINE</p>
               <p>We respect your privacy and protecting your personal information is a high priority for us at WWW.MAGAZINESWORLD.ONLINE. We are governed by the Indian Law regarding collection, use and disclosure of the information collected from you.</p>
               <p>During the process of making a purchase at WWW.MAGAZINESWORLD.ONLINE we collect and store your personal information which is provided by you like name, email address, billing and shipping address, etc. which is essential for completing the sale transaction. This information is securely stores in your account until requested by you for removal. </p>
               <p>To complete the sale transaction, these details are shared with the seller who is responsible for delivery of the products.</p>
               <p>While registering at WWW.MAGAZINESWORLD.ONLINE you are required to provide your email id. On successful validation of the email id you must choose a new password of your choice which is known only to you.</p>
               <p>We do not disclose your personal information at an individual level to any third party without your consent. As applicable under law we are obliged to share customer specific information when sought by an agency authorized by the Government of India</p>
               <p>MAGAZINES WORLD holds the right to store usage statistics of customers and share it with third parties for marketing analytics and offers. </p>
               <p>Your information is adequately protected by security measures commensurate to the sensitivity of such information. </p>
           </div>
        </div>
</section>
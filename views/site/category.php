<?php 
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\Subject;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
use yz\shoppingcart\ShoppingCart;
use yii\data\Pagination;
use app\helpers\PriceHelper;
use yii\widgets\LinkPager;

$cart = new ShoppingCart();
$cartItems = $cart->getPositions();
$magazines = new Magazine();
$subje = new Subject();
$cat = Yii::$app->getRequest()->getQueryParam('name');
$category = str_replace(' ','--', $cat);

$subject =  $subje::find()->where(['del_status' => '0','subject_slug' => $category])->one();
$subId = $subject->id;
$countQuery = $magazines::find()->where(['del_status' => '0','subject_id' => $subId])->count();
$pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 12]);
$nationalMagazine =  $magazines::find()->where(['del_status' => '0','subject_id' => $subId])->limit($pages->limit)->offset($pages->offset)->all();

$categoryId = $subject->id;                   
?>
<style type="text/css">.carousel-indicators{
display: none!important;
}</style>
<input type="hidden" value="<?= $categoryId;?>" id="categoryId">
<div id="feature-item_listing_block" class="category-page-content-main">
      <div class="col-sm-12 text-center">
      
      	<div class="col-xs-12 nopadding main-inner-heading"><h1> <?= $subject->subject_name;?> magazines</h1></div>
        <div class="col-xs-12 nopadding featured-magazine-tabs-outer">
        
        <div class="col-xs-12 featured-tabs-top-con category-tab-top-con">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                     <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#newest_tab" id="newest" class="sortby">Newest</a></li>
                        <li><a data-toggle="pill" href="#plth_tab" id="plth" class="sortby">Price: Low - High</a></li>
                        <li><a data-toggle="pill" href="#phtl_tab" id="phtl" class="sortby">Price: High - Low</a></li>
                        <li><a data-toggle="pill" href="#nasc_tab" id="nasc" class="sortby">Name: Z - A</a></li>
                        <li><a data-toggle="pill" href="#ndsc_tab" id="ndsc" class="sortby">Name: A - Z</a></li>
                      </ul>
              	</div>
   
                <div class="panel-body">
                	 <!-- <div class="col-xs-12 nopadding category-national-tabs">
                        <div class="category-international-checks">
                            <div class="international-checks-outer"><input name="" id="checkbox" type="checkbox" value=""> <label for="checkbox" class="category-national-icon">National</label></div>
                            <div class="international-checks-outer"><input name="" id="checkbox1" type="checkbox" value=""> <label for="checkbox1" class="category-international-icon">International</label></div>
                        </div>
                    </div> -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active pagination-result" id="newest_tab" class="sortby">
                          <?php 
                          foreach ($nationalMagazine as $value) {    
                          ?>
                          <div class="featured-magazine-bottom-outer">
                          <div class="col-xs-12 nopadding featured-magazine-bottom-inner">

                          <div class="col-xs-12 nopadding featured-magazine-pic">
                            <a href="/site/product?name=<?= $value['slug']?>">
                            <?php
                            $appPath = \Yii::getAlias('@app'); 
                            if(file_exists($appPath."/images/".$value['cover_image'].".jpg"))
                            {
                              $extension = '.jpg';
                            }
                            else
                            {
                              $extension = '.png';
                            }
                            ?>
                              <img src="/images/<?= $value['cover_image'].$extension;?>" alt="featured magazine pics" style="height: 100%;">
                            </a>

                           <div class="quick-veiw-con">
                            <span class="quick-view-text"><a href="/site/quickview?name=<?= $value['slug'];?>" id="quick-view" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i> Quick View</a></span></div>
                          </div> 
                          <?php
                                $alreadyInCart = $cart->getPositionById($value['id']);
                            ?>
                          <a href="/site/product?name=<?= $value['slug']?>"><h4><?= substr($value['name'], 0, -12)?></h4></a>
                          <div class="col-xs-12 nopadding featured-magazine-price"><span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?= number_format($value->getPrice());?></span>
                            <span>
                              <a data-toggle="tooltip" title="Add To Cart" class="btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $value['id']?>"><i class="fa fa-shopping-cart"></i></a>
                              <a data-toggle="tooltip" title="Checkout" href="/site/checkout" target="_blank" class="btn-addcart <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $value['id']?>"><img src="/images/checkout.png" height="20"></a>
                            </span></div>
                          </div>
                          </div>
                          <?php }?>
                        <?php if(!empty($nationalMagazine)){?>
                            <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
                             'pagination' => $pages,
                             ]);
                            ?></center><?php }?></div>
                          </div>
                        </div>

                        
                        <div class="tab-pane fade pagination-result" class="sortdata" id="plth_tab">
                            <div id="load">
                                <h4><img src="/images/load.gif" /> Please Wait</h4>
                            </div>
                        </div>

                        <div class="tab-pane fade pagination-result" class="sortdata" id="phtl_tab">
                            <div id="load">
                                <h4><img src="/images/load.gif" /> Please Wait</h4>
                            </div>
                        </div>

                        <div class="tab-pane fade pagination-result" class="sortdata" id="nasc_tab">
                            <div id="load">
                                <h4><img src="/images/load.gif" /> Please Wait</h4>
                            </div>
                        </div>

                        <div class="tab-pane fade pagination-result" class="sortdata" id="ndsc_tab">
                            <div id="load">
                                <h4><img src="/images/load.gif" /> Please Wait</h4>
                            </div>
                        </div>

                    </div> 
              </div>
            </div>
          </div>
          
        </div>
    </div>
</div>


<?php $this->registerJs("
  $(document).ready(function(){
     $(\"[data-toggle='tooltip']\").tooltip();
   });"); ?>

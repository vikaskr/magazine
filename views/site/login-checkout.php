<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
    $user = Yii::$app->user->identity;
?>
<section class="category-banner-panel">

</section>
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>

<section class="product-main-outer-panel">

    <div class="row">
    
        <div class="col-xs-12 nopadding shopping-cart-main-outer">
        <div class="col-xs-12 nopadding shopping-cart-top">
        <div class="col-sm-6 col-md-6 nopadding shopping-cart-heading">Continue</div>
        <div class="col-sm-6 col-md-6 nopadding shopping-cart-conitune"><a href="/site/checkout"><i class="fa fa-shopping-cart"></i> Back To cart</a></div>
        </div>
        
         <div class="col-xs-12 nopadding shopping-login-checkout-main">
          <?php if (Yii::$app->user->isGuest) {?>
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main">
          <div class="col-xs-12 nopadding shopping-cart-left-inner">
          <h3>Checkout</h3>
          <h4>You must have registered for placing order.</h4>
          
          
          <div class="shopping-login-con" style="display: none;" >
            <input type="radio" id="guest" name="cc" value="guest"/>
            <label for="guest">Checkout as Guest</label>
            </div>
            	
           <div class="shopping-login-con">
            <input type="radio" id="register" name="cc" value="register" checked=""  />
            <label for="register">Register</label>
            </div> 
            <div class="col-xs-12 nopadding shopping-checkout-outer" style="width: 57%;"><a><i class="fa fa-lock"></i> <input type="button" value="Continue" style="background: none!important; border: none!important;"></a></div>
          
                      
          </div>
          </div>
          <?php }?>
          
             <?php if (Yii::$app->user->isGuest) {?>
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main shopping-login-right-outer">
             <div class="col-xs-12 nopadding shopping-cart-left-inner">
             <h3>Registered Customers</h3>
             <h3>All Registered customers can login here!</h3>
              <?php $form = ActiveForm::begin(['options' => ['id' => 'checkout-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
             <div class="col-xs-12 nopadding product-advertise-form-inner shopping-login-checkout-fields">
              <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'login-popup-panel-main input'])->label(false); ;?>
              </div>

             <div class="col-xs-12 nopadding product-advertise-form-inner"><input type="submit" name="login" value="Sign In">
             </div>
            <?php ActiveForm::end(); ?>
            </div>
         </div>
         </div>    
            <?php } else {?>
        <div class="col-xs-12 col-sm-12 col-md-12 nopadding shopping-cart-left-main shopping-login-right-outer">
             <div class="col-xs-12 nopadding shopping-cart-left-inner">
             <h3 style="text-align: center;">Registered Customers</h3>
             <h2 style="color: #fff; text-align: center;">Logged in as <?= $user->username;?>.</h2><br/>
             <button type="submit" class="btn-quote" onclick="location.href='/site/payments'">Continue</button>
             <?php }?>
           </div></div></div>
        
        </div>
  
  </div>
</section>
</div>

</div>
<div class="banner-slider-main-outer about-main-banner">

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
    <li data-target="#myCarousel" data-slide-to="5"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active banner-slider-pic01 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item">
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <div class="item banner-slider-pic02 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> 
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <div class="item banner-slider-pic03 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item">
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
     </div>
     <div class="item banner-slider-pic04 banner-slider-con">
     <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> 
        </div>
        <div id="location_slider_item_block">
             <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
     </div>
      <div class="item banner-slider-pic05 banner-slider-con">
      <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> 
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      
     
     <div class="item banner-slider-pic06 banner-slider-con">
      <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item">
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      
       
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only"></span>
  </a>
</div>

</div>
<!--BANNER SLIDER END-->






<div id="location-map-block">
  <div id="location-homemap-block"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div id="location-link-item">
          <button id="map_list"><i class="fa fa-angle-double-up"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>






<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           

<section class="refund-policy-content about-us-content">
	
        	 <div class="col-xs-12 about-content-inner">
             	 <div class="col-xs-12 nopadding about-content-top-img"><img src="/images/about-banner-button-img.png" alt=""></div>
                 <div class="col-xs-12 nopadding about-content-box-one">
                     <div class="col-xs-12 about-content-box-text">
                     	<p>Magazines World offers magazines on Animals & Pets, Architecture, Art & Design, Auto & Cycles, Aviation, Business & Finance, Celebrity & Films, Children, Comics, Computers & Electronics, Craft, Culture, Education, Enrichment, Entertainment, Fashion, Fiction, Food & Beverage, General Interest, Health & Fitness, History, Hobbies & Puzzles, Home & Garden, Humor, Industry & Trade, Investment, Journals, Lifestyle, Local & Regional, Medical, Men's, Music & Dance, Nature, News & Politics, Parenting & Family, Photography, Psychology, Real Estate, Religion, Science & Technology, Sports & Recreation, Teen, Travel, Weddings, Women's.</p>
                     </div>
                </div>
                
                <div class="col-xs-12 nopadding  about-content-box-one  about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	 <div class="col-xs-12 about-content-box-img"><img src="/images/Best-Choice-1.jpg" alt=""></div>
                     <div class="col-xs-12 about-content-box-text">
                     	<p>
We strive to get you the choice of your Magazines with our existing 10000 plus and ever-increasing titles. We understand that today's readers are more inquisitive and want to explore newer avenues. Keeping this in mind we always keep updating our inventory frequently. Choose Magazinesfrom your favorite subject category.
</p>
                     </div>
                     </div>
                </div>
                
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/Best-Prices-1.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                            <p>
We are one of the most comprehensive sources for supplying Magazines from all around the world online, at an affordable & economical price. We are here to serve your reading appetite just at your doorstep.
    </p>
                         </div>
                     </div>
                </div>
                                 
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/Best-Service-1.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                           <p>
We process your orders immediately, with the publishers and make sure that you get your subscription started quickly, and receive issues on time. We invite you to experience an efficient service that delivers right at your door step, anywhere in the world.
</p>
                         </div>
                     </div>
                </div>
                
                
                                  
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/24-hours-1.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	<p>We provide 24 hours service to our customers, connect with us any time, any day, we are always at your service. Your satisfaction is our ultimate goal and we're dedicated to deliver it!
</p>
                         </div>
                     </div>
                </div>
                
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/Support-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	<p>If you need any assistance, with your selection or order, you can write to customer support. We provide unique experience for users and offer the widest variety of magazines from all corners of world to cater to every kind of reader.
</p>
                         </div>
                     </div>
                </div>
                
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/Gift-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>Get gifts on your subscriptions, as offered by the publishers on various magazines. Check on the offers available with magazines subscriptions, in gift section.
</p>
                         </div>
                     </div>
                </div>
                
                 <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/Free-Delivery-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>We offer free home delivery; all the issues will be sent free of delivery charges to your address.</p>
                         </div>
                     </div>
                </div>
                
                 <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/Delivery-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>We are here to serve your reading appetite, just at your doorstep. With a huge assortment of magazine genres, it's a world of magazines from all around the globe in one place. Just browse, choose and subscribe your magazine and we will deliver it to you promptly.
</p>
                         </div>
                     </div>
                </div>
                
                 <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/email-us-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>If you wish to write to us, you can email us at Contact@Subscriptions.World, we will be happy to assist you!</p>
                         </div>
                     </div>
                </div>
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/Shop-Online-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>We make, managing and renewing subscriptions, easy, whether it's 1 or 100 magazine subscriptions, we'll save you time and money. Shop with a click of button! You will find everything at One Place – Review, Subscribe, Renew…its Easy!
</p>
                         </div>
                     </div>
                </div>
                
                
                 <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/Save-Money-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>You can save money, by using our services, on magazines subscriptions and get great discounts.</p>
                         </div>
                     </div>
                </div>
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img about-content-box-right-img"><img src="/images/Customer-Service-2.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>We provide unique experience for users and offer the widest variety of Magazines & Journals from all corners of world to cater to every kind of reader. We strive to get you the choice of your magazine with our existing 10000 plus and ever-increasing titles. We understand that today's readers are more inquisitive and want to explore newer avenues. Keeping this in mind we always keep updating our inventory frequently.
</p>
                         </div>
                     </div>
                </div>
                
                <div class="col-xs-12 nopadding about-content-box-one about-content-box-two">
                	<div class="col-xs-12 nopadding about-content-box-two-inner">
                	     <div class="col-xs-12 about-content-box-img"><img src="/images/Sign-up.jpg" alt=""></div>
                         <div class="col-xs-12 about-content-box-text">
                          	 <p>Sign up for free updates and newsletters on promotional offers.</p>
                         </div>
                     </div>
                </div>
                
                <div class="col-xs-12 about-bottom-listing">
                	<h2>Why choose Magazines World?</h2>
                    <ul>
                        <li>We take care of each customer!</li>
                        <li>We make it EASY – One point of contact and one invoice.</li>
                        <li>Ask for subject & product catalogs for selection.</li>
                        <li>Get quick quotations for Magazines & Journals.</li>
                        <li>Save money – Receive assured discounts on all the publications.</li>
                        <li>Save time – one invoice for all your requirements.</li>
                        <li>No extra charges – no admin or service fees.</li>
                        <li>Flexible payment options for Corporate & Libraries.</li>
                        <li>Free Delivery.</li>
                        <li>24 x 7 Online Customer Support.</li>
                        <li>Hassle-free – a dedicated account manager to answer any queries and resolve any issues.</li>
                        <li>Quick and Urgent Shipments are handled with expertise and best courier services.</li>

                    </ul>
                </div>
                
            </div>

</section>


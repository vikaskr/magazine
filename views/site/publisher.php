<section class="category-banner-panel">

</section>


<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           



<section class="refund-policy-content publishers-content">
	<div class="col-xs-12 nopadding main-inner-heading"><h1>Publishers</h1></div>
    <div class="col-xs-12 refund-content-inner">
       <ul>
        <li>3 Tales Publishing</li>
        <li>AB ART Press</li>
        <li>Abbott & Hast Publications</li>
        <li>Aberdeen & North-East Scotland Family History Society</li>
        <li>ABK Publications</li>
        <li>Abrahams Australia</li>
        <li>Absolute Publishing Ltd</li>
        <li>ACK media Direct Ltd</li>
        <li>Acres U.S.A.</li>
        <li>Action Publishing</li>
        <li>ADAC Verlag</li>
        <li>Adleader Publications</li>
        <li>Adsert Web Solutions Ltd</li>
        <li>Adtice</li>
        <li>ADV Publishing House</li>
        <li>Advantage Publishing</li>
        <li>ADVISOR MEDIA, Inc</li>
        <li>African Publishers' Network (APNET)</li>
        <li>After the Battle</li>
        <li>Air Age Media</li>
        <li>AIRtime Publishing Inc.</li>
        <li>Alfol Publishing Limited</li>
        <li>ALIA Publishing</li>
        <li>Alicubi Publications</li>
        <li>Allt om Hobby</li>
        <li>AME Research</li>
        <li>American Arbitration Association</li>
        <li>American Carriage House Publishing</li>
        <li>American City Business Journals</li>
        <li>American Diabetes Association</li>
        <li>American Farrier's Association</li>
        <li>American Federation of Television and Radio Artists</li>
        <li>American Heartworm Society</li>
        <li>American Indian Studies Center at UCLA</li>
        <li>American Institute for Cancer Research</li>
        <li>American Jewish Historical Society</li>
        <li>American Mathematical Society</li>
        <li>American Radio Relay League</li>
        <li>American Society of Composers, Authors and Publishers (ASCAP)</li>
        <li>American Vision</li>
        <li>Amnesty International</li>
        <li>Andover Junction Publications</li>
        <li>Angel Business Communications Ltd</li>
        <li>Angelis Press</li>
        <li>Angling Publications</li>
        <li>Animal Welfare Institute</li>
        <li>Antique Collectors' Club Ltd</li>
        <li>Aperture Foundation</li>
        <li>Apollonia</li>
        <li>Apple Publishing Ltd</li>
        <li>Archant Dialogue</li>
        <li>Archant Life</li>
        <li>Archant Specialist</li>
        <li>Architecture Media Pty Ltd</li>
        <li>Areopagus Publications</li>
        <li>Arizona Highways</li>
        <li>Ark Group Ltd</li>
        <li>Array Publications</li>
        <li>Artscript Publications</li>
        <li>ASAPP MEDIA PVT LTD</li>
        <li>Asay Publishing Co.</li>
        <li>Ashdown</li>
        <li>Aspect Media</li>
        <li>Association of American University Presses</li>
        <li>Association of Baptists for World Evangelism</li>
        <li>Astrom Editions</li>
        <li>Athene Publishing Ltd</li>
        <li>Atlantic Publication Group, Inc.</li>
        <li>Atlantic Publishers</li>
        <li>Atlantic Publishing Group, Inc.</li>
        <li>Atlas Publishing</li>
        <li>Atom</li>
        <li>August Home Publishing</li>
        <li>Australian Consolidated Press</li>
        <li>Axon Publishing</li>
        <li>Babcox Publications</li>
        <li>Ballinger Publishing</li>
        <li>Barks Publications, Inc.</li>
        <li>Bauer Consumer Media</li>
        <li>Bauer Publishing</li>
        <li>Baylight Publishing Music & Media</li>
        <li>BCInsight Ltd</li>
        <li>Bearpark Publishing</li>
        <li>Beckett Publications</li>
        <li>Bee Publishing Company</li>
        <li>Belmont International Inc</li>
        <li>Bennett Publishing Ltd</li>
        <li>Berita Publishing SdnBhd</li>
        <li>Bertelsmann AG</li>
        <li>Bharathan Publications</li>
        <li>Biblical Archaeology Society</li>
        <li>Biblinkeditori</li>
        <li>Billian Publishing</li>
        <li>Biz Publishing</li>
        <li>Black Academy Press, Inc.</li>
        <li>Black Apollo Press</li>
        <li>Black Dress Press</li>
        <li>Black Raven Press</li>
        <li>Bladonmore Media</li>
        <li>Blindside Publishing</li>
        <li>Blu Inc Media (HK) Ltd</li>
        <li>Blue Orange Media Ltd</li>
        <li>BNP Media</li>
        <li>Boat International Media</li>
        <li>Bombshelter Press</li>
        <li>Boolarong Press</li>
        <li>Boston Publishing Pty Ltd</li>
        <li>Bowker</li>
        <li>BPG Ltd.</li>
        <li>Bracecorp Publications Pvt. Ltd</li>
        <li>British Wildlife Publishing</li>
        <li>Brodie Publishing</li>
        <li>Brooklands Group</li>
        <li>BSI International, Inc.</li>
        <li>Bull City Press</li>
        <li>Burda Communications</li>
        <li>Business India Ltd (Fortune India)</li>
        <li>BUSINESS MEDIA PRIVATE LIMITED</li>
        <li>BW Businessworld Media Pvt. Ltd.</li>
        <li>C21 Media</li>
        <li>Caravan Books Publishing House</li>
        <li>Carden Jennings Publishing Co., Ltd.</li>
        <li>CAREERS360</li>
        <li>Carnyx Group</li>
        <li>Casper Publications Pty Ltd</li>
        <li>Caspian Publishing Ltd</li>
        <li>Cedar Communications</li>
        <li>Chalk Farm Publishing Ltd</li>
        <li>Challenge Publications, Inc.</li>
        <li>Chartered Management Institute</li>
        <li>Chesterton & Davies, Ltd.</li>
        <li>Child Care Information Exchange</li>
        <li>Chitralekha</li>
        <li>Christianity Today International</li>
        <li>Circle Publishing</li>
        <li>Citizen32</li>
        <li>CitySavvy Ltd</li>
        <li>Citywire Financial Publishers Ltd</li>
        <li>Climate Institute</li>
        <li>CMP Information Ltd</li>
        <li>Cobblestone Publishing Company</li>
        <li>Coffey Communications</li>
        <li>Columbia College Center for Black Music Research</li>
        <li>Competition Affairs</li>
        <li>Complete Wellbeing Publishing Pvt. Ltd.</li>
        <li>Computer Press</li>
        <li>Conde Nast India Pvt Ltd</li>
        <li>Conde Nast Publications</li>
        <li>CondeNet International</li>
        <li>Confederation of British Industry</li>
        <li>Connect Communications</li>
        <li>Consortium eLearning Network Pvt. Ltd</li>
        <li>Consumers Union</li>
        <li>Cool Companies Incorporated</li>
        <li>Country Bumpkin</li>
        <li>Covenanter Press</li>
        <li>CQ Products</li>
        <li>Crafts Council</li>
        <li>Crambeth Allen Publishing</li>
        <li>Craven Publishing</li>
        <li>Crawdaddy!</li>
        <li>Cross Border Ltd</li>
        <li>Crystalline Sphere Publishing</li>
        <li>CSM Publishing / Charles Simpson Ministries</li>
        <li>Cyber Media (India) Ltd.</li>
        <li>D. C. Thomson & Co Ltd</li>
        <li>D. T. Media & Entertainment Pvt. Ltd</li>
        <li>Daily Thanthi group</li>
        <li>Dalesman Publishing Company</li>
        <li>Dalkey Archive Press</li>
        <li>Dancing girl press</li>
        <li>Dancingfish Press</li>
        <li>Dansk psykologiskForlag</li>
        <li>Dar al-Kotob al-Ilmiyah</li>
        <li>Dar El Shorouk</li>
        <li>DaVinci Press</li>
        <li>Dawes Designs Home Plans</li>
        <li>DDP Publications Ltd</li>
        <li>De Proverbio</li>
        <li>Dead Dog Press</li>
        <li>Deeson Group</li>
        <li>Delhi Press Patra Prakashan Ltd</li>
        <li>Dennis Publishing</li>
        <li>DeSciner Business Network Ltd.</li>
        <li>Development Hell Ltd.</li>
        <li>Diamond Magazines Pvt. Ltd.</li>
        <li>Diamond Publication</li>
        <li>Digital 18 Media Ltd.</li>
        <li>Dinamalar</li>
        <li>Dino AudinoEditore</li>
        <li>Director Publications</li>
        <li>Divisible by Zero Pty Ltd</li>
        <li>Dmg world media</li>
        <li>Dockery House Publishing</li>
        <li>Dog Horn Publishing</li>
        <li>Dorn Publications, Inc.</li>
        <li>Down East Books</li>
        <li>Dragoon Publishing</li>
        <li>Drawn & Quarterly</li>
        <li>Dreamtime Press</li>
        <li>DRG</li>
        <li>DSIJ Pvt. Ltd. Delhi Press</li>
        <li>DT Media Entertainment Pvt Ltd</li>
        <li>Dustbooks</li>
        <li>DVV Media UK Limited</li>
        <li>Dzanc Books</li>
        <li>Eastland Press</li>
        <li>Eclectic Publications Pvt. Ltd.</li>
        <li>Economic & Political Weekly</li>
        <li>EdicionesCientificas</li>
        <li>Edipresse Asia</li>
        <li>Editions Bim</li>
        <li>Editions Soleil Publishing Inc</li>
        <li>Editorial Gustavo Gili, S.A.</li>
        <li>Editorial Reus</li>
        <li>EFY Enterprises Pvt Ltd</li>
        <li>Egmont</li>
        <li>Ehlert Publishing Group, Inc</li>
        <li>Electric Power Research Institute (EPRI)</li>
        <li>EletsTechnomediaPvt Ltd</li>
        <li>Emap</li>
        <li>Emond Montgomery Publications</li>
        <li>Emphasis Media Limited</li>
        <li>Empire Publications</li>
        <li>Environmental Data Services</li>
        <li>EXIT</li>
        <li>EXPOSURE MEDIA MARKETING (P) LIMITED</li>
        <li>Express Publictions Madurai Ltd</li>
        <li>F+W Publications, Inc.</li>
        <li>Fabian Society</li>
        <li>Factory Media</li>
        <li>Fairchild Fashion Media</li>
        <li>Faircount International LLC</li>
        <li>Fairplay Publications</li>
        <li>Faversham House Group</li>
        <li>Feather Books</li>
        <li>Federico Motta Editore</li>
        <li>First City Media Ltd</li>
        <li>Fitzgerald Publishing Co., Inc.</li>
        <li>Five Star Publications, Inc.</li>
        <li>Flashes Publishers</li>
        <li>FMC</li>
        <li>FMS</li>
        <li>Forum of Federations</li>
        <li>Forward Group</li>
        <li>Forward Movement Publications</li>
        <li>FOW</li>
        <li>Fox Chapel Publishing Company</li>
        <li>Fox Racing, Inc.</li>
        <li>Franchise India Holdings Limited</li>
        <li>FriedlNet.com</li>
        <li>Friends of the Earth</li>
        <li>Friends of the Earth (UK)</li>
        <li>FT Business</li>
        <li>Future Publishing Limited</li>
        <li>Gates of Heck</li>
        <li>Gauntlet Press</li>
        <li>GBN Media Pvt Ltd</li>
        <li>GDS Publishing</li>
        <li>GeenBladvoor de Mond BV</li>
        <li>GenNext Publishing</li>
        <li>George Warman Publications</li>
        <li>Geospatial Media & Communications</li>
        <li>GGZ consult online</li>
        <li>Giraffe Media Limited</li>
        <li>Glimmer Train Press, Inc.</li>
        <li>Golf International Services Limited</li>
        <li>Gopali& Co</li>
        <li>Gospel Advocate Company</li>
        <li>Graffiti Publications Pty.Ltd.</li>
        <li>Granta Publications</li>
        <li>Great Northern Publishing (UK)</li>
        <li>Gregath Publishing Company</li>
        <li>Grist Ltd.</li>
        <li>Growing Edge</li>
        <li>Grupo Editorial Sinos</li>
        <li>Guideposts</li>
        <li>Hachette Filipacchi Media Asia Pacific</li>
        <li>Hachette Filipacchi UK Ltd</li>
        <li>Halldale Media Ltd</li>
        <li>Halper Publishing Company</li>
        <li>Hamerville Magazines Limited</li>
        <li>Hanging Loose Press</li>
        <li>Harbor House Publishers</li>
        <li>Hard Press Editions</li>
        <li>Harmony The Magazine</li>
        <li>Harmony Unlimited</li>
        <li>Haymarket Media(India) Private Limited</li>
        <li>Hayward Medical Communications</li>
        <li>Hearst Communications Inc</li>
        <li>Heldref Publications</li>
        <li>Helion & Company</li>
        <li>Hemming Information Services</li>
        <li>Hendon Publishing, Inc</li>
        <li>Hf media & events</li>
        <li>High Tide Press</li>
        <li>Hillaine Publishing Ltd</li>
        <li>Himalayan Institute Press</li>
        <li>Historical Society of Alberta</li>
        <li>History Today</li>
        <li>HochschulVerlag GmbH</li>
        <li>Holyrood Communications</li>
        <li>Home Education Magazine</li>
        <li>Horizon House Publications</li>
        <li>Hospice Foundation of America</li>
        <li>Hotei Publishing</li>
        <li>Housebuilder Media Ltd</li>
        <li>Howard University Press</li>
        <li>HR Information Services</li>
        <li>Hunt-Scanlon Corporation</li>
        <li>I B H Books & Magazines Distributors Ltd</li>
        <li>Ian Allan Publishing</li>
        <li>IC Publications</li>
        <li>Ideals Publications Incorporated</li>
        <li>IDG Czech, a.s.</li>
        <li>IDG Media Private Ltd.</li>
        <li>IFWG Publishing</li>
        <li>Images Multimedia Pvt. Ltd.</li>
        <li>Imagine Publishing</li>
        <li>Imako Media S.A</li>
        <li>IML Group</li>
        <li>Immediate Media Co.</li>
        <li>Incisive Media</li>
        <li>Indigo Publications</li>
        <li>Infinitheism Spiritual Foundation Limited</li>
        <li>Ink Publishing</li>
        <li>Institute of Electrical and Electronics Engineers, Inc.</li>
        <li>Institute of Physics Publishing</li>
        <li>Institute of Public Administration</li>
        <li>Institution of Professional Engineers New Zealand</li>
        <li>Intent Media</li>
        <li>Intercollegiate Studies Institute (ISI)</li>
        <li>International Alliance for Women in Music</li>
        <li>International Bee Research Association</li>
        <li>International Magazines Ltd</li>
        <li>International Monetary Fund</li>
        <li>International Trade Show Press</li>
        <li>International Women's Democracy Center</li>
        <li>InterStudy Publications</li>
        <li>InterVarsity Press</li>
        <li>Interweave Press</li>
        <li>Investor Intelligence Partners</li>
        <li>IPC Media Limited</li>
        <li>ISI Books</li>
        <li>Island Travel Trader</li>
        <li>James Pembroke Publishing</li>
        <li>John Brown Publishing</li>
        <li>JPR Publications / Institute for Jewish Policy Research</li>
        <li>JuneWarren Publishing</li>
        <li>Kable Limited</li>
        <li>Kal Publications</li>
        <li>Kalakaumudi Publications</li>
        <li>Kalmbach Publishers</li>
        <li>KAV Publicity Ltd.</li>
        <li>Kelsey Publishing Ltd</li>
    
        <li>Kemps Publishing</li>
        <li>Krause Publications, Inc.</li>
        <li>Kursiv Publishers</li>
        <li>Kyoorius Digital</li>
        <li>Lance Publishing Ltd</li>
        <li>Land Technology Ltd</li>
        <li>Lao Tse Press</li>
        <li>Leading Brands Publishing</li>
        <li>League Publications Ltd</li>
        <li>Lebhar-Friedman</li>
        <li>Lessiter Publications</li>
        <li>Lewis Masonic</li>
        <li>LibreriaJuridicaIntercodex</li>
        <li>Life Media</li>
        <li>Life Time Wellness RX International Ltd</li>
        <li>Light Technology Publishing Company</li>
        <li>Lighthouse Publishing Inc.</li>
        <li>Linworth Publishing</li>
        <li>Lionheart Publishing, Inc.</li>
        <li>Living Aboard</li>
        <li>Living Media India Ltd</li>
        <li>Livros do Oriente</li>
        <li>London Review of Books</li>
        <li>Lovatts Publications</li>
        <li>LPD Press</li>
        <li>LRP Publications</li>
        <li>LSC Publishing</li>
        <li>Luxury Publishing Ltd.</li>
        <li>Mac Elle Publishing ApS</li>
        <li>Mack Brooks Publishing Ltd</li>
        <li>Magicalia Ltd.</li>
        <li>Magna Publishing Co. Ltd</li>
        <li>Magna Publishing group</li>
        <li>Magnesium Media</li>
        <li>Main Street Rag Publishing Company</li>
        <li>Majellan Publications</li>
        <li>MalayalaManorama Company Ltd</li>
        <li>Mangalam Publications</li>
        <li>Maritime Books</li>
        <li>Marketing and Creative Handbook</li>
        <li>Marketing Editorial.Com</li>
        <li>Markoulakis Publications</li>
        <li>Marvel Enterprises, Inc</li>
        <li>Mary Glasgow Magazines</li>
        <li>Mash Media</li>
        <li>Mathematical Association of America</li>
        <li>Mathrubhumi Publications</li>
        <li>Mayo Clinic Publications</li>
        <li>McGraw-Hill International (UK) Ltd</li>
        <li>McGuire Publishing</li>
        <li>Media Visions</li>
        <li>MediaClash Group</li>
        <li>Medical Economics Company</li>
        <li>Meredith Corporation</li>
        <li>Micromedia ProQuest</li>
        <li>Milan Presse</li>
        <li>Minnesota Physician Publishing</li>
        <li>Mira Editores</li>
        <li>Mission 21 Publishing Ltd</li>
        <li>Moments With The Book</li>
        <li>Moran Publishing Company, Inc</li>
        <li>Mortons Media Group Limited</li>
        <li>Motivate Publishing</li>
        <li>MOTOR Information Systems</li>
        <li>Mountainside Publishing, Inc.</li>
        <li>MRS (The Market Research Society)</li>
        <li>Mucusart Publications</li>
        <li>MW.Com India Pvt. Ltd</li>
        <li>NashreKetabdar</li>
        <li>National Childbirth Trust</li>
        <li>National Geographic Society</li>
        <li>National Institute of Industrial Research</li>
        <li>National Magazine Company</li>
        <li>National Trust Books</li>
        <li>National Urban League</li>
        <li>Naval War College Press</li>
        <li>NavPress</li>
        <li>Neonatal Network/NICU Ink Books</li>
        <li>Nestron Publishing</li>
        <li>Network 18 Publishing</li>
        <li>New England Historic Genealogical Society</li>
        <li>New Forums Press, Inc.</li>
        <li>New Generation Media</li>
        <li>New Hope International</li>
        <li>New Internationalist Publications Ltd</li>
        <li>New Media Group</li>
        <li>New Moon Publishing</li>
        <li>New Start Publishing Ltd</li>
        <li>New York City in Pictures, Inc</li>
        <li>Newhall Publications Limited</li>
        <li>Newsco Insider</li>
        <li>NewsMax Media, Inc.</li>
        <li>Newtrade Publishing Ltd</li>
        <li>Next Gen Publishing Ltd</li>
        <li>Nielsen Book Services Limited</li>
        <li>Nine Dot Nine Mediaworx Pvt. Ltd.</li>
        <li>Noble House Media</li>
        <li>North American Publishing Company</li>
        <li>Northstar Publishing</li>
        <li>Northwest Media Inc.</li>
        <li>NPC Productions</li>
        <li>NWMedia& e-Publishing</li>
        <li>Ocean Media Group</li>
        <li>Ogaan Publications Private Limited</li>
        <li>Olympus PremediaPvt Ltd</li>
        <li>One Media Group</li>
        <li>Open Media Network Pvt. Ltd.</li>
        <li>Open Spaces Publications, Inc</li>
        <li>Orientica</li>
        <li>Origin Publishing</li>
        <li>Outlook Publishing (India) Pvt. Ltd.</li>
        <li>Oxmoor House Publications</li>
        <li>Oyster Publishing</li>
        <li>PA Customer Publishing</li>
        <li>Pacific Edge Publishing</li>
        <li>Pageant Media Limited</li>
        <li>Paisano Publications, LLC</li>
        <li>Parabola</li>
        <li>Paragraph Publishing</li>
        <li>ParentEdge</li>
        <li>Parker Ellis Publishing</li>
        <li>Paternoster Publishing</li>
        <li>Pathfinders Publishing Private Limited</li>
        <li>Pavilion</li>
        <li>Pavilion (formerly Keyways Publishing)</li>
        <li>Pendragon Professional Information Ltd</li>
        <li>Penton Media, Inc.</li>
        <li>Permanent Publications</li>
        <li>Perpetuity Press</li>
        <li>Perspective Publishing</li>
        <li>Petal Pusher Press</li>
        <li>Philosophy Now</li>
        <li>Photographers' Institute Press</li>
        <li>PICTS (Paramartha International Center for Tasawwuf Studies)</li>
        <li>Pinpoint Scotland Ltd</li>
        <li>Pioneer Scientific Publisher</li>
        <li>Pipe Trades Media Group</li>
        <li>Pipeline Publications Australia Pty Ltd</li>
        <li>Piter Publishing Company</li>
        <li>Plaza Publishing</li>
        <li>Plenham Ltd.</li>
        <li>Poetry Business</li>
        <li>Poetry in the Arts, Inc.</li>
        <li>Poetry London</li>
        <li>Polestar Publishing Ltd</li>
        <li>Population Reference Bureau</li>
        <li>Positive Publications</li>
        <li>Postmedia</li>
        <li>Practical Action Publishing</li>
        <li>Pratiyogita Darpan</li>
        <li>Prayag consulting Pvt Ltd-</li>
        <li>Printrade India Publications</li>
        <li>Process Press</li>
        <li>Professional Engineering Publishing</li>
        <li>Professors World Peace Academy</li>
        <li>Project Management Institute</li>
        <li>Property Data</li>
        <li>Prufrock Press</li>
        <li>PSP Publishing Ltd</li>
        <li>PSP Rare Publishing</li>
        <li>Publishing Business</li>
        <li>Publishing Events Ltd</li>
        <li>PUG / Presses Universitaires de Grenoble</li>
        <li>Putney Press</li>
        <li>Quinlan Publishing Group</li>
        <li>Raceform</li>
        <li>Rahul Sidhu Group</li>
        <li>Raincoast Books</li>
        <li>Raven Productions, Inc</li>
        <li>RCN Publishing Company Limited</li>
        <li>Reader's Digest</li>
        <li>Reader's Digest Association (Canada) Ltd.</li>
        <li>Reader's Digest Association Limited</li>
        <li>Real London Ltd</li>
        <li>Recycler Publishing & Events Limited</li>
        <li>Redactive Media Group</li>
        <li>Redan Publishing</li>
        <li>Redwood</li>
        <li>Regent Publications</li>
        <li>Regnum Books</li>
        <li>Research Institute of America</li>
        <li>Result Customer Communications</li>
        <li>Retirement Publishing</li>
        <li>Rhinegold Publishing</li>
        <li>Rimal Publications</li>
        <li>Ringier</li>
        <li>River Publishing</li>
        <li>Robbie Dean Press</li>
        <li>RobinAge Weekly Children's Newspaper</li>
        <li>Rolling River Publications Ltd</li>
        <li>Romsey Publishing Group</li>
        <li>Royal British Columbia Museum</li>
        <li>Royal Philatelic Society of New Zealand</li>
        <li>RP Publishing</li>
        <li>Rural Press Limited</li>
        <li>Rutherford House</li>
        <li>Sangiovanni's strategies</li>
        <li>Scholastic (US)</li>
        <li>Scholastic Canada</li>
        <li>Scottish Wedding Directory</li>
        <li>Selvedge</li>
        <li>Setform Ltd</li>
        <li>Sheen Publishing Ltd.</li>
        <li>Shoetrades Publishing</li>
        <li>Shogakukan</li>
        <li>ShortList Media Ltd</li>
        <li>Sigaria Ltd.</li>
        <li>Signature Publishing Ltd</li>
        <li>Silverdart Ltd</li>
        <li>Sky Publishing Corporation</li>
        <li>Slimming World</li>
        <li>Smaart Publishing</li>
        <li>Society for Environmental Communications</li>
        <li>Society of Biblical Literature</li>
        <li>Solo Publications</li>
        <li>Sosland Publishing Company</li>
        <li>Sound On Sound</li>
        <li>South Downs Living</li>
        <li>Southwestern Publishing</li>
        <li>Sovereign Publications</li>
        <li>Sozler Publications</li>
        <li>Special Publications</li>
        <li>Specialist Publications UK</li>
        <li>Speleo Projects-Caving Publications International</li>
        <li>Spenta Multimedia</li>
        <li>SPH Magazines Pte Ltd.</li>
        <li>Spice Publishing Group</li>
        <li>Splash Publication Pvt Ltd</li>
        <li>Sport Media & Strategy Ltd</li>
        <li>Square Up Media</li>
        <li>Square7 Media Ltd</li>
        <li>St John Patrick Publishers</li>
        <li>St. Anthony Messenger Press</li>
        <li>Standard Publishing</li>
        <li>Stanley Gibbons Publications Ltd.</li>
        <li>Story Worldwide</li>
        <li>Stratfield Ltd</li>
        <li>String Letter Publishing</li>
        <li>Subcontinent Publishing Ltd.</li>
        <li>Sundial Magazines</li>
        <li>SWP Publishing</li>
        <li>Tangerine Press</li>
        <li>Tate Gallery</li>
        <li>Taunton Press</li>
        <li>Taxmann Publications</li>
        <li>TechMedia</li>
        <li>Ten Alps Publishing</li>
        <li>Terrapinn Holdings Ltd</li>
        <li>Tfm Publishing</li>
        <li>The Academy of Natural Sciences</li>
        <li>The Ad Plain</li>
        <li>The Advocado Press</li>
        <li>The Africa Institute of South Africa</li>
        <li>The Association of Corporate Treasurers</li>
        <li>The Association of Cricket Statisticians and Historians</li>
        <li>The Bitter Oleander Press</li>
        <li>The British Horse Society</li>
        <li>The Building Information Center Publications</li>
        <li>The Cinema Theatre Association</li>
        <li>The Consumers' Association of Ireland</li>
        <li>The Continental Divide Trail Society</li>
        <li>The Croatian Academy of America</li>
        <li>The Dana Press</li>
        <li>The Defense Research Institute</li>
        <li>The Eagle Publishing Company</li>
        <li>The Economist Group</li>
        <li>The Golden Gate Production Co Ltd.</li>
        <li>The Hemming Group</li>
        <li>The Illustrated London News Group</li>
        <li>The Institute of Historical Research</li>
        <li>The Institution of Engineering and Technology</li>
        <li>The International Institute for Strategic Studies</li>
        <li>The MalayalaManorama Co. Ltd.</li>
        <li>The McGraw-Hill Companies</li>
        <li>The Modern Language Association of America</li>
        <li>The National Literacy Trust</li>
        <li>The Publishers Ltd.</li>
        <li>The Retail Jeweller</li>
        <li>The Rialto</li>
        <li>The Royal Horticultural Society</li>
        <li>The Royal Society for the Protection of Birds</li>
        <li>The Royal Society of Literature</li>
        <li>The Spectator</li>
        <li>The Stationery Office (formerly HMSO)</li>
        <li>The Tablet Publishing Company</li>
        <li>The TNT Group</li>
        <li>The Upper Room</li>
        <li>The Wordhouse Publishing Group</li>
        <li>Theosophical Publishing House</li>
        <li>Think Publishing</li>
        <li>Third World Network</li>
        <li>Thomas Publishing Company</li>
        <li>Thomson</li>
        <li>Thorpe-Bowker</li>
        <li>TouteBagai Publishing</li>
        <li>Trader Media Group</li>
        <li>Trails Media Group, Inc.</li>
        <li>Trajan Publishing Corporation</li>
        <li>Transitions Abroad</li>
        <li>TSL Education Limited</li>
        <li>Tudor Rose</li>
        <li>Turret Group</li>
        <li>U.S. Government Printing Office</li>
        <li>Unionsverlag</li>
        <li>Unity Media Plc</li>
        <li>Uplook Ministries</li>
        <li>Vaccari</li>
        <li>Vaka-Helgafell</li>
        <li>Vance Publishing Corporation</li>
        <li>Vasan Publications Pvt. Ltd.</li>
        <li>Verlag Dohr</li>
        <li>Verlag Lindenstruth</li>
        <li>Verlag M. & H. Schaper</li>
        <li>Veterinary Business Development</li>
        <li>Visual Reference Publications, Inc.</li>
        <li>Vitesse Media Plc</li>
        <li>Vortex Centrum Limited</li>
        <li>Waitrose</li>
        <li>Wanderlust</li>
        <li>Wardour Publishing & Design</li>
        <li>Warren Communications News</li>
        <li>Weider Publishing Ltd.</li>
        <li>Wellred Books</li>
        <li>Wharncliffe Publishing Limited</li>
        <li>What Is Enlightenment? Press</li>
        <li>Which?</li>
        <li>White Light Media Ltd</li>
        <li>White Mountain Publications</li>
        <li>Whitmar Publications</li>
        <li>Wildfire Communications Limited</li>
        <li>Wildfire Publishing</li>
        <li>William Reed Publishing</li>
        <li>Wilmington Group plc</li>
        <li>Wisconsin Historical Society Press</li>
        <li>Wisden</li>
        <li>Wngates.com, Inc. dba W.N. Gates Company</li>
        <li>Wolfgang G. Haas</li>
        <li>WoodenBoat Publications</li>
        <li>Woodhead Publishing</li>
        <li>WordWide Communications</li>
        <li>World Advertising Research Center</li>
        <li>World Eagle</li>
        <li>World Wide Media</li>
        <li>WydawnictwoDiG</li>
        <li>WydawnictwoLibramed</li>
        <li>Wyrm Publishing</li>
        <li>X-Ray Book Company</li>
        <li>Yaffa Publishing</li>
        <li>Yale Robbins Inc.</li>
        <li>YapýKrediKulturSanatYayincilik</li>
        <li>Zafusy contemporary poetry journal</li>
        <li>Zeta Books</li>
        <li>Ziff-Davis Publishing</li>
        <li>ZONE</li>
      </ul>
    </div>
</section>

<!--NEWSLETTER SECTION START-->
<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$signupMail = new \app\models\SignupMail();
?>
<div class="newsletter-main-outer-panel">
<div class="container">
<div class="row">
<div class="col-xs-12 nopadding newsletter-main-inner">
<div class="col-xs-5 col-sm-5 col-md-5 nopadding newsletter-main-inner-left">
<div class="newsletter-con"><img src="/images/img_newsletter_icon.png" alt="newsletter"><h3>Subscribe <span>Our Newsletter</span></h3></div>
</div>
<div class="col-xs-7 col-sm-7 col-md-7 nopadding newsletter-main-inner-right">

		<?php $form = ActiveForm::begin(['id' => 'contact_office', 'method' => 'post']); ?>
            <div class="form-group">
              <?= $form->field($signupMail, 'email')->textInput(['class' => 'form-control','placeholder' =>'Enter Your Email Address'])->label(false);?>
            </div>
            <div class="form-group">
              <button type="submit" class="btn-quote">Submit</button>
            </div>
        <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
<!--NEWSLETTER SECTION END-->
<?php
            
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use app\helpers\PriceHelper;
    use yz\shoppingcart\ShoppingCart;
    use app\modules\MubAdmin\modules\csvreader\models\Origin;
    use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
    
    $user = new \app\models\MubUser();



?>
<style type="text/css">
.carousel-indicators{
  display: none;}
  .banner-slider-main-outer{
    background-color: #fff!important;
  }#mgQuant{
    padding-left: 5px;
    padding-right: 5px;
    border: 1px solid grey;
}
</style>
</div>
<section class="category-banner-panel">

</section>
<div class="listing-wide-main-panel" id="cart-view">
<div class="col-xs-12 nopadding search-keywords-main">
        <ul>
        <li><a href="#">Wide Range</a></li>
        <li><a href="#">Great prices</a></li>
        <li><a href="#">Best Discounts</a></li>
        <li><a href="#">Quick Delivery</a></li>
        <li><a href="#">24x7 Customer care</a></li>
        <li><a href="#">Free Shipping</a></li>
        <li><a href="#">Easy Renewals</a></li>
        </ul>
  </div>
</div>

<section class="product-main-outer-panel">

    <div class="row">
        
        <div class="col-xs-12 nopadding shopping-cart-main-outer">
        <div class="col-xs-12 nopadding shopping-cart-top">
        <div class="col-sm-6 col-md-6 nopadding shopping-cart-heading">Shopping Cart</div>
        <div class="col-sm-6 col-md-6 nopadding shopping-cart-conitune"><a href="/"><i class="fa fa-shopping-cart"></i> Continue Shopping</a></div>
        </div>
        
        <?php if(!empty($cartItems)){
       foreach($cartItems as $item){
       $itemCartId = $item['id'];
       $itemCartName = $item['name'];
        }?>
         <div class="col-xs-12 nopadding shopping-cart-bottom-main">
          <div class="col-xs-12 col-sm-8 col-md-8 nopadding shopping-cart-left-main">
            <?php 
             foreach($cartItems as $item){
               $itemCartId = $item['id'];
               $itemCartName = $item['name'];
               $itemCartSlug = $item['slug'];
            ?>
          <div class="col-xs-12 nopadding shopping-cart-item-con">
          <div class="col-xs-3 nopadding shopping-item-pic">
            <?php
                $appPath = \Yii::getAlias('@app'); 
                if(file_exists($appPath."/images/".$item['cover_image'].".jpg"))
                {
                  $extension = '.jpg';
                }
                else
                {
                  $extension = '.png';
                }
                $quant = $item->getQuantity();?>
                  <a href="/site/product?name=<?= $itemCartSlug;?>"><img src="/images/<?= $item['cover_image'].$extension;?>" alt="pic"></a></div>
          <div class="col-xs-4 nopadding shopping-item-details"><a href="/site/product?name=<?= $itemCartSlug;?>"><h3><?= $itemCartName;?></h3></a><br/>
          <h4>Annual Subscription</h4><br/>
          QTY: <span style="padding-left: 5px; cursor: pointer;"><i class="fa fa-minus decrementcart" id="dec_<?=$itemCartId;?>"></i></span>
          <span id="mgQuant_<?= $itemCartId;?>"><?= $item->getQuantity();?></span>
          <span style="padding-right: 10px; cursor: pointer;"><i class="fa fa-plus incrementcart" id="inc_<?=$itemCartId;?>"></i></span>
                <?php
        $alreadyInCart = $cart->getPositionById($itemCartId);
        ?>

          <a class="btn-updateaddcart cusbutton" id="add-cart_<?= $itemCartId;?>" style="cursor: pointer;">Update</a>
       
          <!--<span>Shipping to India</span>-->
          <!-- <label>Comments</label>
          <textarea name="" cols="" rows="" placeholder=""></textarea>
          <div class="col-xs-12 nopadding shopping-cart-wishlist"><span><a href="#">Move to Wishlist</a></span><span><a href="#">Remove</a></span></div> -->
          </div>
          <div class="row">
          <div class="col-xs-2 nopadding shopping-cart-quantity"><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= floor($item->getPrice());?>  x <?= $item->getQuantity();?></div>
          <div class="col-xs-2 nopadding shopping-cart-price"><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= $quant*(floor($item->getPrice()));?><a class="btn-remove-item" id="remove_item_<?= $item['id'];?>"><i class="fa fa-trash pull-right" style="margin-top: 3px;"></i></a></div>
          </div>
          </div>
          <?php }?>
          
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 nopadding shopping-cart-right-main">
          <div class="shopping-summary-outer">
          <h3>Price Details</h3>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Subtotal: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?= floor($cart->getCost());?></span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Estimated Shipping: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  0</span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Estimated tax: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  0</span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Discount Value: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?php if(!empty(substr($discValue, 0, -3))){
            echo substr($discValue, 0, -3);
          }else{?> 0
         <?php };?></span></div>
          <div class="col-xs-12 nopadding shopping-subtotal-outer">Amount Payable: <span><?php echo PriceHelper::getCurrencyCode($origin->id);?>   <?= floor($cart->getCost());?></span></div>
          <div class="col-xs-12 nopadding shopping-checkout-outer"><a href="/site/login-checkout"><i class="fa fa-lock"></i> Checkout</a></div>
          </div>
          </div>
         </div>
       <?php }  else {?>

        <div class="container" style="background-color: #fff!important;">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h1 style="margin-top: 1em;">Your Cart is Empty.</h1></div>
          </div>
        </div>
        <?php }?>
        </div> 
  </div>
</section>


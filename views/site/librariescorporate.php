<section class="category-banner-panel">

</section>

<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           



<section class="refund-policy-content shiping-content-outer">
	<div class="col-xs-12 nopadding main-inner-heading"><h1>Libraries & Corporate Orders</h1></div>
        	<div class="col-xs-12 nopadding shiping-content-inner">
            
            	<div class="col-xs-12 nopadding shiping-content-first">
                     <div class="col-md-4 col-sm-4 col-xs-12 nopadding shiping-content-left coprate-left-img"><img src="/images/coprate-left-img1.jpg" alt=""></div>
                     <div class="col-md-8 col-sm-8 col-xs-12 nopadding shiping-content-right">
                        <p>Libraries can place their order by sending their list of magazines to Customer Service at <strong><a href="mailto:Contact@MagazinesWorld.Online">Contact@MagazinesWorld.Online</a></strong>, upon receipt, Magazines World will check the availability and will send a Quotation, with applicable discount.</p>
                     </div>
                 </div>
                 
                 <div class="col-xs-12 nopadding shiping-content-first">
                     <div class="col-md-4 col-sm-4 col-xs-12 nopadding shiping-content-left coprate-left-img"><img src="images/coprate-left-img2.jpg" alt=""></div>
                     <div class="col-md-8 col-sm-8 col-xs-12 nopadding shiping-content-right">
                        <p>We process Corporate Orders, too! If you are a corporate house, please feel free to write to <strong><a href="mailto:Contact@MagazinesWorld.Online">Contact@MagazinesWorld.Online</a></strong> and send your selection of Magazine, upon receipt, our customer service representative will get in touch with you, with further details.</p>
                        <p>Libraries and Corporates can request for Magazines Catalogs / List for selection purpose.</p>
                     </div>
                 </div>
                 
            </div>
</section>

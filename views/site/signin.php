<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
?>
</style>

<button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
<div class="login-popup-outer">
     <h3 class="magazines-by-sectors-heading "><strong>Login</strong></h3>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'frontend-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
            <div class="col-xs-12 nopadding product-advertise-form">
              <div class="col-xs-12 nopadding product-advertise-form-inner">
                <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'login-popup-panel-main input'])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'login-popup-panel-main input'])->label(false); ;?>
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button">
                <input type="submit" name="login" value="login">
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con"><span><a href="#" class="frontend-forget" id="forget">Forgot Password</a></span> <span>Not a member? <a href="#" class="signup-user login-button-nav" id="signup-modal"> Register</a></span></div>
            </div>
         <?php ActiveForm::end(); ?>
</div>

<div class="modal-footer">  
</div>

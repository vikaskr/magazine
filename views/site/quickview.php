
<?php 
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\modules\MubAdmin\modules\csvreader\models\Language;
use app\modules\MubAdmin\modules\csvreader\models\Publisher;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
$cart = new ShoppingCart();
// $origin = new Origin();
//     $originName = $origin::find()->where(['del_status' => '0', 'id' => $magazineDetails->origin_id])->one(); 
//     $language = new Language();
//     $languageName = $language::find()->where(['del_status' => '0', 'id' => $magazineDetails->language_id])->one(); 
//     $publisher = new Publisher();
//     $publisherName = $publisher::find()->where(['del_status' => '0', 'id' => $magazineDetails->publisher_id])->one(); 
?>
<!-- Modal -->
    <div class="modal-dialog" style="border: 5px solid #fbc103!important; width: 700px!important;">
    <button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
<div class="quick-view-popup-main-inner" >
<div class="col-xs-6 col-md-6 col-sm-6 nopadding quick-view-popup-left">
    <a href="/site/product?name=<?= $magazineDetails->slug;?>">
        <?php
        $appPath = \Yii::getAlias('@app'); 
        if(file_exists($appPath."/images/".$magazineDetails->cover_image.".jpg"))
        {
            $extension = '.jpg';
        }
        else
        {
            $extension = '.png';
        }
        ?>
          <img src="/images/<?= $magazineDetails->cover_image.$extension;?>" alt="featured magazine pics">
        </a>
</div>
<div class="col-xs-6 col-md-6 col-sm-6 nopadding quick-view-popup-right">
<h4><?= $magazineDetails->name; ?></h4>
<span class="col-xs-12 nopadding quick-issues-con"><?= $magazineDetails->issues_per_year; ?> Isuues Per Year</span>
<div class="col-xs-12 nopadding quick-view-annual-con">Annual Subscription<span><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= number_format($magazineDetails->getPrice());?></span></div>
<!-- <div class="col-xs-12 nopadding quick-total-price"> <span>$274</span> $243</div> -->
<div class="col-xs-12 nopadding quick-quantity-con">
<h5>Quantity</h5>
<ul>
<li class="decrementcart" id="dec_<?= $magazineDetails->id;?>"> <i class="fa fa-minus"></i></li>
<li id="mgQuant_<?= $magazineDetails->id;?>">1</li>
<li class="incrementcart" id="inc_<?= $magazineDetails->id;?>"><i class="fa fa-plus"></i></li>
</ul><br/><br/>
<div class="row" style="min-height: 100px!important;"><?php if($magazineDetails->description !== 'NA'){?><h5>Description</h5>
<p><?= substr($magazineDetails->description, 0, 150); ?></p><?php }?></div>
<div class="col-xs-12 nopadding quick-comment-con">
<!-- <label><i class="fa fa-comments"></i> Comments</label>
<textarea name="" cols="" rows="" placeholder="Let us know if you have any issue or Cover preference"></textarea> -->
<div class="col-xs-12 nopadding quick-buttons-con">
<?php
$alreadyInCart = $cart->getPositionById($magazineDetails->id);
?>
<a class="btn-quickaddcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart-quick_<?= $magazineDetails->id;?>" style="cursor: pointer;">Add To Cart</a>
<a href="/site/checkout" target="_blank" class="btn-quickaddcart <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $magazineDetails->id;?>" style="width: 15px;">View Cart</a>
<a href="/site/product?name=<?= $magazineDetails->slug;?>">View Details</a>
</div>
</div>
</div>
</div>

</div>
</div>

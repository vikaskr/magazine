<?php 
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
use yz\shoppingcart\ShoppingCart;

use yii\widgets\LinkPager;
$cart = new ShoppingCart();
use app\helpers\PriceHelper;

$cartItems = $cart->getPositions();

?>
<?php 
    foreach ($result as $value) {                       
    ?>
    <div class="featured-magazine-bottom-outer">
    <div class="col-xs-12 nopadding featured-magazine-bottom-inner">

    <div class="col-xs-12 nopadding featured-magazine-pic">
      <a href="/site/product?name=<?= $value['slug']?>">
      <?php
      $appPath = \Yii::getAlias('@app'); 
      if(file_exists($appPath."/images/".$value['cover_image'].".jpg"))
      {
        $extension = '.jpg';
      }
      else
      {
        $extension = '.png';
      }
      ?>
        <img src="/images/<?= $value['cover_image'].$extension;?>" alt="featured magazine pics" style="height: 100%;">
      </a>

     <div class="quick-veiw-con">
      <span class="quick-view-text"><a href="/site/quickview?name=<?= $value['slug'];?>" id="quick-view" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i> Quick View</a></span></div>
    </div> 
    <?php
          $alreadyInCart = $cart->getPositionById($value['id']);
      ?>
    <a href="/site/product?name=<?= $value['slug']?>"><h4><?= substr($value['name'], 0, -12)?></h4></a>
    <div class="col-xs-12 nopadding featured-magazine-price"><span><?php echo PriceHelper::getCurrencyCode($origin->id);?>  <?php echo number_format(PriceHelper::getCountryPrice($value['price']));?></span>
      <span>
        <a data-toggle="tooltip" title="Add To Cart" class="btn-sortaddcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart-sort_<?= $value['id'];?>"><i class="fa fa-shopping-cart"></i></a>
<a data-toggle="tooltip" title="Checkout" href="/site/checkout" target="_blank" class="btn-sortaddcart <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $value['id'];?>"><img src="/images/checkout.png" height="20"></a>
       
      </span></div>
    </div>
    </div>
    <?php }?>
    <?php if(!empty($result)){?>
    <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
     'pagination' => $pages,
     ]);
    ?></center><?php }?></div></div>
   <?php $this->registerJs("
  $(document).ready(function(){
     $(\"[data-toggle='tooltip']\").tooltip();
   });"); ?>

   
                        
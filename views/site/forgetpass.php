<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
</style>

<button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
<div class="login-popup-outer">
     <h3 class="magazines-by-sectors-heading "><strong>Forget Password</strong></h3>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'frontend-forget','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
            <div class="col-lg-12 nopadding product-advertise-form" style="margin-top: -1em;">
              <div class="col-lg-12 nopadding product-advertise-form-inner">
                <div class='col-sm-10 col-sm-offset-1'>
               <?= $form->field($model, 'email')->textInput()->input('email', ['placeholder' => "Enter Your Registered Email", 'class' => 'login-popup-panel-main input'])->label(false); ?></div>
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button">
                <input type="submit" name="login" value="Submit">
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con"><span></span> <span>Not a member? <a href="#" class="signup-user login-button-nav" id="signup-modal"> Register</a></span></div>
            </div>
         <?php ActiveForm::end(); ?>
</div>

<div class="modal-footer">  
</div>


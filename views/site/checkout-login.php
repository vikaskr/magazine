<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
    $user = Yii::$app->user->identity;
?>
<style type="text/css">.has-error .help-block{
  color: #fff!important;}
  .shopping-cart-left-inner{
    padding: 0px!important;
  }
  .shopping-cart-left-main{
    padding: 10px!important;
  }
</style>

<div class="col-xs-12 col-sm-12 col-md-12 nopadding shopping-cart-left-main shopping-login-right-outer">
     <div class="col-xs-12 nopadding shopping-cart-left-inner">
      <?php $form = ActiveForm::begin(['options' => ['id' => 'checkout-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
     <div class="col-xs-12 nopadding product-advertise-form-inner shopping-login-checkout-fields">
      <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'login-popup-panel-main input'])->label(false); ?>
        <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'login-popup-panel-main input'])->label(false); ;?>
      </div>

     <div class="col-xs-12 nopadding product-advertise-form-inner"><input type="submit" name="login" value="Sign In">
     </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
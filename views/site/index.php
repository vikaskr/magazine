<?php 
use app\modules\MubAdmin\modules\csvreader\models\Magazine;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
use app\modules\MubAdmin\modules\csvreader\models\Subject;
use yz\shoppingcart\ShoppingCart;
use app\helpers\PriceHelper;
$cart = new ShoppingCart();
$cartItems = $cart->getPositions();

?>
<!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active banner-slider-pic01 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Welcome to <span>magazines</span> World</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <div class="item banner-slider-pic02 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Find Wide range of <span>Computer </span> magazines</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
    <div class="item banner-slider-pic03 banner-slider-con">
    <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Biggest Collection of <span>Fashion</span> magazines</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
     </div>
     <div class="item banner-slider-pic04 banner-slider-con">
     <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Subscribe <span>Food and Beverage</span> magazines</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
             <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
     </div>
      <div class="item banner-slider-pic05 banner-slider-con">
      <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Subscribe <span>Business and Finance</span> magazines</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      
     
     <div class="item banner-slider-pic06 banner-slider-con">
      <div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Subscribe <span>Health and Fitness</span> magazines</span>
          <h1><span>Subscribe To Magazines from around the world</span></h1>
          <p>Magazines for everyone!</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      
       
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only"></span>
  </a>
</div>

</div>
<!--BANNER SLIDER END-->
<div id="location-map-block">
  <div id="location-homemap-block"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div id="location-link-item">
          <button id="map_list"><i class="fa fa-angle-double-up"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--SEARCH PANEL START-->
<div id="search-categorie-item-block">
  <form id="categorie-search-form" action="/site/category?name= ">
    <h1>Search for your Favourite magazines</h1>
    <div class="col-sm-9 col-md-10 nopadding home-banner-search-field">
      <div id="search-input">

        <div class="col-sm-12 nopadding">
          <div class="form-group typeahead__container">
            <div class="form-group typeahead__field">
           <input id="exampleInputAmount" class="js-typeahead-input form-control form-control-lg exampleInputAmount" name="name" type="search" autocomplete="off" placeholder="Search Magazines"/ required="">
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="col-sm-3 col-md-2 text-right nopadding-right home-banner-search-button">
      <div id="location-search-btn">
        <button type="submit" id="search-btn"><i class="fa fa-search"></i>Search</button>
      </div>
    </div>
  </form>
</div>

<!--SEARCH PANEL END-->



<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a href="#">Wide Range</a></li>
            <li><a href="#">Great prices</a></li>
            <li><a href="#">Best Discounts</a></li>
            <li><a href="#">Quick Delivery</a></li>
            <li><a href="#">24x7 Customer care</a></li>
            <li><a href="#">Free Shipping</a></li>
            <li><a href="#">Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           



<!--HIGHLIGHTED CATEGORY PANEL START-->
<div id="search-categorie-item" class="highlighted-categories-main-panel">
      <div class="col-sm-12 text-center">
   
          <div class="col-md-12 categories-heading bt_heading_3">
            <h2 class="main-heading-text">Highlighted<span>Categories</span></h2>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=fashion"><div class="cate_item_social hi-icon"><i class="fa fa-home"></i></div></a>
                <h1><a href="/site/category?name=fashion">Fashion</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=business--finance"><div class="cate_item_social hi-icon"><i class="fa fa-briefcase"></i></div></a>
                <h1><a href="/site/category?name=business--finance">Business & Finance</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=health--fitness"><div class="cate_item_social hi-icon"><i class="fa fa-heartbeat"></i></div></a>
                <h1><a href="/site/category?name=health--fitness">Health &amp; Fitness</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=womens-interest"><div class="cate_item_social hi-icon"><i class="fa fa-female"></i></div></a>
                <h1><a href="/site/category?name=womens-interest">Women's Interest</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=mens-interest"><div class="cate_item_social hi-icon"><i class="fa fa-male"></i></div></a>
                <h1><a href="/site/category?name=mens-interest">Men's Interest</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=children"><div class="cate_item_social hi-icon"><i class="fa fa-child"></i></div></a>
                <h1><a href="/site/category?name=children">Children</a></h1>
              </div>
            </div>
          </div>
         <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=home--garden"><div class="cate_item_social hi-icon"><i class="fa fa-home"></i></div></a>
                <h1><a href="/site/category?name=home--garden">Home &amp; Garden</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=architecture"><div class="cate_item_social hi-icon"><i class="fa fa-users"></i></div></a>
                <h1><a href="/site/category?name=architecture">Architecture</a></h1>
              </div>
            </div>
          </div>

         <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=news--politics"><div class="cate_item_social hi-icon"><i class="fa fa-file"></i></div></a>
                <h1><a href="/site/category?name=news--politics">News &amp; Politics</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=travel"><div class="cate_item_social hi-icon"><i class="fa fa-plane"></i></div></a>
                <h1><a href="/site/category?name=travel">Travel</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=hobbies--puzzles"><div class="cate_item_social hi-icon"><i class="fa fa-trophy"></i></div></a>
                <h1><a href="/site/category?name=hobbies--puzzles">HOBBIES & PUZZLES</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=cooking-food--beverage"><div class="cate_item_social hi-icon"><i class="fa fa-coffee"></i></div></a>
                <h1><a href="/site/category?name=cooking-food--beverage">Cooking, Food &amp; Beverage</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=lifestyle"><div class="cate_item_social hi-icon"><i class="fa fa-female"></i></div></a>
                <h1><a href="/site/category?name=lifestyle">Lifestyle</a></h1>
              </div>
            </div>
          </div>
          <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=sports--recreation"><div class="cate_item_social hi-icon"><i class="fa fa-spotify"></i></div></a>
                <h1><a href="/site/category?name=sports--recreation">Sports &amp; Recreation</a></h1>
              </div>
            </div>
          </div>
           <div class="featured-listing-con">
            <div class="categorie_item">
              <div class="cate_item_block hi-icon-effect-8">
                <a href="/site/category?name=weddings"><div class="cate_item_social hi-icon"><i class="fa fa-users"></i></div></a>
                <h1><a href="/site/category?name=weddings">Weddings</a></h1>
              </div>
            </div>
          </div>
            
      </div>
 
</div>

<!--HIGHLIGHTED CATEGORY PANEL END-->

<!--TABINATION  START-->
<div id="feature-item_listing_block">
      <div class="col-sm-12 text-center">
        <div class="col-xs-12 nopadding featured-magazine-tabs-outer">
        
        <div class="col-xs-12 featured-tabs-top-con">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <!--<ul class="nav nav-tabs">
                            <li class="active"><a href="#feature-magazine" data-toggle="tab">Featured Magazines</a></li>
                            <li><a href="#best-seller" data-toggle="tab">Best Sellers</a></li>
                            <li><a href="#new-arrival" data-toggle="tab">New Arrivals</a></li>
                        </ul>-->
                        
                        <ul class="nav nav-tabs new">
                          <img src="/images/feature.png" width="550">
                            <!-- <li class="active"><a href="#feature-magazine" class="feature-tab-pic" data-toggle="tab">&nbsp;</a></li>
                            <li><a href="#best-seller"  class="best-seller-tab-pic" data-toggle="tab">&nbsp;</a></li>
                            <li ><a href="#new-arrival" class="new-arrivals-tab-pic" data-toggle="tab">&nbsp;</a></li> -->
                        </ul>
              </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="feature-magazine">

                          <?php 
                          foreach ($nationalMagazines as $value) {

                            ?>

                          <div class="featured-magazine-bottom-outer">
                          <div class="col-xs-12 nopadding featured-magazine-bottom-inner">

                          <div class="col-xs-12 nopadding featured-magazine-pic">
                            <a href="/site/product?name=<?= $value['slug']?>">
                            <?php
                            $appPath = \Yii::getAlias('@app'); 
                            if(file_exists($appPath."/images/".$value['cover_image'].".png"))
                            {
                            	$extension = '.png';
                            }
                            else
                            {
                            	$extension = '.jpg';
                            }
                            ?>
                              <img src="/images/<?= $value['cover_image'].$extension;?>" alt="featured magazine pics" style="height: 100%!important;">
                            </a>

                          <div class="quick-veiw-con">
                            <span class=""><a href="/site/quickview?name=<?= $value['slug'];?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i> Quick View</a></span></div>
                          </div>  
                          <?php
                            $alreadyInCart = $cart->getPositionById($value['id']);
                            ?>
                          <a href="/site/product?name=<?= $value['slug']?>"><h4><?= substr($value['name'], 0, -12)?></h4></a>
                          <div class="col-xs-12 nopadding featured-magazine-price"><span><?php echo PriceHelper::getCurrencyCode($origin->id);?> <?= number_format($value->getPrice());?></span>
                            <span>
                              <a data-toggle="tooltip" title="Add To Cart" class="btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $value['id']?>"><i class="fa fa-shopping-cart"></i></a>
                              <a data-toggle="tooltip" title="Checkout" href="/site/checkout" target="_blank" class="btn-addcart <?=($alreadyInCart)? '' :'check' ?>" id="checkout_<?= $value['id']?>"><img src="/images/checkout.png" height="20"></a>
                            </span></div>
                          </div>
                          </div>
                          <?php }?>
                           </div>                           
                        </div>                         
                    </div>
                </div>
            </div>
        </div>
     </div>
</div>
        
       



<!--SHOP BY CATAGORIES SECTION START-->
<div class="shop-categories-main-outer-panel" id="shopsection">
<div class="col-xs-12 nopadding shop-categories-main-outer">

<h2 class="main-heading-text">SHOP BY  <br><span class="categories-button"><img src="/images/img_categories_button.png" alt="categories pic"></span></h2>
<div class="shop-categories-main-inner">
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=womens-interest"><img src="/images/Women-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=lifestyle"><img src="/images/Lifestyle-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=architecture"><img src="/images/Architecture-MAGAZINES.jpg" alt="shop pic ">
</a></div>
</div>

<div class="shop-categories-main-inner">
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=lifestyle"><img src="/images/Beauty-Makeup-MAGAZINES.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=computers--electronics"><img src="/images/Computer-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=children"><img src="/images/Children-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=enrichment"><img src="/images/entertainment-magazines.jpg" alt="shop pic ">
</a></div>

</div>

<div class="shop-categories-main-inner">
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=animals--pets"><img src="/images/Animals- Pets-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=photography"><img src="/images/Photography-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=travel"><img src="/images/Travel-Tourism-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=business--finance"><img src="/images/Business Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=celebrity--films"><img src="/images/Celebrity-Films-Magazines.jpg" alt="shop pic ">
</a></div>
</div>

<div class="shop-categories-main-inner">
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=music--dance"><img src="/images/Music-Dance-MAGAZINES.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=celebrity--films"><img src="/images/Celebrity-Films-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=jewellery-design"><img src="/images/Jewellery-MAGAZINES.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=parenting--family"><img src="/images/parenting_family.jpg" alt="shop pic ">
</a></div>
</div>

<div class="shop-categories-main-inner">
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=health--fitness"><img src="/images/Health-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=interior-designing"><img src="/images/Interior-Design-Magazines.jpg" alt="shop pic ">
</a></div>
<div class="col-xs-12 nopadding shop-categories-main-con"><a href="/site/category?name=mens-interest"><img src="/images/Mens-Magazines.jpg" alt="shop pic ">
</a></div>

</div>


</div>
</div>
<!--SHOP BY CATAGORIES SECTION END-->

<!--FASHION MAGAZINES PIC -->
<div class="fashion-magazine-main-outer"><a href="/site/category?name=fashion"><img src="/images/img_fashion-magazines_pic.jpg" alt="pic"></a></div>
<!--FASHION MAGAZINES PIC END -->

<div class="vfx-counter-block">
  <div class="vfx-item-container-slope vfx-item-bottom-slope vfx-item-left-slope"></div>
    <div class="vfx-item-counter-up">
        <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-file"></i></div>
            <div class="text_number">Wide </div>
            <div class="counter_text">Range of Magazines</div>
          </div>
        </div>
         <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-users"></i></div>
             <div class="text_number">View </div>
            <div class="counter_text">Collection</div>
          </div>
        </div>
         <div class="vfx-counter-main">
          <div class="vfx-item-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th"></i></div>
             <div class="text_number">Subscribe </div>
            <div class="counter_text">Magazines</div>
          </div>
        </div>
        <div class="vfx-counter-main">
          <div class="vfx-item-countup last-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th-list"></i></div>
             <div class="text_number">Renew</div>
            <div class="counter_text">Subscription</div>
          </div>
        </div>
        <div class="vfx-counter-main">
          <div class="vfx-item-countup last-countup">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-th-list"></i></div>
            <div class="text_number ">Easy</div>
            <div class="counter_text">Ordering</div>
          </div>
        </div>

    </div>
  </div>
  
  <div class="modal fade quick-view-popup-main-panel" id="quick-view-popup" role="dialog">
    <div class="modal-dialog">
    <button type="button" class="close" onClick="closeContact();" data-dismiss="modal">&times;</button>
      <div class="quick-view-popup-main-inner" id="quick-view-popup">

      </div>
    </div>
  </div>
 <script src="/js/jquery-2.1.0.min.js"></script>
       <script src="/js/jquery.typeahead.js"></script>
       <?php
            $subject_slug[] = array();
            $subje = new Subject();
            $subject =  $subje::find()->select('subject_slug')->where(['del_status' => '0','status' => 'active'])->all();
            
            foreach ($subject as $value)
            {
                $subject_slug[] = $value->subject_slug;               
            }

            $data = str_replace('--', ' ', $subject_slug);
        
        ?>
       <script>

             var data = {
                 countries: <?php echo json_encode($data); ?>

             };

             $('.js-typeahead-input').typeahead({
                 minLength: 1,
                 order: "asc",
                 group: true,
                 maxItemPerGroup: 5,
                 hint: true,
                 emptyTemplate: "No result for {{query}}",
                 source: {
                     Select: {
                         data: data.countries
                     }
                 },
                 debug: true
             });
    </script>

<?php $this->registerJs("
  $(document).ready(function(){
     $(\"[data-toggle='tooltip']\").tooltip();
   });"); ?>


<?php 

	$name = Yii::$app->getRequest()->getQueryParam('name');

    $bisac = new \app\modules\MubAdmin\modules\csvreader\models\Bisac;

    $oneBisac = $bisac::find()->where(['slug' => $name])->one();

	$product = new \app\modules\MubAdmin\modules\csvreader\models\Product();
	$allProducts = \Yii::$app->db->createCommand("SELECT `product`.`id` as `product_id`,
    	`product`.`ing_product_id`,
    	`product`.`product_name`,
    	`product`.`format`,
    	`product`.`publish_date`,
    	`product`.`bisac`,
    	`product`.`type`,
    	`products_author`.`author_id`,
    	`author`.`f_name`,
    	`author`.`l_name`,
    	`author`.`key_name`,
    	`publisher_products`.`publisher_id`,
    	`publisher`.`publisher`,
    	`publisher`.`slug` as `publisher_slug`
    	 FROM `product` 
    	 INNER JOIN `products_author` ON `product`.`id` = `products_author`.`products_id` 
    	 LEFT JOIN `author` ON `products_author`.`author_id` = `author`.`id` 
    	 INNER JOIN `publisher_products` ON `publisher_products`.`product_id` = `product`.`id`
    	 LEFT JOIN `publisher` ON `publisher_products`.`publisher_id` = `publisher`.`id`
    	 where `product`.`status` = 'active' AND `product`.`product_name` <> 'NA' LIMIT 200")->queryAll();

?>
<div class="tz-2 tz-2-admin">
	<div class="tz-2-com tz-2-main">
		<div class="split-row">
			<div class="">
				<div class="box-inn-sp ad-inn-page">
					<div class="tab-inn ad-tab-inn">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th><B>Title</B></th>
										<th><B>Author / Contributer</B></th>
										<th><B>Bind</B></th>
										<th><B>Publisher</B></th>
										<th><B>Pub Date</B></th>
										<th><B>List_Price</B></th>
										<th><B>Disc</B></th>
										<th><B>S_Price</B></th>
										<th><B>Quantity</B></th>
										<th><B>Cart</B></th>
									</tr>
								</thead>
								<?php

									foreach($allProducts as $product)
									{
										$dateAndTime = $product['publish_date'];
										$date = date('M Y', strtotime($dateAndTime));
								?>
							 	<tbody>
									<tr>
										<td class="titl">
											<a href="/site/book-details?name=<?= $product['slug'];?>"><?= $product['product_name']; ?></a>
										</td>
										<td class="auth">Sharikant Rajiv Gupta</td>
										<td class="bran"><?= $product['type']; ?></td>
										<td class="pub"> LAP Lambert Acadmic </td>
										<td style="font-size: 10px;"><?= $date; ?></td>
										<td><span class="label label-primary">$99.00</span>  </td>
										<td><span class="label label-success">5%</span></td>
										<td><span class="label label-info">$80</span></td>
										<td><input type="text" value="1" style="padding-left: 10px; width: 40px;"></td>
							     		<td class="fa fa-shopping-cart"></td>
							 		</tr>	
								</tbody>
								<?php }?>
						   </table>
						</div>
					</div>
				</div>
				<div class="admin-pag-na">
					<ul class="pagination list-pagenat">
						<li class="disabled"><a href="#!!"><i class="material-icons">chevron_left</i></a> </li>
						<li class="active"><a href="#!">1</a> </li>
						<li class="waves-effect"><a href="#!">2</a> </li>
						<li class="waves-effect"><a href="#!">3</a> </li>
						<li class="waves-effect"><a href="#!">4</a> </li>
						<li class="waves-effect"><a href="#!">5</a> </li>
						<li class="waves-effect"><a href="#!">6</a> </li>
						<li class="waves-effect"><a href="#!">7</a> </li>
						<li class="waves-effect"><a href="#!">8</a> </li>
						<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
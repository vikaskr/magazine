<section class="category-banner-panel">

</section>

<!--LISTING PANEL START-->
<div class="listing-wide-main-panel">
<div class="col-xs-12 nopadding search-keywords-main">
            <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>

</div>
 <!--LISTING PANEL END-->           



<section class="refund-policy-content privacy-policy-content">
		<div class="col-xs-12 nopadding main-inner-heading"><h1>Payment Methods</h1></div>
        <div class="col-xs-12 refund-content-inner">
            <div class="col-md-4 col-sm-4 col-xs-12 nopadding privacy-policy-content-left"> <img src="/images/payment-options.jpg"></div>
           <div class="col-md-8 col-sm-8 col-xs-12 nopadding privacy-policy-content-right">
               <p>You can pay online using your credit card, through payment gateway, or can pay by Bank Draft / Cheque or Bank Transfer. </p>
               <p> All MasterCard & Visa Credit Cards (Domestic)</p>
               <p>8 Major bank's EMI options <span> SBI / ICICI / HDFC / Axis / Kotak / IndusInd / HSBC / Central Bank of India</span></p>
               <p>100+ Debit Cards <span> MasterCard / Visa / Maestro / RuPay</span></p>
               <p>53+ Netbanking</p>
               <p>4 Cash Cards <span>ITZCash / ICash / Oxicash / PayCash</span></p>
               <p>8 Wallets <span>Paytm / MobiKwik / JioMoney / PayZapp / Jana Cash / Freecharge / SBI Buddy / The Mobile Wallet</span></p>
               <p>46 Bank's IMPS<span>MasterCard & Visa Credit Cards (International)</span><span>American Express / Amex EMI, JCB & Diners Club</span></p>
               <p>Library & Corporates can make payment by Bank Transfer / Demand Draft / Cheque</p>
               <p>Please get in touch with Customer Support for more details at Contact@MagazinesWorld.Online</p>
           </div>
        </div>
</section>
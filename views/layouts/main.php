<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">

<?= Html::csrfMetaTags() ?>
    <title>Magazines World - Print Digirtal</title>
    <?php $this->head() ?>
<?php $this->beginBody() ?>
 <body>

<div class="banner-slider-main-outer">

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
    <li data-target="#myCarousel" data-slide-to="5"></li>
  </ol>

  

<div id="logo-header" data-spy="affix" data-offset-top="500" class="navbar-fixed">

      <div class="col-sm-2 col-md-2 col-xs-9 col-lg-3 nopadding">
        <div id="logo"> <a href="/"><img src="/images/logonew.png" alt="logo"></a> </div>
      </div>
      <div class="col-sm-10 col-md-10 col-lg-9 text-right nopadding navigation-right-outer">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#thrift-1" aria-expanded="false"> <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="collapse navbar-collapse" id="thrift-1"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
            <div id="nav_menu_list">
              <ul>
                <li class="<?= ($this->params['page'] == 'home') ? 'active' :'' ;?>"><a href="/">Home</a></li>
                <li class="<?= ($this->params['page'] == 'about') ? 'active' :'' ;?>"><a href="/site/about">About Magazines World</a></li>
                <li class="<?= ($this->params['page'] == 'category') ? 'active' :'' ;?>" id="magazine-categories-con"><a href="#">All Categories</a>
                
                <div class="magazine-categories-drop-down" id="magazine-categories-drop">
                <ul>
                <li><a href="/site/category?name=animals--pets">Animals &amp; Pets</a></li>
                <li><a href="/site/category?name=architecture">Architecture</a></li>
                <li><a href="/site/category?name=art--design">Art &amp; Design</a></li>
                <li><a href="/site/category?name=auto--cycles">Auto &amp; Cycles</a></li>
                <li><a href="/site/category?name=aviation">Aviation</a></li>
                <li><a href="/site/category?name=ayurveda--herbal">Ayurveda,Herbal & Organic</a></li>
                <li><a href="/site/category?name=agriculture">Agriculture</a></li>
                <li><a href="/site/category?name=business--finance">Business &amp; Finance</a></li>
                <li><a href="/site/category?name=celebrity--films">Celebrity &amp; Films</a></li>
                <li><a href="/site/category?name=children">Children</a></li>
                <li><a href="/site/category?name=comics">Comics</a></li>
               
                </ul>
                
                <ul>
                <li><a href="/site/category?name=computers--electronics">Computers &amp; Electronics</a></li>
                <li><a href="/site/category?name=cooking-food--beverage">Cooking, Food &amp; Beverage</a></li>
                <li><a href="/site/category?name=craft--hobbies">Craft &amp; Hobbies </a></li>
                <li><a href="/site/category?name=cricket">Cricket </a></li>
                <li><a href="/site/category?name=culture">Culture</a></li>
                <li><a href="/site/category?name=education">Education</a></li>
                <li><a href="/site/category?name=enrichment">Enrichment</a></li>
                <li><a href="/site/category?name=entertainment--tv">Entertainment &amp; TV</a></li>
                <li><a href="/site/category?name=fashion">Fashion</a></li>
                <li><a href="/site/category?name=fiction">Fiction</a></li>
                <li><a href="/site/category?name=gaming">Gaming</a></li>
                
                </ul>
                
                <ul>
                
                <li><a href="/site/category?name=general-interest">General Interest</a></li>
                <li><a href="/site/category?name=health--fitness">Health &amp; Fitness</a></li>
                <li><a href="/site/category?name=history">History</a></li>
                <li><a href="/site/category?name=hobbies--puzzles">Hobbies &amp; Puzzles</a></li>
                <li><a href="/site/category?name=home--garden">Home &amp; Garden</a></li>
                <li><a href="/site/category?name=hotels--hospitality">Hotels &amp; Hospitality</a></li>
                <li><a href="/site/category?name=industry--trade">Industry &amp; Trade</a></li>
                <li><a href="/site/category?name=interior-designing">Interior Designing</a></li>
                <li><a href="/site/category?name=jewellery-design">Jewellery Design</a></li>
                <li><a href="/site/category?name=journals">Journals</a></li>
                <li><a href="/site/category?name=language-learning-skills">Language Learning Skills</a></li>

                </ul>
                
                <ul>
                <li><a href="/site/category?name=lifestyle">Lifestyle</a></li>
                <li><a href="/site/category?name=medical">Medical</a></li>
                <li><a href="/site/category?name=mens-interest">Men's Interest</a></li>
                <li><a href="/site/category?name=music--dance">Music &amp; Dance</a></li>
                <li><a href="/site/category?name=nature">Nature</a></li>
                <li><a href="/site/category?name=newspapers">Newspapers</a></li>
                <li><a href="/site/category?name=news--politics">News &amp; Politics</a></li>
                <li><a href="/site/category?name=parenting--family">Parenting &amp; Family</a></li>
                <li><a href="/site/category?name=photography">Photography</a></li>
                <li><a href="/site/category?name=psychology">Psychology</a></li>
                <li><a href="/site/category?name=real-estate">Real Estate</a></li>

                </ul>

                <ul>
                <li><a href="/site/category?name=religious--spiritual">Religious &amp; Spiritual</a></li>
                <li><a href="/site/category?name=salon--spa">Salon &amp; Spa</a></li>
                <li><a href="/site/category?name=science--technology">Science &amp; Technology</a></li>
                <li><a href="/site/category?name=special-needs">Special Needs</a></li>
                <li><a href="/site/category?name=self-help">Self Help</a></li>
                <li><a href="/site/category?name=sports--recreation">Sports &amp; Recreation </a></li>
                <li><a href="/site/category?name=teen">Teen</a></li>
                <li><a href="/site/category?name=travel">Travel</a></li>
                <li><a href="/site/category?name=weddings">Weddings</a></li>
                <li><a href="/site/category?name=womens-interest">Women's Interest</a></li>
                <li><a href="/site/category?name=yoga">Yoga</a></li>
               
                </ul>

                </div>
                </li>
                
                 <li><a href="/#shopsection">Shop By Categories</a></li>
                
                <li id="magazine-sectors-con" class="<?= ($this->params['page'] == 'magazinebysector') ? 'active' :'' ;?>"><a href="/site/magazinebysector">Magazines By Sectors</a>
                
                <div class="magazine-categories-drop-down" id="magazine-sectors-drop">
                <ul>
                <li><a href="/site/magazinebysector#con1">Doctor's Offices</a></li>
                <li><a href="/site/magazinebysector#con2">Dentist's Offices</a></li>
                <li><a href="/site/magazinebysector#con3">Health Clubs &amp; Gyms</a></li>
                <li><a href="/site/magazinebysector#con4">Hospital &amp; Medical Clinics </a></li>
                
                </ul>
                
                <ul>
                <li><a href="/site/magazinebysector#con5">Beauty Salons &amp; Spa</a></li>
                <li><a href="/site/magazinebysector#con6">Architecture Firms</a></li>
                <li><a href="/site/magazinebysector#con7">Hotel Lobbies</a></li>
                <li><a href="/site/magazinebysector#con8">Consulting Firms</a></li>
      
                </ul>
                </div>
                
                </li>
                
                <li class="<?= ($this->params['page'] == 'contact') ? 'active' :'' ;?>"><a href="/site/contact/">Contact Us</a></li>
                <?php if(Yii::$app->user->isGuest){?><li class="login-button-nav login-nav-outer"><a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal">Login</a></li><li class="login-button-nav login-nav-outer hidden-lg hidden-md hidden-sm hidden-xs"><a href="/site/signup" id="signup-modal" data-toggle="modal" data-target="#myModal">Login</a></li><?php } else {?>
                <li class="login-button-nav login-nav-outer"><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >Logout</a></li>
                <?php }?>
                <li class="cart-nav-pic"><a href="/site/checkout#cart-view" scroll={false}><img src="/images/img_cart_pic.png" alt="cart"></a></li><span class="cartcount"><?php echo $this->render('/site/cartcount');?></span>
		         </ul>	
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>

</div>   


    <?= $content ?>
<?php echo $this->render('/site/subscriber');?>


<!--SHOP BY CATAGORIES SECTION START-->
<div class="experience-world-main-outer-panel">
<div class="col-xs-12 nopadding experience-world-main-inner">
<h2 class="main-heading-text">Experience a World Class<span class="categories-button"><img src="/images/img_services_button.png" alt="categories pic"></span></h2>
<div class="experience-service-slider-con">
 <div id="demo">
    <div id="owl-demo" class="owl-carousel">
      
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic01.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic02.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic03.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic04.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic05.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic06.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic07.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic08.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic09.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic10.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic11.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic12.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic13.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic14.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic15.jpg" alt="logo"></a></div>
      <div class="item"><a href="#"><img src="/images/img_experience_logo_pic16.jpg" alt="logo"></a></div>
       <div class="item"><a href="#"><img src="/images/img_experience_logo_pic17.jpg" alt="logo"></a></div>
</div>
</div>

      
</div>
</div>
</div>
<!--SHOP BY CATAGORIES SECTION END-->



<div id="recent-product-item-listing">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="col-md-12 recent-item-listing-heading bt_heading_3">
          <h1>Recent <span>Listing</span></h1>
          <div class="blind line_1"></div>
          <div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
          <div class="blind line_2"></div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img1.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img2.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img3.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img4.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img5.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="recent-listing-box-container-item">
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-image">
                  <h1>Food</h1>
                  <img src="/images/product/img6.png" alt="img1"> </div>
                <div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 nopadding">
                <div class="recent-listing-box-item">
                  <div class="listing-boxes-text"> <a href="listing_detail.html">
                    <h3>Hello Directory Listing</h3>
                    </a> <a href="#"><i class="fa fa-phone"></i> +91 087 654 3210</a>
                    <p>Eiusmod tempor incidiunt labore velit dolore magna aliqu sed veniam quis nostrud lorem ipsum dolor sit amet consectetur...</p>
                  </div>
                  <div class="recent-feature-item-rating">
                    <h2><i class="fa fa-map-marker"></i> Your City Here</h2>
                    <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="pricing-item-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="col-md-12 pricing-heading-title bt_heading_3">
          <h1>Pricing <span>Plan</span></h1>
          <div class="blind line_1"></div>
          <div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
          <div class="blind line_2"></div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Basic</h1>
              <hr>
              <p>Free <span>$24</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Limited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block active">
              <h1>Premium</h1>
              <hr>
              <p>Free <span>$49</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Plus</h1>
              <hr>
              <p>Free <span>$99</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Personal Training</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer class="site-footer footer-map">
  <div class="footer-top">
      <div class="">
      <div class="">
      <div class="col-xs-12 footer-links-main footer-magazine-links">
      <h1 style="text-align: center; margin-top: -1em; margin-bottom: 1em;">SUBSCRIBE MAGAZINES IN EASY STEPS</h1>
      <ul>
          <li><a href="/site/category?name=animals--pets">Animals &amp; Pets</a></li>
          <li><a href="/site/category?name=architecture">Architecture</a></li>
          <li><a href="/site/category?name=art--design">Art &amp; Design</a></li>
          <li><a href="/site/category?name=auto--cycles">Auto &amp; Cycles</a></li>
          <li><a href="/site/category?name=aviation">Aviation</a></li>
          <li><a href="/site/category?name=ayurveda--herbal">Ayurveda, Herbal & Organic</a></li>
          <li><a href="/site/category?name=agriculture">Agriculture</a></li>
          <li><a href="/site/category?name=business--finance">Business &amp; Finance</a></li>
          <li><a href="/site/category?name=celebrity--films">Celebrity &amp; Films</a></li>
          <li><a href="/site/category?name=children">Children</a></li>
          <li><a href="/site/category?name=comics">Comics</a></li>
          <li><a href="/site/category?name=computers--electronics">Computers &amp; Electronics</a></li>
          <li><a href="/site/category?name=cooking-food--beverage">Cooking, Food &amp; Beverage</a></li>
          <li><a href="/site/category?name=craft--hobbies">Craft &amp; Hobbies </a></li>
          <li><a href="/site/category?name=cricket">Cricket </a></li>
          <li><a href="/site/category?name=culture">Culture</a></li>
          <li><a href="/site/category?name=education">Education</a></li>
          <li><a href="/site/category?name=enrichment">Enrichment</a></li>
          <li><a href="/site/category?name=entertainment--tv">Entertainment &amp; TV</a></li>
          <li><a href="/site/category?name=fashion">Fashion</a></li>
          <li><a href="/site/category?name=fiction">Fiction</a></li>
          <li><a href="/site/category?name=gaming">Gaming</a></li>
          <li><a href="/site/category?name=general-interest">General Interest</a></li>
          <li><a href="/site/category?name=health--fitness">Health &amp; Fitness</a></li>
          <li><a href="/site/category?name=history">History</a></li>
          <li><a href="/site/category?name=hobbies--puzzles">Hobbies &amp; Puzzles</a></li>
          <li><a href="/site/category?name=home--garden">Home &amp; Garden</a></li>
          <li><a href="/site/category?name=hotels--hospitality">Hotels &amp; Hospitality</a></li>
          <li><a href="/site/category?name=industry--trade">Industry &amp; Trade</a></li>
          <li><a href="/site/category?name=interior-designing">Interior Designing</a></li>
          <li><a href="/site/category?name=jewellery-design">Jewellery Design</a></li>
          <li><a href="/site/category?name=journals">Journals</a></li>
          <li><a href="/site/category?name=language-learning-skills">Language Learning Skills</a></li>
          <li><a href="/site/category?name=lifestyle">Lifestyle</a></li>
          <li><a href="/site/category?name=medical">Medical</a></li>
          <li><a href="/site/category?name=mens-interest">Men's Interest</a></li>
          <li><a href="/site/category?name=music--dance">Music &amp; Dance</a></li>
          <li><a href="/site/category?name=nature">Nature</a></li>
          <li><a href="/site/category?name=newspapers">Newspapers</a></li>
          <li><a href="/site/category?name=news--politics">News &amp; Politics</a></li>
          <li><a href="/site/category?name=parenting--family">Parenting &amp; Family</a></li>
          <li><a href="/site/category?name=photography">Photography</a></li>
          <li><a href="/site/category?name=psychology">Psychology</a></li>
          <li><a href="/site/category?name=real-estate">Real Estate</a></li>
          <li><a href="/site/category?name=religious--spiritual">Religious &amp; Spiritual</a></li>
          <li><a href="/site/category?name=salon--spa">Salon &amp; Spa</a></li>
          <li><a href="/site/category?name=self-help">Self Help</a></li>
          <li><a href="/site/category?name=science--technology">Science &amp; Technology</a></li>
          <li><a href="/site/category?name=special-needs">Special Needs</a></li>
          <li><a href="/site/category?name=sports--recreation">Sports &amp; Recreation </a></li>
          <li><a href="/site/category?name=teen">Teen</a></li>
          <li><a href="/site/category?name=travel">Travel</a></li>
          <li><a href="/site/category?name=weddings">Weddings</a></li>
          <li><a href="/site/category?name=womens-interest">Women's Interest</a></li>
          <li><a href="/site/category?name=yoga">Yoga</a></li>  
     </ul>
    </div>
      
      
         <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>About Us</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="/site/about"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;About Us</a></li>
            <li><a href="/site/faq"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;FAQ's – Frequently Asked Questions</a></li>
            <li><a href="/site/publisher"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Publishers</a></li>
            <li><a href="/site/contact"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Contact Us</a></li>
          </ul>
        </div>
        
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>Customer Service</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="/site/location"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Locations We Ship to</a></li>
            <li><a href="/site/shipping"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Shipping Policy</a></li>
            <li><a href="/site/estimated-delivery"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Estimated Delivery Time</a></li>
            <li><a href="/site/librariescorporate"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Libraries & Corporate Orders</a></li>
          </ul>
        </div>
        
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>Payment &amp; Terms</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="/site/term"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Terms &amp; Condition</a></li>
            <li><a href="/site/payment"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Payment Method</a></li>
            <li><a href="/site/refund"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Refund Policy</a></li>
            <li><a href="/site/policy"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>My Account</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;My Account</a></li>
            <li><a href="/site/checkout"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;My Cart</a></li>
            <li><a href="/site/checkout"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Checkout</a></li>
            <li><a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Login</a></li>
          </ul>
          
        </div>
         </div>
          </div>
     
  </div>
  <div class="footer-bottom">
    <div class="">
    
    <div class="">
      <div class="col-xs-12 nopadding need-assitance-main"><p><span>Need Assistance?</span> Contact our Customer Support Team 24 x 7 by email at <a href="mailto:Support@MazagineWorld.online">Support@MazaginesWorld.online</a></p>
      <p>For Corporate & Library Orders, Please write to Customer Service at <a href="mailto:contact@mazagineworld.online">Contact@MazaginesWorld.online</a></p>
      <p>If You wish to Advertise in any Magazines, Please write to  <a href="mailto:advertise@mazagineworld.online">Advertise@MazaginesWorld.online</a></p>
      </div>
      </div>
    
      <div class="">
      <div class="col-xs-12 footer-links-main">
      <ul>
            <li><a>Wide Range</a></li>
            <li><a>Great prices</a></li>
            <li><a>Best Discounts</a></li>
            <li><a>Quick Delivery</a></li>
            <li><a>24x7 Customer care</a></li>
            <li><a>Free Shipping</a></li>
            <li><a>Easy Renewals</a></li>
            </ul>
            </div>
      </div>
        <div class="col-xs-12 col-sm-12">
          <p class="text-xs-center">Magazines World &copy; Copyright 2017 All Rights Reserved.</p>
        </div>
        <div><a href="#" class="scrollup">Scroll</a></div>
      </div>
    </div>
  </div>
</footer>
<!--================================ Login and Register Forms ===========================================--> 
  
  <div class="modal fade login-popup-panel-main" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info" id="dynamic-modal">
            
            
        </div>
    </div>
  </div>

  
<!--REGISTER POPUP END-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
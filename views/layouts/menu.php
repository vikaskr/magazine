<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}

if(\Yii::$app->user->can('user/index'))
{
$menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user"];
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $post app\models\Post */

$this->title = $post->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $post->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $post->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $post,
        'attributes' => [
            'id',
            'mub_user_id',
            'post_title',
            'post_date',
            'post_excerpt',
            'status',
            'url:url',
            'post_name',
            'post_brief:ntext',
            'post_parent',
            'pinned',
            'order',
            'post_type',
            'comment_count',
            'approved_by',
            'approved_on',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>
A Product of Yii Software LLC
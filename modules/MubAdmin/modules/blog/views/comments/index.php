<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\postComments\PostCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Post Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['postComment' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Post Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'email:email',
            'name',
            'title',
            // 'type',
            'comment_text:ntext',
            // 'mub_user_id',
            [
                'attribute' => 'Approve',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->approved_by==null) ? '<button onClick="approveComment('.$model->id.')">Approve</button>' : '<button onClick="unApprove('.$model->id.')">Un Approve</button>';
                },
            ],
            // 'approved_on',
            // 'approved_by',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

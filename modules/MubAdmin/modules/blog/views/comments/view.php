<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $postComment app\postComments\PostComment */

$this->title = $postComment->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Post Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $postComment->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $postComment->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $postComment,
        'attributes' => [
            'id',
            'post_id',
            'email:email',
            'name',
            'comment_id',
            'title',
            'type',
            'comment_text:ntext',
            'mub_user_id',
            'status',
            'approved_on',
            'approved_by',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>

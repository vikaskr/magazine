<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;
use app\modules\MubAdmin\modules\csvreader\models\Origin;
use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;

/**
 * This is the model class for table "magazine".
 *
 * @property integer $id
 * @property integer $origin_id
 * @property string $product_code
 * @property string $subject
 * @property string $name
 * @property string $slug
 * @property string $issues_per_year
 * @property string $periodicity
 * @property string $issn
 * @property integer $publisher_id
 * @property integer $language_id
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Language $language
 * @property Origin $origin
 * @property Publisher $publisher
 * @property MagazinePrice[] $magazinePrices
 * @property MagazineSubject[] $magazineSubjects
 */

class Magazine extends \app\components\Model implements CartPositionInterface
{
    use CartPositionTrait;

    public function getPrice()
    {
        $userDetails = ip_info();
        $userCountry = $userDetails['country'];
        if(($userCountry == '')||
            ($userCountry == 'US')||
            ($userCountry == 'USA'))
        {
            $userCountry = 'UNITED STATES OF AMERICA';
        }

        $userCountry = \app\helpers\StringHelper::generateSlug($userCountry);
        $origin = Origin::find()->where(['origin_slug' => $userCountry])->one();
        if(!empty($origin))
        {
            $originId = $origin->id;
            $magazineId = $this->id;
            $gbpPrice = MagazinePrice::find()->where(['magazine_id' => $magazineId,'origin_id' => $originId])->one();
            $discount = $gbpPrice->discount_percentage;

            switch($originId)
            {
                case '158': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price))) - ($discount / 100) * (ceil(floatval($gbpPrice->price))): (cei(floatval($gbpPrice->price)));                   
                    break;
                }
                case '100': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.83782)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.83782)): (ceil(floatval($gbpPrice->price) * 1.83782));                     
                    break;
                }
                case '56': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 0.529505)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 0.529505)): (ceil(floatval($gbpPrice->price) * 0.529505));                   
                    break;
                }
                case '126': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.14227)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.14227)): (ceil(floatval($gbpPrice->price) * 1.14227));
                    break;
                }
                case '164': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.81913)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.81913)): (ceil(floatval($gbpPrice->price) * 1.81913));
                    break;
                }

                case '61': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 8.86216)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 8.86216)): (ceil(floatval($gbpPrice->price) * 8.86216));
                    break;
                }

                case '15': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 24.8449)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 24.8449)): (ceil(floatval($gbpPrice->price) * 24.8449));
                    break;
                }
                case '62': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 91.7839)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 91.7839)): (ceil(floatval($gbpPrice->price) * 91.7839));
                    break;
                }
                case '67': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 150.001)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 150.001)): (ceil(floatval($gbpPrice->price) * 150.001));
                    break;
                }
                case '69': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 0.42348)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 0.42348)): (ceil(floatval($gbpPrice->price) * 0.42348));
                    break;
                }
                case '73': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 5.44895)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 5.44895)): (ceil(floatval($gbpPrice->price) * 5.44895));
                    break;
                }
                case '79': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 0.541517)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 0.541517)): (ceil(floatval($gbpPrice->price) * 0.541517));
                    break;
                }
                case '83': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 5.12645)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 5.12645)): (ceil(floatval($gbpPrice->price) * 5.12645));
                    break;
                }
                case '84': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 81.215)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 81.215)): (ceil(floatval($gbpPrice->price) * 81.215));
                    break;
                }
                case '96': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 5.17233)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 5.17233)): (ceil(floatval($gbpPrice->price) * 5.17233));
                    break;
                }
                case '182': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.40833)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.40833)): (ceil(floatval($gbpPrice->price) * 1.40833));
                    break;
                }
                case '68': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 0.998505)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 0.998505)): (ceil(floatval($gbpPrice->price) * 0.998505));
                    break;
                }
                case '72': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 2123.06)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 2123.06)): (ceil(floatval($gbpPrice->price) * 2123.06));
                    break;
                }
                case '85': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 5.28147)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 5.28147)): (ceil(floatval($gbpPrice->price) * 5.28147));
                    break;
                }

                case '86': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.8471)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.8471)): (ceil(floatval($gbpPrice->price) * 1.8471));
                    break;
                }

                case '45': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 16.5631)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 16.5631)): (ceil(floatval($gbpPrice->price) * 16.5631));
                    break;
                }

                case '88': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 219.249)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 219.249)): (ceil(floatval($gbpPrice->price) * 219.249));
                    break;
                }

                case '156': 
                {
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.34625)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.34625)): (ceil(floatval($gbpPrice->price) * 1.34625));
                    break;
                }

               default:  
                    return isset($discount)? (ceil(floatval($gbpPrice->price) * 1.40833)) - ($discount / 100) * (ceil(floatval($gbpPrice->price) * 1.40833)): (ceil(floatval($gbpPrice->price) * 1.40833));
                    break;
            }
        }
        else
        {
            p($userCountry);
        }        
    }

    public function getId()
    {
        return $this->id;
    }

    public static function tableName()
    {
        return 'magazine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin_id', 'publisher_id', 'language_id', 'subject_id'], 'integer'],
            [['name', 'slug', 'description', 'status', 'cover_image', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_code', 'issues_per_year', 'periodicity','estimated_delivery','format','promoted_by','distributed_by','supplied_by','marketed_by'], 'string', 'max' => 255],
            [['issn'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['origin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Origin::className(), 'targetAttribute' => ['origin_id' => 'id']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_code' => 'Product Code',
            'subject_id' => 'Subject id',
            'cover_image' => 'cover_image',
            'name' => 'Name',
            'slug' => 'Slug',
            'issues_per_year' => 'Issues Per Year',
            'periodicity' => 'Periodicity',
            'issn' => 'Issn',
            'publisher_id' => 'Publisher ID',
            'language_id' => 'Language ID',
            'origin_id' => 'Origin ID',
            'estimated_delivery' => 'Estimated Delivery',
            'format' => 'Format',
            'promoted_by' => 'Promoted By',
            'distributed_by' => 'Distributed By',
            'supplied_by' => 'Supplied By',
            'marketed_by' => 'Marketed By',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrigin()
    {
        return $this->hasOne(Origin::className(), ['id' => 'origin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazinePrices()
    {
        return $this->hasMany(MagazinePrice::className(), ['magazine_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazineSubjects()
    {
        return $this->hasMany(MagazineSubject::className(), ['id' => 'subject_id']);
    }
}

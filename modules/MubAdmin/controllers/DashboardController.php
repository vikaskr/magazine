<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\MubUser;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;
use app\models\MubUserSearch;

class DashboardController extends MubController
{
	public function getProcessModel()
	{
        return new BlogProcess();
	}

	public function getPrimaryModel()
	{
        return new MubUser();
	}

    public function getSearchModel()
    {
        return new MubUserSearch();
    }

	public function actionImageUpload()
	{
		$file = \Yii::$app->getBodyParams();
		p($file);
	}

	public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function saveBookingDetails($postData)
    {
         $cart = new ShoppingCart();
         $cartItems = ArrayHelper::toArray($cart->getPositions());
         $price = $cart->getCost();

        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->first_name = $postData['billing_name'];
            $booking->last_name = $postData['last_name'];
            $booking->email = $postData['billing_email'];
            $booking->order_id = $postData['order_id'];
            $booking->mobile = $postData['billing_tel'];
            $booking->landline = $postData['landline'];
            $booking->mub_user_id = '1';
            $booking->address = $postData['billing_address'];
            $booking->city = $postData['billing_city'];
            $booking->state = $postData['billing_state'];
            $booking->country = $postData['billing_country'];
            $booking->pin_code = $postData['billing_zip'];
            $booking->magazine = $postData['magazine'];
            $booking->quantity = $postData['quantity'];
            $booking->status = 'booking';
            $booking->amount = $postData['amount'];

            if(!$booking->save())
            {
               p($booking->getErrors());
            }
        }
        return true;
    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $postData = \Yii::$app->request->getBodyParams();
    
          $this->saveBookingDetails($postData);
          return $postData;
        }
    }
}
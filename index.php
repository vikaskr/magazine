<?php

function p($data){
    echo '<pre>';
    die(print_r($data));
}
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');
\Yii::setAlias('@upload', __DIR__.'/uploads');
$config = require(__DIR__ . '/config/web.php');

function ip_info($ip = NULL) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE)
    {
        $ip = $_SERVER["REMOTE_ADDR"];
        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if(empty($ip)){
    if (filter_var($ip, FILTER_VALIDATE_IP)) 
    {
        // $ipData = @json_decode(file_get_contents("https://extreme-ip-lookup.com/json/". $ip;));
        $ch = curl_init();
        $url = "https://extreme-ip-lookup.com/json/". $ip;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $ipData = @json_decode(curl_exec($ch));
        curl_close($ch); 
    }
    $output['country'] = $ipData->country;
    return $output;
    }}
(new yii\web\Application($config))->run();
